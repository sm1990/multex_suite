Software package to simulate atomically resolved elemental maps taken with an Energy-Filtering Transmission Electron Microscope (EFTEM) in a relativistically correct framework. The programs `wavegen` and `transpot` are used to calculate the transition potentials for a beam electron ionizing a specimen atom. Using these potentials, the program `multex` can perform a multislice calculation that yield EFTEM elemental maps. For a description of how to use these programs, see the manuals in their respective folders. For the theory behind these calculations, see [1][Majert2019].

The subfolder `misc` contains some undocumented scripts that were used during the conception of [1][Majert2019].

[Majert2019]: S. Majert and H. Kohl, in: Advances in Imaging and Electron Physics, vol.211, to-be-published
