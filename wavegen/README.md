Self-consistent solution of the Kohn-Sham equation for an atom with a scalar-relativistic Hamilton operator.

The program is based on a programm written by D.R.Hamann [1][Hamann1989] and was slightly modified and provided to us by the work group of P.Krüger [2][Krueger2015]. 

For details on compilation and usage see the transpot manual (located in multex_suite/transpot/doc).

[Hamann1989]: D.R.Hamann, Physical Review B 40 (1989) 2980, DOI: 10.1103/PhysRevB.40.2980
[Krueger2015]: P. Krüger, personal communication, 2015, URL: https://www.uni-muenster.de/Physik.FT/Forschung/agrohlfing/index.html
