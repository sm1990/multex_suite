cmake_minimum_required(VERSION 3.5 FATAL_ERROR)
project("TransPot" LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
#set(CMAKE_VERBOSE_MAKEFILE ON)

set(
    CMAKE_RUNTIME_OUTPUT_DIRECTORY
    ${PROJECT_SOURCE_DIR}/build
 )
include(version.cmake)


## Sources
set(SOURCES main.cpp
            transpot_constants.hpp
            transpot_exceptions.hpp 
            transpot_shared_data.hpp
            transition_potential_containers.hpp
            path_switcher.hpp
            logger.hpp
            transpot_io.hpp	               transpot_io.cpp
            value_pairs.hpp	               value_pairs.cc
            spline_fit.hpp	               spline_fit.cc
            hankel_trafo.hpp	           hankel_trafo.cc
            spherical_harmonics.hpp        spherical_harmonics.cc
            three_harmonics_integrated.hpp three_harmonics_integrated.cc
            transition_potential.hpp       transition_potential.cc
            prepare_dft_results.hpp        prepare_dft_results.cpp)
set(SOURCES_ABS)
foreach (source ${SOURCES})
	list(APPEND SOURCE_ABS ${PROJECT_SOURCE_DIR}/src/${source})
endforeach()



## Targets
add_executable(transpot ${SOURCE_ABS})
set_target_properties(transpot PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(transpot fftw3 wignerSymbols armadillo boost_system boost_filesystem)
target_compile_definitions(transpot PRIVATE VERSION="${VERSION}")

##Compiler flags
set(RELEASE_FLAGS -Ofast -march=native)
set(DEBUG_FLAGS -Wall -Wextra -Wconversion -Wpedantic -Ofast -march=native)
set(DEBUG_GDB_FLAGS -Wall -Wextra -Wconversion -Wpedantic -g) #gdb; file transpot; run;
set(PROFILE_FLAGS -Ofast -march=native -pg -no-pie) #after execution: gprof ./transpot > profile.txt
set_target_properties(transpot PROPERTIES LINK_FLAGS "-pg -no-pie") #necessary for profile build
if ( CMAKE_CXX_COMPILER_ID MATCHES "Clang|AppleClang|GNU" )
    target_compile_options( transpot PRIVATE ${PROFILE_FLAGS})
endif()