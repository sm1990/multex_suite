# Introduction
The program `transpot/TransPot` ("Transition Potentials") was written during the PhD thesis \cite MyThesis and can calculate transition potentials describing inner-shell ionizations in a relativistic or a non-relativistic fashion. These transition potentials are intended to be used in the program `multex` to simulate elemental maps obtained by Energy Filtering Transmission Electron Microscopy (EFTEM). To calculate the transition potential of an ionization, `transpot` requires the radial atomic wave function of the atomic electron that is ejected during the ionization and the atomic potential. Both of these quantities can be calculated with the program `wavegen`, which is a DFT program that is based on a method by *Hamann* \cite Hamann1989 and was slightly modified and provided to us by the work group of *Krüger* \cite Krueger2015. The aim of this manual is to explain how to setup and use both `transpot` and `wavegen`. For details on the theory behind these two programs, please refer to \cite Hamann1989 and \cite MyThesis. 


# Setup
## Requirements and Recommendations
Aside from a current C++ compiler (the [GNU Compiler Collection - GCC](https://gcc.gnu.org/) will be used here), `transpot`
requires the following libraries:
* [Armadillo](http://arma.sourceforge.net/) \cite Sanderson2016 \n
  A linear algebra library for C++ that `transpot` uses for some of the required matrix calculations.
* [FFTW](http://www.fftw.org/)  \cite Frigo2005 \n
  The library with the fastest Fourier transform in the west powers the cosine/sine transforms that are required
  for the Hankel transform algorithm described in \cite Toyoda2010.
* [Boost](http://www.boost.org/) \n
  A host of useful libraries for C++. `TransPot` utilizes the Property Tree and the Filesystem library for
  user input and output.
* [WignerSymbols](http://dx.doi.org/10.5281/zenodo.591876) \cite Schulten1975 \cite Schulten1976 \cite Luscombe1998 \n
  Library that evaluates the Wigner 3j-symbols required for the calculation of the transition potential. A copy of
  this library should be located in the zip-file `Wigner_symbols_library.zip` inside the `transpot`-directory. 

The program `wavegen` only requires a current Fortran compiler (included in the [GNU Compiler Collection](https://gcc.gnu.org/)). Although technically not required, [cmake](https://cmake.org/) is very helpful when building `transpot` since `transpot` comes with its own `CMakeLists.txt` file. Note that building `wavegen` and `transpot` was only tested on Linux systems, but should in principle also be possible on Windows machines.

## Building wavegen
If the required components (i.e. GCC) are properly installed, `wavegen` can be compiled by performing the following steps:
1. Open your shell of choice.
2. Navigate to the `wavegen` directory.
3. Compile `wavegen` by running the command
~~~~~~~~~~~~~~~~~~~~~~~ 
	gfortran -o wavegen wavegen.f
~~~~~~~~~~~~~~~~~~~~~~~ 

## Building transpot
To build `transpot`, using `cmake` is highly recommended. If the required components (i.e. GCC, Armadillo, FFTW, WignerSymbols and Boost) are properly installed, `transpot` can be compiled by performing the following steps:
1. Make sure Armdaillo, FFTW, WignerSymbols and Boost are in your path.
2. Open your shell of choice.
3. Navigate to the `transpot/build` directory.
4. Run the command
~~~~~~~~~~~~~~~~~~~~~~~ 
	cmake -G "Unix Makefiles" ..
~~~~~~~~~~~~~~~~~~~~~~~ 
	to generate a makefile for `transpot`.
5. Compile `transpot` by running the make command:
~~~~~~~~~~~~~~~~~~~~~~~ 
	make
~~~~~~~~~~~~~~~~~~~~~~~


# How to use wavegen
## The config file
The config file for `wavegen` should be named `wavegen.dat`. It tells the program which exchange-correlation (XC) functional to use, the atomic number (Z) of the simulated atom and its electron configuration. The electron configuration is described in rows, where each row contains the principal quantum number (n), the azimuthal quantum number (l) and the occupation number (ON) separated by spin direction (ONup and ONdown). Conceptually, the config file for `wavegen` looks like this:
~~~~~~~~~~~~~~~~~~~~~~~
XC-functional
Z
n l ONup ONdown
n l ONup ONdown
...............
n l Onup Ondown
~~~~~~~~~~~~~~~~~~~~~~~  
The XC-functional-field can be set to two different values, 'LDA' (Local Density Approximation) and 'GGA' (Generalized Gradient Approximation). Each row of the electron configuration describes the occupation of a single electron subshell separated into a spin-up part and a spin-down part. To give an example, a fully filled K-shell would be denoted by the single row '1 0 1 1' (since the K-shell only has one subshell), while a fully filled L-shell would be denoted by two rows '2 0 1 1' and '2 1 3 3'. 

\anchor sec_example_wavegen
## An example for wavegen
Let us assume we want to simulate an EFTEM image \f$\unit[10]{eV}\f$ above the oxygen K-edge for a \f$\text{SrTiO}_3\f$ specimen with the program `multex`. As input, we need transition potentials for the oxygen K-edge from `transpot`, which in turn requires the wave function of the K-edge electrons and the atomic potential from `wavegen`. Assuming that the atomic electrons are equally distributed between spin-up and spin-down and that the oxygen atom is not excited before the ionization, the 'wavegen.dat' file looks like this:
~~~~~~~~~~~~~~~~~~~~~~~
LDA
 8
     1     0     1. 1.
     2     0     1. 1.
     2     1     2. 2.   
~~~~~~~~~~~~~~~~~~~~~~~
Running `wavegen`, three output files are obtained, `r_v_value.dat`, `waveup.dat` and `wavedown.dat`. The `r_v_value.dat` file contains the atomic potential. Because the atomic potential is also contained in the other two files, the `r_v_value.dat` file is not required as an input for `transpot`. Since we have assumed an equal distribution of spin-up and spin-down electrons, the two files `waveup.dat` and `wavedown.dat` are identical. A partial image of the content of the `waveup.dat` file is shown in fig. \ref fig_waveup_dat "1.1". The first row of the file contains the eigenvalues of the subshells in eV. In the following rows, the first column represents the distance from the atomic center in atomic units. The second column is the atomic potential in Hartree and the following columns are the radial wave functions of the atomic electrons of the different subshells. Plotting the potential and the bound wave functions from the `waveup.dat` file, the graphs in fig. \ref fig_atomic_potential "1.2" and \ref fig_boundwaves "1.3" are obtained.
\anchor fig_waveup_dat
\image html waveup_dat.jpg "Content of the file waveup.dat."
\image latex waveup_dat.pdf "Content of the file waveup.dat."
\anchor fig_atomic_potential
\image html atomic_potential.jpg "Atomic potential resulting from the test run."
\image latex atomic_potential.pdf "Atomic potential resulting from the test run."
\anchor fig_boundwaves
\image html boundwaves.jpg "Radial wave functions of the electron subshells resulting from the test run."
\image latex boundwaves.pdf "Radial wave functions of the electron subshells resulting from the test run."


# How to use transpot
## The config file
`TransPot` reads the parameters required for a simulation from a config file. The config file has to be in json-format and is read by transpot using the json parser of `boost::property_tree`. Depending on
the config file, transpot will calculate only a single transition potential \f$\ket{nlm_l}\rightarrow\ket{e_w l' m_l'}\f$ or several transition potentials for different \f$m_l,~l'\f$ and \f$m_l'\f$. To output more than a single transition potential, several values for \f$m_l,~l'\f$ and \f$m_l'\f$ can be listed in the config file. In general, the config file has to contain the following fields:
* `dft_filename`\n
  Name of the file that contains the results of the DFT simulation performed by `wavegen`, i.e. the
  radial wave functions of the bound electrons and the potential. Either of the files with the
  default name `waveup.dat` and `wavedown.dat` can serve as an input for `transpot`. 
* `wave_function_col_no`\n
  Column number of the radial wave function that corresponds to the atomic electron before ionization
  in the DFT file named in the `dft_filename` object. Because the zeroth column of the output file of `wavegen`
  starts with the \f$r\f$-values and the first column is the atomic potential, the correct value for
  a K-shell ionization would be "2". 
* `output_dir_name`\n
  Name of the directory to which the simulation results are to be saved. If the output directory
  does not exist within the execution directory, `transpot` will create it. If the output directory
  already exists, `transpot` will create a new directory with a number appended to it. For example, if
  the output directory name is `transpot_simulation` and the the folder `transpot_simulation` is
  already present in the execution directory, `transpot` will save the results of the simulation
  run in the folder `transpot_simulation1`. 
* `spin_setting`\n
  Set the spins of the atomic electron before and after ionization. Possible settings are:
    * `up_up`
    * `up_down`
    * `down_up`
    * `down_down`
    * `none`

  The `none`-setting signifies a conventional simulation. The different settings for the 
  relativistic simulation (`up_up`, `up_down`, `down_up` and `down_down`) are for the 
  different possible spin orientations of the Darwin wave functions of the free (i.e. the bound
  atomic electron after the ionization) and the bound atomic electron, respectively.
* `atomic_number`\n
  Atomic number of the nucleus.
* `n_bound` (\f$ n \f$ in \cite MyThesis)\n
  Principal quantum number of the bound atomic electron.
* `l_bound` (\f$ l \f$ in \cite MyThesis)\n
  Azimuthal quantum number of the bound atomic electron.
* `mls_bound` (\f$ m_l \f$ in \cite MyThesis)\n
  List of magnetic quantum number(s) of the bound electron. A transition potential is computed for each magnetic quantum number given here.
* `energy_free` (\f$ e_w \f$ in \cite MyThesis)\n
  Energy of the free atomic electron after the ionization.
* `ls_free`(\f$ l' \f$ in \cite MyThesis)\n
  List of the azimuthal quantum number(s) of the free atomic electron. A transition potential is computed for each azimuthal quantum number given here.
* `mls_free`(\f$ m_l' \f$ in \cite MyThesis)\n
  List of the magnetic quantum number(s) of the free atomic electron. A transition potential is computed for each magnetic quantum number given here.
* `acc_voltage`\n
  Acceleration voltage of the TEM in kV.
* `x_pixel` and `y_pixel`\n
  Dimensions of the calculated transition potential(s) in pixel.
* `qx_dim` and `qy_dim`\n
  Dimensions of the calculated transition potential(s) in Fourier space divided by \f$2\pi\f$ in \f$1/\text{\AA}\f$.
* `samples_hankeltrafo`\n
  Amount of times the Hankel transform algorithm samples the radial wave functions of the atomic
  electron.

## An example for transpot
Let us assume we want to simulate an EFTEM image \f$\unit[10]{eV}\f$ above the oxygen K-edge for a \f$\text{SrTiO}_3\f$ specimen with the program `multex`. As input, we need transition potentials for the oxygen K-edge from `transpot`, which in turn requires the wave function of the K-edge electrons and the atomic potential that we have \ref sec_example_wavegen "already" calculated. Assuming that only dipole allowed transitions occur, transition potentials from the K-shell (i.e. \f$n=1,l=0,m_l=0\f$) to the free states \f$\ket{e_w = \unit[10]{eV},l'=1,m_l'=0},~\ket{ \unit[10]{eV},l'=1,m_l'=-1},~\ket{ \unit[10]{eV},l'=1,m_l'=1}\f$ are required. To calculate the three different transition potentials with `transpot`, one for each final state of the atomic electron, the following steps have to be performed:
1. Add the result of the `wavegen`-calculation (i.e. either the file `waveup.dat` or `wavedown.dat`) 
   to the directory with the `transpot` executable. 
2. Create a config file for `transpot`. In this example, the config file is named `oxygen_K.json`.
3. Fill out the config file. For this example, the values shown in fig. \ref fig_oxygen_K_json "1.4"
   are used. One could of course use different values for the fields `output_dir_name`,
   `spin_setting`, `acc_voltage`, `x_pixel`, `y_pixel`, `qx_dim`, `qy_dim` and `samples_hankeltrafo`.
   The other fields of the config file have to be set to fit the possible bound and free states of
   the atomic electron.
4. Open a terminal and run `transpot`:
~~~~~~~~~~~~~~~~~~~~~~~ 
	./transpot oxygen_K.json
~~~~~~~~~~~~~~~~~~~~~~~ 

As specified in the config file, the three resulting transition potentials (in Fourier space) are saved in a directory with the name `oxygen_K`. Fig. \ref fig_transition_potential "1.5" shows the projection of one of these transition potentials, namely the one that describes a transition of the atomic electron from the K-shell to the state \f$\ket{e_w = \unit[10]{eV},l'=1,m_l'=0}\f$.
\anchor fig_oxygen_K_json
\image html oxygen_K_json.jpg "Content of the file oxygen_K.json."
\image latex oxygen_K_json.pdf "Content of the file oxygen_K.json."
\anchor fig_transition_potential
\image html transition_potential.jpg "Imaginary part of the projected transition potential of an oxygen K-shell ionization. The state of the free atomic electron after the ionization is e_w = 10eV, l'=1, m_l'=0."
\image latex transition_potential.pdf "Imaginary part of the projected transition potential of an oxygen K-shell ionization. The state of the free atomic electron after the ionization is e_w = 10eV, l'=1, m_l'=0."


