#ifndef prepare_dft_results_HPP
#define prepare_dft_results_HPP

#include<cmath>


#include"transpot_shared_data.hpp"
#include"value_pairs.hpp"

/**
 *@file prepare_dft_results.hpp
 *@brief Functions to prepare the results of wavegen (header)
 */

///Convert values of the bound wave function of the atomic electron to appropriate units
/*!
 * _wavegen_ outputs the wave function resulting from the DFT calculation (which is assumed to be stored in \p tsd)
 * as \f$u = R/r\f$, where \f$r\f$ is the distance from the nucleus and \f$R\f$ is the radial wave function.
 * This function (_prepare_bound_wave_) obtains \f$R\f$ by multiplying the results of _wavegen_ with \f$r\f$.
 * It then converts \f$R\f$ from atomic units to Angstrom.
 */
Value_pairs prepare_bound_wave(const Transpot_shared_data& tsd);

///Calculate wave function of the (free) atomic electron after ionization
/*!
 * This function first calculates the wave function of the free atomic electron with the help of the
 * contwave-function from \cite Frigge2011. The results of this function are then converted from
 * atomic units and Hartree to Angstrom and electron Volt. The parameter \p l_free is the azimuthal
 * quantum number of the free atomic electron and the potential resulting from the DFT calculation,
 * the energy of the free atomic electron and the grid parameter \p amesh used in wavegen are
 * required form the \p tsd storage container.
 */
Value_pairs prepare_free_wave(unsigned int l_free, const Transpot_shared_data& tsd);


///Convert values of the atomic potential resulting from the DFT calculation to appropriate units
/*!
 * The potential outputted by _wavegen_ (assumed to be stored in \p tsd) is converted from atomic units
 * to Angstrom (\f$r\f$-values) and from Hartree to electron Volt (\f$V(r)\f$-values).
 */
Value_pairs prepare_potential(const Transpot_shared_data& tsd);


///Calculate normalization factors for the Darwin wave function for a given radial wave function \p wave
/*!
 * The normalization factors of the Darwin wave function corresponding to the radial wave function \p wave
 * of an atomic electron with an energy \p energy are calculated. This procedure requires the grid parameter
 * \p amesh, which is assusmed to be stored in \p tsd. The mathematics of this procedure are discussed in
 * \cite Frigge2011.
 */
double darwin_norm(double energy, const Value_pairs& wave, const Transpot_shared_data& tsd);
#endif
