#include "prepare_dft_results.hpp"

#include"transpot_constants.hpp"
#include"contwave.c"

/**
 *@file prepare_dft_results.cpp
 *@brief Functions to prepare the results of wavegen (source)
 */

Value_pairs prepare_bound_wave(const Transpot_shared_data& tsd){
	//convert atomic units to Angstrom and convert u from wavegen to R = u/r;
	//convert R from 1/sqrt(au^3) to 1/sqrt(Ang^3)
	const double x_conversion = cst::au_to_ang;
	const double y_conversion = 1./sqrt(pow(cst::au_to_ang,3));

	const Value_pairs& bound_wave = tsd.cd.bound_wavefunction;
	Value_pairs result(bound_wave.size());
	for (unsigned int i=0; i<bound_wave.size(); i++){
		result(i,0) = bound_wave(i,0)*x_conversion;
		result(i,1) = (bound_wave(i,1)/bound_wave(i,0))*y_conversion;
	}

	return result;
}



Value_pairs prepare_free_wave(unsigned int l_free, const Transpot_shared_data& tsd){
	int mmax = static_cast<int>(tsd.cd.potential.size());
	const int emax = 0; 	//energy steps calculated
	const double einc = 0; //energy increase per step

	//create dummyArray for use with contwave;
	//different energy steps are mentioned even though only one energy loss
	//is needed for the calculation of the corresponding transition potential
	std::vector<std::vector<std::vector<double>>> dummy_array;
	dummy_array.resize(mmax);
	for (int i=0; i<mmax; i++){
		dummy_array[i].resize(emax+1);
		for (int e=0; e<=emax; e++){
			dummy_array[i][e].resize(l_free+1);
		}
	}


	//use contwave from \cite Frigge2011
	std::vector<double> pot_0 = tsd.cd.potential.column_as_vector(0);
	std::vector<double> pot_1 = tsd.cd.potential.column_as_vector(1);

	contwave(tsd.cd.amesh,emax,l_free,einc,tsd.cd.energy_free,mmax,pot_0,pot_1,dummy_array);


	//parse data to output (for one energy value) and convert atomic units (au)
	//to Angstrom and 1/(sqrt(au^3)*sqrt(hartree)) to 1/(sqrt(Angstrom^3)*sqrt(eV)
	Value_pairs result(tsd.cd.potential.size());
	const double x_conversion = cst::au_to_ang;
	const double y_conversion = 1./(sqrt(pow(cst::au_to_ang,3))*sqrt(cst::hart_to_eV));
	for (unsigned int i=0; i<tsd.cd.potential.size(); i++){
		result(i,0) = tsd.cd.potential(i,0)*x_conversion;
		result(i,1) = dummy_array[i][0][l_free]*y_conversion;
	}

	return result;
}

Value_pairs prepare_potential(const Transpot_shared_data& tsd){
	const double x_conversion = cst::au_to_ang;
	const double y_conversion = cst::hart_to_eV;

	const Value_pairs& potential = tsd.cd.potential;
	Value_pairs result(potential.size());
	for (unsigned int i=0; i<potential.size(); i++){
		result(i,0) = potential(i,0)*x_conversion;
		result(i,1) = potential(i,1)*y_conversion;
	}

	return result;


}




double darwin_norm(double energy, const Value_pairs& wave, const Transpot_shared_data& tsd){

	//calculate the integral int_0^(infty) R(r)^2 V(r) r^2 dr converting V from hartree to eV
	double integral = 0;
	double log_amesh = log(tsd.cd.amesh);
	for (unsigned int i=0; i<wave.size(); i++){
		integral += pow(wave(i,1),2)*cst::hart_to_eV*tsd.cd.potential(i,1)*pow(wave(i,0),3)*log_amesh;
	}

	//calculate the normalization factor of the Darwin wavefunction
	double norm = 1./sqrt(1.+1./(2.*cst::m_ech2)*(energy-integral));


	return norm;
}
