#ifndef transition_potential_containers_HPP
#define transition_potential_containers_HPP

#include<armadillo>

#include"hankel_trafo.hpp"

/**
 *@file transition_potential_containers.hpp
 *@brief Some structs for the combined storage of results (integrals and reciprocal space coordinates)
 */

///Store several reciprocal space coordinates
struct Reciprocal_space_grids{
	///\f$q_z\f$ of the transition
	double qz;
	///Reciprocal space coordinate along x-direction of calculated area
	arma::rowvec qx_grid;
	///Recirprocal space coordinate along y-direction of calculated area
	arma::colvec qy_grid;
	///Reciprocal space coordinates of calculated area
	arma::mat qabs_grid;
};

///Radial integrals needed for the calculation of the transition potential (see \cite MyThesis)
struct Radial_integrals{
	///Integral \f$I_{R0}\f$
	Hankel_trafo I_R0;
	///Integral \f$I_{R1}\f$
	Hankel_trafo I_R1;
	///Integral \f$I_{R2}\f$
	Hankel_trafo I_R2;
	///Integral \f$I_{R3}\f$
	Hankel_trafo I_R3;
	///Normalization factor of the Darwin wave function of the bound atomic electron
	double norm_bound;
	///Normalization factor of the Darwin wave function of the free atomic electron
	double norm_free;
};

///Integrals needed for the calculation of the transition potential (see \cite MyThesis)
struct Integrals{
	///Integral \f$I_0\f$
	arma::cx_mat I_0;
	///Integral \f$I_{\Delta}\f$
	arma::cx_mat I_Delta;
	///Integral \f$I_x\f$
	arma::cx_mat I_x;
	///Integral \f$I_y\f$
	arma::cx_mat I_y;
	///Integral \f$I_z\f$
	arma::cx_mat I_z;
};










#endif
