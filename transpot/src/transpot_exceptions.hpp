#ifndef transpot_exceptions_HPP
#define transpot_exceptions_HPP

#include<exception>


/**
 *@file transpot_exceptions.hpp
 *@brief Exceptions for transpot
 */

///Exceptions in TransPot
class transpot_exception : public std::exception{
public:
	///Construct with error message \p error_msg
	explicit transpot_exception(const std::string& error_msg)
			 : error_message(error_msg) {}	
	///Return error message
	virtual const char* what() const throw(){
		return error_message.c_str();	
	}

private:
	const std::string error_message;

};

///Input/output-exceptions in transpot
class transpot_io_exception : public transpot_exception{
 public:
	///Construct with error message \p error_msg
	explicit transpot_io_exception(const std::string& error_msg)
			 : transpot_exception(error_msg) {}
};


///Exceptions in preparation phase of transpot
class transpot_prep_exception : public transpot_exception{
 public:
	///Construct with error message \p error_msg
	explicit transpot_prep_exception(const std::string& error_msg)
			 : transpot_exception(error_msg) {}
};



#endif
