#ifndef spherical_harmonics_HPP
#define spherical_harmonicss_HPP

#include <unordered_map>

#include <boost/functional/hash.hpp>

#include"transition_potential_containers.hpp"

/**
 *@file spherical_harmonics.hpp
 *@brief Class for the calculation of spherical harmonics (header)
 */

///Calculate spherical harmonics
/**
 * Calculate spherical harmonics \f$Y_{LM}\f$ on a defined reciprocal space grid.
 * The results of the calculation are saved internally. If a combination of \f$L\f$
 * and \f$M\f$ has already been calculated and is requested again, the stored values
 * will be given instead of performing a costly recalculation.
 */
class Spherical_harmonics{
 public:
	///Construct for set of (reciprocal space) grid-points \p q_grids
	Spherical_harmonics(Reciprocal_space_grids q_grids);
	///Calculate the spherical harmonic \f$Y_{LM}\f$
	/**Calculate the spherical harmonic \f$Y_{LM}\f$ on the grid-points that
	 * were provided to the Constructor.
	 */
	arma::cx_mat calc_spherical_harmonic(unsigned int L, int M);


 private:
	const Reciprocal_space_grids q_grids;
	using boost_hash = boost::hash<std::pair<unsigned,int>>;
	std::unordered_map< std::pair<unsigned, int>, arma::cx_mat, boost_hash> storage;

};





#endif
