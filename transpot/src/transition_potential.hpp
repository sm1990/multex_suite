#ifndef transition_potential_HPP
#define transition_potential_HPP

#include<armadillo>

#include"transpot_shared_data.hpp"
#include"transition_potential_containers.hpp"
#include"spherical_harmonics.hpp"

/**
 *@file transition_potential.hpp
 *@brief Class that calculates the transition potential (header)
 */


///Types of radial integrals, i.e. \f$I_{R0},~I_{R1},~I_{R2},~I_{R3}\f$ (see \cite MyThesis)
enum class Rad_int_types {I_R0, I_R1, I_R2, I_R3};

///Calculate the transition potential (for details see \cite MyThesis)
class Transition_potential{
 public:
	///Constructor
	/**Construct a Transition_potential object. While it needs to be constructed with the
	 * parameters stored in \p tsd, the magnetic quantum number of the bound electron \p m_bound
	 *  and the azimuthal quantum number of the free electron \p l_free can be chosen freely at
	 *  construction (even though a lot of values will just yield zero, e.g. if \p m_bound is
	 *  chosen to be higher than the \p l_bound saved in \p tsd).
	 */
	Transition_potential(int m_bound, unsigned int l_free, const Transpot_shared_data& tsd);
	///Calculate transition potential for a given \p m_free
	/**Calculate the transition potential for the magnetic quantum number of the
	 * free electron \p m_free. The results are saved in \p tsd, but no values from the
	 * \p tsd object are used in this function.
	 */
	void calc_transition_potential(int m_free, Transpot_shared_data& tsd);

 private:
	const Spin_settings spins;
	Radial_integrals rad_ints;
	const double energy_loss_by_hbarc;
	const Reciprocal_space_grids q_grids;
	const int l_bound;
	const int m_bound;
	const unsigned int l_free;

	arma::mat calc_radial_integral_on_q_grid(unsigned int L, Rad_int_types int_type);
	Integrals calc_integrals(int m_free);
	arma::cx_mat calc_integral_I0(int m_free, Spherical_harmonics& Y_LMs);
	double A(char sign, int k);


};





#endif
