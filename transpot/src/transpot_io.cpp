#include"transpot_io.hpp"

#include<fstream>
#include<sstream>
#include<math.h>

#include<armadillo>
#include<boost/property_tree/ptree.hpp>
#include<boost/property_tree/json_parser.hpp>
#include<boost/format.hpp>
#include<boost/math/special_functions/round.hpp>

#include"path_switcher.hpp"
#include"transpot_exceptions.hpp"
#include"transpot_constants.hpp"
#include"logger.hpp"

/**
 *@file transpot_io.cpp
 *@brief Functions for input and output (source)
 */

namespace{
	Config_data read_config_file(std::string config_filename){
		namespace pt = boost::property_tree;

		pt::ptree input;
		pt::read_json(config_filename.c_str(), input);

		Config_data cd;
		cd.config_filename = config_filename;
		cd.dft_filename = input.get<std::string>("dft_filename");
		cd.output_dir_name = input.get<std::string>("output_dir_name");
		cd.wavefunction_col_no = input.get<unsigned int>("wavefunction_col_no");
		
		const std::string spins = input.get<std::string>("spin_setting");
		if (spins == "up_up"){cd.spins = Spin_settings::up_up;
		} else if (spins == "up_down"){cd.spins = Spin_settings::up_down;
		} else if (spins == "down_up"){cd.spins = Spin_settings::down_up; 
		} else if (spins == "down_down"){cd.spins = Spin_settings::down_down;
		} else if (spins == "none"){cd.spins = Spin_settings::none;
		} else { throw transpot_io_exception("Unrecognized spin_setting.");}
		cd.atomic_number = input.get<unsigned int>("atomic_number");
		cd.n_bound = input.get<unsigned int>("n_bound");
		cd.l_bound = input.get<unsigned int>("l_bound");
		for (const auto& entry: input.get_child("mls_bound")){
			cd.mls_bound.push_back(entry.second.get_value<int>());
		}
		cd.energy_free = input.get<double>("energy_free");
		for (const auto& entry: input.get_child("ls_free")){
			cd.ls_free.push_back(entry.second.get_value<unsigned int>());
		}
		for (const auto& entry: input.get_child("mls_free")){
			cd.mls_free.push_back(entry.second.get_value<int>());
		}

		cd.acc_voltage = input.get<double>("acc_voltage");
		cd.x_pixel = input.get<unsigned int>("x_pixel");
		cd.y_pixel = input.get<unsigned int>("y_pixel");
		cd.qx_dim = cst::pi*input.get<double>("qx_dim");
		cd.qy_dim = cst::pi*input.get<double>("qy_dim");
		cd.samples_hankeltrafo = input.get<unsigned int>("samples_hankeltrafo");

		return cd;
	}


	double read_bound_energy(std::string dft_filename, unsigned int col_no){
		std::ifstream file(dft_filename.c_str());
		if (!file.is_open()){
			throw transpot_io_exception("Wavefunction input file not found.");
		}
		if (col_no<2){
			throw transpot_io_exception("Invalid wavefunctin_col_no - must be at least 2.\n");
		}

		//read all values of the bound energy
		std::string dummy_string;
		std::getline(file, dummy_string);
		file.close();

		//search through stringstream to find correct energy
		std::stringstream dummy_stream;
		dummy_stream.str(dummy_string);
		dummy_stream>>dummy_string; //skip "Radial"
		dummy_stream>>dummy_string; //skip "waveEigenvalue"
		double energy_bound;
		int n_skips = static_cast<int>(col_no)-1;	
		if (n_skips < 0){
			throw transpot_io_exception("Wavefunction_col_no too small.");
		}
		for (int i = 0; i<n_skips; i++){
			dummy_stream>>energy_bound;
		}

		return energy_bound;

	}


	void read_wavegen_file(Config_data& cd){
		const std::string dft_filename = cd.dft_filename;
		cd.energy_bound = read_bound_energy(dft_filename,cd.wavefunction_col_no);		
		cd.bound_wavefunction = Value_pairs(dft_filename,cd.wavefunction_col_no);
		cd.potential = Value_pairs(dft_filename,1);
	}


	void calculate_real_space_dimensions(Config_data& cd){
		cd.x_dim = 2.*cst::pi*cd.x_pixel/cd.qx_dim;
		cd.y_dim = 2.*cst::pi*cd.y_pixel/cd.qy_dim;
	}

	void set_amesh(Config_data& cd){
		const double at_num = static_cast<double>(cd.atomic_number);
		const double pot_size = static_cast<double>(cd.potential.size());
		cd.amesh = pow(45.*160.*at_num, 1./pot_size);
	}

	void warn_about_maximal_spatial_frequency(const Config_data& cd){
		const int no_of_wavegen_datapoints = static_cast<int>(cd.potential.size())-1;
		const double qabs_limit = cd.potential(no_of_wavegen_datapoints,0);
		logg::out()<<"Reciprocal space dimensions in config-file:\n";
		logg::out()<<"\t qx_dim = "<<cd.qx_dim<<" 1/Angstrom ";
		logg::out()<<"\t qy_dim = "<<cd.qy_dim<<" 1/Angstrom\n";
		const double qabs_max = sqrt(pow(cd.qx_dim,2)+pow(cd.qy_dim,2));
		logg::out()<<"\t  => Maximum qabs = "<<qabs_max<<" 1/Angstrom\n";
		logg::out()<<"Maximum absolute reciprocal space dimension in wavegen file:\n";
		logg::out()<<"\t qabs_limit = "<<qabs_limit<<" 1/Angstrom\n";
		if (qabs_limit<qabs_max){
			logg::out()<<"WARNING - It is possible that a lot of empty reciprocal space is sampled!\n";
		}

	}


}

Config_data read_input(std::string config_filename){
	Config_data cd = read_config_file(config_filename);
	read_wavegen_file(cd);
	calculate_real_space_dimensions(cd);
	set_amesh(cd);
	warn_about_maximal_spatial_frequency(cd);

	return cd;
}


namespace{

	///creates the output directory \p output_dir_name in \p work_path
	/*!An output_directory with the name \p output_dir_name is created
   	   in the work_path \p work_path. If the directory already exists, a one, two, etc. is
   	   appended to the directory name instead of a zero. If more than a hundred directories
   	   exist, an exception is thrown.
   	   @param[in] output_dir_name name of the created output directory (without appended zero)
   	   @param[in] work_path path from which the program is run*/
	boost::filesystem::path create_output_directory(std::string output_dir_name,
													   boost::filesystem::path work_path){
		namespace bfs = boost::filesystem;

		//output directory path if this is the first calculation with the output_dir_name
		unsigned int iteration=0;
		std::string no_output_dir_name = output_dir_name;
		bfs::path out_path= work_path / bfs::path(no_output_dir_name);

		//determine how many simulation result directories with the given output_dir_name are in the
		//working directory and set out_path accordingly
		while (exists(out_path)){
			iteration++;
			no_output_dir_name = output_dir_name+std::to_string(iteration);
			out_path = work_path / bfs::path(no_output_dir_name);
			if (iteration>100){
				std::string error_msg = "More than a hundred result directories of the same name!\n"
										"Clean up your working directory! -> Aborting";
				throw transpot_io_exception(error_msg);
			}
		}

		//create output directory
		if ( !bfs::create_directory(out_path) ){
			throw transpot_io_exception("Could not create output directory!->Aborting");
		}

		return out_path;
	}

	std::string generate_output_filename(const Transition_potential_result& tpd, const Config_data& cd){
		std::string out_filename = cst::elemental_abbrv.at(cd.atomic_number)+"_";
		out_filename+=boost::str(boost::format("%1%kV_")%cd.acc_voltage);
		out_filename+=cst::n_abbrv.at(cd.n_bound)+std::to_string(cd.l_bound)+std::to_string(tpd.m_bound);
		out_filename+=boost::str(boost::format("_%1%_")%cd.energy_free);
		out_filename+=std::to_string(tpd.l_free)+std::to_string(tpd.m_free)+"_";
		int x_extent_int = boost::math::iround(cd.x_dim);
		int y_extent_int = boost::math::iround(cd.y_dim);
		out_filename+=boost::str(boost::format("%1%x%2%.bin")%x_extent_int%y_extent_int);
		return out_filename;
	}

	void save_transition_potential(const std::string& filename, const Transition_potential_result& tpd,
			const Config_data& cd){
		arma::cx_cube additional_data(5,1,1);
		additional_data(0,0,0) = static_cast<double>(cd.atomic_number);
		additional_data(1,0,0) = static_cast<double>(cd.acc_voltage);
		const double energy_loss = (cd.energy_free-cd.energy_bound)*pow(10,-3);
		additional_data(2,0,0) = static_cast<double>(energy_loss);
		additional_data(3,0,0) = static_cast<double>(cd.x_dim);
		additional_data(4,0,0) = static_cast<double>(cd.y_dim);
		arma::field<arma::cx_cube> output(2);
		output(0) = additional_data;
		output(1) = tpd.transition_matrix;
		output.save(filename);
	}

	void write_logfile(const std::string& filename){
		std::ofstream out_file(filename);
		out_file<<logg::out().get_log();
		out_file.close();
	}

}


void save_results(const Transpot_shared_data& tsd){
	namespace bfs = boost::filesystem;

	bfs::path work_path = bfs::current_path();
	bfs::path out_path = create_output_directory(tsd.cd.output_dir_name, work_path);

	//switch current path to output directory
	//(switched back upon destruction of out_switcher)
	Path_switcher out_switcher(out_path);

	for (const auto& result : tsd.results){
		const std::string output_filename = generate_output_filename(result,tsd.cd);
		save_transition_potential(output_filename,result,tsd.cd);
	}


	bfs::path config_file_wpath( work_path / bfs::path(tsd.cd.config_filename) );
	bfs::path config_file_opath( out_path / bfs::path(tsd.cd.config_filename) );
	bfs::copy_file(config_file_wpath,config_file_opath,bfs::copy_option::fail_if_exists);

	write_logfile("transpot.log");

}







