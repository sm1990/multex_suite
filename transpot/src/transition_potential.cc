#include"transition_potential.hpp"

#include<complex>

#include"transpot_exceptions.hpp"
#include"transpot_constants.hpp"
#include"spline_fit.hpp"
#include"hankel_trafo.hpp"
#include"three_harmonics_integrated.hpp"
#include "prepare_dft_results.hpp"

/**
 *@file transition_potential.cc
 *@brief Class that calculates the transition potential (source)
 */

namespace{
	std::complex<double> pow_i_L(unsigned int L){
		if (L%2==0){
			return std::complex<double>(pow(-1,L/2),0);
		} else {
			return std::complex<double>(0, pow(-1,(L+3)/2));
		}
	}


	Value_pairs dr(const Value_pairs& xy_values){
		Spline_fit spline(xy_values);
		Value_pairs result(xy_values.size());
		for (unsigned int i=0; i<xy_values.size(); i++){
			const double x_value = xy_values(i,0);
			result(i,0) = x_value;
			result(i,1) = spline.dy_at(x_value);
		}

		return result;
	}


	Radial_integrals prepare_radial_integrals(unsigned int l_free, const Transpot_shared_data& tsd){
		Value_pairs radial_bound = prepare_bound_wave(tsd);
		Value_pairs radial_free = prepare_free_wave(l_free,tsd);
		Value_pairs potential = prepare_potential(tsd);
		const double l_bound = tsd.cd.l_bound;
		const double energy_free = tsd.cd.energy_free;
		const double energy_bound = tsd.cd.energy_bound;


		const double norm_bound = darwin_norm(energy_bound, radial_bound, tsd);
		const double norm_free = darwin_norm(energy_free, radial_free, tsd);
		

		const arma::colvec r = radial_bound.column_as_colvec(0);
		const Value_pairs dr_radial_bound = dr(radial_bound);
		

		const Value_pairs integrand_I_R0 = radial_free*radial_bound;
		const Value_pairs integrand_I_R1 = radial_free*(-1)*(potential-energy_bound)*radial_bound;
		const Value_pairs integrand_I_R2 = radial_free*(dr_radial_bound - l_bound/r*radial_bound);
		const Value_pairs integrand_I_R3 = radial_free*(dr_radial_bound + (l_bound+1)/r*radial_bound);


		const unsigned int samples = tsd.cd.samples_hankeltrafo;
		return Radial_integrals{Hankel_trafo(Spline_fit(integrand_I_R0),samples),
								Hankel_trafo(Spline_fit(integrand_I_R1),samples),
								Hankel_trafo(Spline_fit(integrand_I_R2),samples),
								Hankel_trafo(Spline_fit(integrand_I_R3),samples),
								norm_bound, norm_free};
	}


	double calculate_qz(const Transpot_shared_data& tsd){
		double e_diff = tsd.cd.energy_free-tsd.cd.energy_bound;
		e_diff *= pow(10,-3); //energy difference in keV
		const double U_acc = tsd.cd.acc_voltage;
		const double wavelen_0=0.1*cst::h/sqrt(2.*cst::m_e*U_acc*0.1*cst::el*(1+0.1*(U_acc*cst::el)/(2.*cst::m_e*cst::c2)));
		const double wavelen_loss = 0.1*cst::h/sqrt(2.*cst::m_e*(U_acc-e_diff)*0.1*cst::el*(1+0.1*((U_acc-e_diff)*cst::el)/(2.*cst::m_e*cst::c2)));
		const double q_0 = 2.*M_PI/wavelen_0;
		const double q_loss = 2.*M_PI/wavelen_loss;
		const double delta_q = (q_0-q_loss);
		const double qz = delta_q;
		return qz;
	}


	inline double calc_energy_loss_by_hbarc(const Transpot_shared_data& tsd){
		return (tsd.cd.energy_free-tsd.cd.energy_bound)/cst::hbarc;
	}


	int calc_periodic_array_coordinate(int index, unsigned int size){
		int new_index = 0;
		if (index<0){
			new_index = size + index;
		} else if (static_cast<unsigned int>(abs(index))>=size){
			new_index = index - size;
		} else {
			new_index = index;
		}

		if ( (new_index < 0) || (new_index >= static_cast<int>(size)) ){
			throw transpot_exception("Index would require multiple shifts to fit into array.\n");
		}
		return new_index;
	}


	Reciprocal_space_grids prepare_q_grids(const Transpot_shared_data& tsd){
		int qx_start=0, qx_end=0;
		int qy_start=0, qy_end=0;
		int nx = static_cast<int>(tsd.cd.x_pixel);
		int ny = static_cast<int>(tsd.cd.y_pixel);
		if (nx%2 == 0){
			qx_start = -nx/2;
			qx_end = -qx_start-1;
		} else {
			qx_start = -(nx-1)/2;
			qx_end = -qx_start;
		}
		if (ny%2 == 0){
			qy_start = -ny/2;
			qy_end = -qy_start-1;
		} else {
			qy_start = -(ny-1)/2;
			qy_end = -qy_start;
		}

		const double qx_step = tsd.cd.qx_dim/tsd.cd.x_pixel;
		arma::rowvec qx_grid(nx);
		for (int iqx = qx_start; iqx<=qx_end; iqx++){
			const double qx = iqx*qx_step;
			const int x_coordinate = calc_periodic_array_coordinate(iqx,nx);
			qx_grid(x_coordinate) = qx;
		}

		const double qy_step = tsd.cd.qy_dim/tsd.cd.y_pixel;
		arma::colvec qy_grid(ny);
		for (int iqy = qy_start; iqy<=qy_end; iqy++){
			const double qy = iqy*qy_step;
			const int y_coordinate = calc_periodic_array_coordinate(iqy,ny);
			qy_grid(y_coordinate) = qy;
		}

		const double qz = calculate_qz(tsd);
		arma::mat qabs_grid(ny,nx);
		for (int iqx = qx_start; iqx<=qx_end; iqx++){
			for (int iqy = qy_start; iqy<=qy_end; iqy++){
				const double qx = iqx*qx_step;
				const double qy = iqy*qy_step;
				const int x_coordinate = calc_periodic_array_coordinate(iqx,nx);
				const int y_coordinate = calc_periodic_array_coordinate(iqy,ny);
				qabs_grid(y_coordinate,x_coordinate) = sqrt(pow(qx,2)+pow(qy,2)+pow(qz,2));
			}
		}

		return Reciprocal_space_grids{qz,qx_grid,qy_grid,qabs_grid};
	}

}



Transition_potential::Transition_potential(int m_bound, unsigned int l_free, const Transpot_shared_data& tsd)
							: spins(tsd.cd.spins),rad_ints(prepare_radial_integrals(l_free,tsd)),
							  energy_loss_by_hbarc(calc_energy_loss_by_hbarc(tsd)),
							  q_grids(prepare_q_grids(tsd)),l_bound(static_cast<int>(tsd.cd.l_bound)),
							  m_bound(m_bound), l_free(l_free){}





arma::mat Transition_potential::calc_radial_integral_on_q_grid(unsigned int L, Rad_int_types int_type){
	Value_pairs transform_results;
	switch(int_type){
	case  Rad_int_types::I_R0:
		transform_results = rad_ints.I_R0.perform_hankel_trafo(L);
		break;
	case Rad_int_types::I_R1:
		transform_results = rad_ints.I_R1.perform_hankel_trafo(L);
		break;
	case Rad_int_types::I_R2:
		transform_results = rad_ints.I_R2.perform_hankel_trafo(L);
		break;
	case Rad_int_types::I_R3:
		transform_results = rad_ints.I_R3.perform_hankel_trafo(L);
		break;
	}

	Spline_fit fitted_transform_results(transform_results);
	const arma::mat& qabs_grid = q_grids.qabs_grid;
	const unsigned int nx = static_cast<int>(qabs_grid.n_cols);
	const unsigned int ny = static_cast<int>(qabs_grid.n_rows);
	arma::mat radial_integral(ny,nx);
	for (unsigned int ix=0; ix<nx; ix++){
		for (unsigned int iy=0; iy<ny; iy++){
			const double qabs = qabs_grid(iy,ix);
			radial_integral(iy,ix) = fitted_transform_results.y_at(qabs);
		}
	}


	return radial_integral;
}


void Transition_potential::calc_transition_potential(int m_free, Transpot_shared_data& tsd){
	const size_t nx = q_grids.qabs_grid.n_cols;
	const size_t ny = q_grids.qabs_grid.n_rows;
	arma::mat prefactor(ny,nx);
	if (spins == Spin_settings::none){
		prefactor = -cst::cemuh_0*arma::pow(q_grids.qabs_grid,-2);
	} else {
		const double darwin_norm = rad_ints.norm_bound*rad_ints.norm_free;
		prefactor = cst::cemuh_0*darwin_norm*arma::ones(ny,nx);
		prefactor /= (pow(energy_loss_by_hbarc,2)-arma::pow(q_grids.qabs_grid,2));
	}

	Integrals integrals = calc_integrals(m_free);
	const arma::cx_mat& I_0 = integrals.I_0;
	arma::cx_cube result;
	if (spins == Spin_settings::none){
		result = arma::cx_cube(ny,nx,1);
		result.slice(0) = prefactor%I_0;
	} else {
		const arma::cx_mat& I_Delta = integrals.I_Delta;
		const arma::cx_mat& I_x = integrals.I_x;
		const arma::cx_mat& I_y = integrals.I_y;
		const arma::cx_mat& I_z = integrals.I_z;

		const double qz = q_grids.qz;
		const arma::mat qx = arma::ones(ny,1)*q_grids.qx_grid;
		const arma::mat qy = q_grids.qy_grid*arma::ones(1,nx);
		result = arma::cx_cube(ny,nx,4);
		const std::complex<double> i = cst::i;
		if (spins == Spin_settings::up_up){
			result.slice(0) = prefactor%(I_0-cst::qhbard2m_ec*(I_Delta+(qy+i*qx)%(I_x+i*I_y)+i*qz*I_z));
			result.slice(1) = prefactor%(-i*cst::hbard2m_ec*(2.*I_x+(qy+i*qx)%I_0));
			result.slice(2) = prefactor%(-i*cst::hbard2m_ec*(2.*I_y+(i*qy-qx)%I_0));
			result.slice(3) = prefactor%(-i*cst::hbard2m_ec*(2.*I_z+i*qz*I_0));
		} else if (spins == Spin_settings::down_down){
			result.slice(0) = prefactor%(I_0-cst::qhbard2m_ec*(I_Delta+(i*qx-qy)%(I_x-i*I_y)+i*qz*I_z));
			result.slice(1) = prefactor%(-i*cst::hbard2m_ec*(2.*I_x+(i*qx-qy)%I_0));
			result.slice(2) = prefactor%(-i*cst::hbard2m_ec*(2.*I_y+(qx+i*qy)%I_0));
			result.slice(3) = prefactor%(-i*cst::hbard2m_ec*(2.*I_z+i*qz*I_0));
		} else if (spins == Spin_settings::up_down){
			result.slice(0) = prefactor%(cst::qhbard2m_ec*((i*qx+qy)%I_z-qz*(i*I_x+I_y)));
			result.slice(1) = prefactor%(cst::hbard2m_ec*qz*I_0);
			result.slice(2) = prefactor%(-i*cst::hbard2m_ec*qz*I_0);
			result.slice(3) = prefactor%(cst::hbard2m_ec*(i*qy-qx)%I_0);
		} else if (spins == Spin_settings::down_up){
			result.slice(0) = prefactor%(cst::qhbard2m_ec*((qy-i*qx)%I_z+qz*(i*I_x-I_y)));
			result.slice(1) = prefactor%(-cst::hbard2m_ec*qz*I_0);
			result.slice(2) = prefactor%(-i*cst::hbard2m_ec*qz*I_0);
			result.slice(3) = prefactor%(cst::hbard2m_ec*(i*qy+qx)%I_0);
		}
	}

	Transition_potential_result tpd{result,m_bound,l_free,m_free};
	tsd.results.emplace_back(tpd);
}



Integrals Transition_potential::calc_integrals(int m_free){
	Spherical_harmonics Y_LMs(q_grids);
	const size_t nx = q_grids.qabs_grid.n_cols;
	const size_t ny = q_grids.qabs_grid.n_rows;

	//I_O and I_Delta:
	Three_harmonics_integrated harmonics_integral_I_0D(l_free,m_free,l_bound,m_bound);
	const auto possible_LMs_I_0D = harmonics_integral_I_0D.get_possible_LMs();
	arma::cx_mat I_0(ny,nx,arma::fill::zeros);
	arma::cx_mat I_Delta(ny,nx,arma::fill::zeros);
	for (const auto& [L,M] : possible_LMs_I_0D){
		const arma::mat I_R0 = calc_radial_integral_on_q_grid(L, Rad_int_types::I_R0);
		const arma::cx_mat Y_LM_ast = arma::conj(Y_LMs.calc_spherical_harmonic(L,M));
		const double ls_ms__l_m = harmonics_integral_I_0D.value_for(L,M);
		I_0 += pow_i_L(L)*Y_LM_ast%I_R0*ls_ms__l_m;
		if (spins != Spin_settings::none){
			const arma::mat I_R1 = calc_radial_integral_on_q_grid(L, Rad_int_types::I_R1);
			I_Delta +=  pow_i_L(L)*Y_LM_ast%I_R1*ls_ms__l_m;
		}
	}
	I_0 *= 4.*cst::pi;
	if (spins!= Spin_settings::none){
		I_Delta *= 4.*cst::pi*(-cst::m_e2dhbarh2);
	}

	Integrals integrals;
	if (spins == Spin_settings::none){
		integrals.I_0 = I_0;
		return integrals;
	} else {
		//I_x and I_y:
		Three_harmonics_integrated harmonics_integral_I_xy_1(l_free, m_free, l_bound+1, m_bound+1);
		Three_harmonics_integrated harmonics_integral_I_xy_2(l_free, m_free, l_bound+1, m_bound-1);
		Three_harmonics_integrated harmonics_integral_I_xy_3(l_free, m_free, l_bound-1, m_bound+1);
		Three_harmonics_integrated harmonics_integral_I_xy_4(l_free, m_free, l_bound-1, m_bound-1);
		auto possible_LMs_I_xy_term_a =  harmonics_integral_I_xy_1.get_possible_LMs();
		possible_LMs_I_xy_term_a.merge(harmonics_integral_I_xy_2.get_possible_LMs());
		auto possible_LMs_I_xy_term_b = harmonics_integral_I_xy_3.get_possible_LMs();
		possible_LMs_I_xy_term_b.merge(harmonics_integral_I_xy_4.get_possible_LMs());

		arma::cx_mat I_x(ny,nx,arma::fill::zeros);
		arma::cx_mat I_y(ny,nx,arma::fill::zeros);

		for (const auto& [L,M] : possible_LMs_I_xy_term_a){
			arma::mat I_R2 = calc_radial_integral_on_q_grid(L, Rad_int_types::I_R2);
			const arma::cx_mat Y_LM_ast = arma::conj(Y_LMs.calc_spherical_harmonic(L,M));
			const double ls_ms__lp1_mp1 = harmonics_integral_I_xy_1.value_for(L,M);
			const double ls_ms__lp1_mm1 = harmonics_integral_I_xy_2.value_for(L,M);
			I_R2 /= sqrt((2.*l_bound+3.)*(2.*l_bound+1.));
			const double braket_I_x = A('+',-1)*ls_ms__lp1_mm1 + A('+',+1)*ls_ms__lp1_mp1;
			const double braket_I_y = A('+',-1)*ls_ms__lp1_mm1 - A('+',+1)*ls_ms__lp1_mp1;
			I_x += pow_i_L(L)*Y_LM_ast%I_R2*braket_I_x;
			I_y += pow_i_L(L)*Y_LM_ast%I_R2*braket_I_y;
		}
		for (const auto& [L,M] : possible_LMs_I_xy_term_b){
			arma::mat I_R3 = calc_radial_integral_on_q_grid(L, Rad_int_types::I_R3);
			const arma::cx_mat Y_LM_ast = arma::conj(Y_LMs.calc_spherical_harmonic(L,M));
			const double ls_ms__lm1_mp1 = harmonics_integral_I_xy_3.value_for(L,M);
			const double ls_ms__lm1_mm1 = harmonics_integral_I_xy_4.value_for(L,M);
			I_R3 /= sqrt((2.*l_bound+1)*(2.*l_bound-1));
			const double braket_I_x = A('-',-1)*ls_ms__lm1_mm1 + A('-',+1)*ls_ms__lm1_mp1;
			const double braket_I_y = A('-',-1)*ls_ms__lm1_mm1 - A('-',+1)*ls_ms__lm1_mp1;
			I_x += pow_i_L(L)*Y_LM_ast%I_R3*braket_I_x;
			I_y += pow_i_L(L)*Y_LM_ast%I_R3*braket_I_y;
		}
		I_x *= 4.*cst::pi*0.5;
		I_y *= 4.*cst::pi*0.5*cst::i;

		//I_z:
		Three_harmonics_integrated harmonics_integral_I_z_1(l_free, m_free, l_bound+1, m_bound);
		Three_harmonics_integrated harmonics_integral_I_z_2(l_free, m_free, l_bound-1, m_bound);
		const auto possible_LMs_I_z_term_a = harmonics_integral_I_z_1.get_possible_LMs();
		const auto possible_LMs_I_z_term_b = harmonics_integral_I_z_2.get_possible_LMs();
		arma::cx_mat I_z(ny,nx,arma::fill::zeros);
		for (const auto& [L,M] : possible_LMs_I_z_term_a){
			arma::mat I_R2 = calc_radial_integral_on_q_grid(L, Rad_int_types::I_R2);
			const arma::cx_mat Y_LM_ast = arma::conj(Y_LMs.calc_spherical_harmonic(L,M));
			const double ls_ms__lp1_m = harmonics_integral_I_z_1.value_for(L,M);
			I_R2 /= sqrt((2.*l_bound+3.)*(2.*l_bound+1.));
			I_z += pow_i_L(L)*Y_LM_ast%I_R2*ls_ms__lp1_m*A('+',0);
		}
		for (const auto& [L,M] : possible_LMs_I_z_term_b){
			arma::mat I_R3 = calc_radial_integral_on_q_grid(L, Rad_int_types::I_R3);
			const arma::cx_mat Y_LM_ast = arma::conj(Y_LMs.calc_spherical_harmonic(L,M));
			const double ls_ms__lm1_m = harmonics_integral_I_z_2.value_for(L,M);
			I_R3 /= sqrt((2.*l_bound+1.)*(2.*l_bound-1.));
			I_z += pow_i_L(L)*Y_LM_ast%I_R3*ls_ms__lm1_m*A('-',0);
		}
		I_z *= 4.*cst::pi;

		integrals.I_0 = I_0;
		integrals.I_Delta = I_Delta;
		integrals.I_x = I_x;
		integrals.I_y = I_y;
		integrals.I_z = I_z;

		return integrals;
	}

}


double Transition_potential::A(char sign, int k){
	double result=0;
	switch(sign){
	case '+':
		switch(k){
		case 1:
			result = sqrt((l_bound+m_bound+2)*(l_bound+m_bound+1));
			break;
		case 0:
			result = sqrt((l_bound+m_bound+1)*(l_bound-m_bound+1));
			break;
		case -1:
			result = -sqrt((l_bound-m_bound+2.)*(l_bound-m_bound+1.));
			break;
		default:
			throw transpot_exception("Unrecognized symbol in A-function.");
		}
		break;
	case '-':
		switch(k){
		case 1:
			result = -sqrt((l_bound-m_bound)*(l_bound-m_bound-1.));
			break;
		case 0:
			result = sqrt((l_bound+m_bound)*(l_bound-m_bound));
			break;
		case -1:
			result = sqrt((l_bound+m_bound)*(l_bound+m_bound-1.));
			break;
		default:
			throw transpot_exception("Unrecognized symbol in A-function.");
		}
		break;
	default:
		throw transpot_exception("Unrecognized symbol in A-function.");
	}

	return result;
}









