#include"hankel_trafo.hpp"

#include<fftw3.h>
#include<cmath>
#include<cassert>

#include"transpot_exceptions.hpp"

/**
 *@file hankel_trafo.cc
 *@brief Class that performs Hankel transform (source)
 */



namespace{
	std::vector<std::array<double,13>> cos_sin_trafo(const Spline_fit& spline, unsigned int samples,
								double min, double max){

		const double spline_min_x = spline.min_x();
		const double spline_max_x = spline.max_x();
		if ( (spline_min_x<0) || (spline_min_x>min) || (spline_max_x<max) || (min>max) ){
			throw transpot_exception("Spline fit not suitable for Hankel transform on chosen interval.");
		}


		//get arrays ready for Fourier transform
		double *ft_array_cos,  *ft_array_sin;
		double *dft_array_cos, *dft_array_sin;
		double *d2ft_array_cos, *d2ft_array_sin;

		ft_array_cos = static_cast<double*>(fftw_malloc(samples*sizeof(double)));
		ft_array_sin = static_cast<double*>(fftw_malloc(samples*sizeof(double)));
		dft_array_cos = static_cast<double*>(fftw_malloc(samples*sizeof(double)));
		dft_array_sin = static_cast<double*>(fftw_malloc(samples*sizeof(double)));
		d2ft_array_cos = static_cast<double*>(fftw_malloc(samples*sizeof(double)));
		d2ft_array_sin = static_cast<double*>(fftw_malloc(samples*sizeof(double)));



		fftw_plan cos_trafo = fftw_plan_r2r_1d(samples, ft_array_cos, ft_array_cos,
									   FFTW_REDFT11, FFTW_MEASURE);
		fftw_plan sin_trafo = fftw_plan_r2r_1d(samples, ft_array_sin, ft_array_sin,
									   FFTW_RODFT11, FFTW_MEASURE);
		fftw_plan d_cos_trafo = fftw_plan_r2r_1d(samples, dft_array_cos, dft_array_cos,
										FFTW_RODFT11, FFTW_MEASURE);
		fftw_plan d_sin_trafo = fftw_plan_r2r_1d(samples, dft_array_sin, dft_array_sin,
										FFTW_REDFT11, FFTW_MEASURE);
		fftw_plan d2_cos_trafo = fftw_plan_r2r_1d(samples, d2ft_array_cos, d2ft_array_cos,
											FFTW_REDFT11, FFTW_MEASURE);
		fftw_plan d2_sin_trafo = fftw_plan_r2r_1d(samples, d2ft_array_sin, d2ft_array_sin,
										 FFTW_RODFT11, FFTW_MEASURE);


		//calculate step size for linear sampling grid
		double step_size = (max-min)/samples;

		//fill arrays with already scaled data (*0.5*stepSize) and perform cosine/sine transform
		for (unsigned int i=0; i<samples; ++i){
			double x = (static_cast<double>(i)+0.5)*step_size+min;
			double yScaled = spline.y_at(x)*0.5*step_size*pow(x,2);
			ft_array_cos[i] = yScaled;
			ft_array_sin[i] = yScaled;
			dft_array_cos[i] = -yScaled*x;
			dft_array_sin[i] = yScaled*x;
			d2ft_array_cos[i] = -yScaled*pow(x,2);
			d2ft_array_sin[i] = -yScaled*pow(x,2);
		}
		fftw_execute(cos_trafo);
		fftw_execute(sin_trafo);
		fftw_execute(d_cos_trafo);
		fftw_execute(d_sin_trafo);
		fftw_execute(d2_cos_trafo);
		fftw_execute(d2_sin_trafo);
		
	
		//fill final array with coefficients used for the hankel transform
		//concept: trafoData[i][..]=k_i, c_0even, c_1even, c2even, c3even, c4even, c5even,
		//						         c_0odd, c_1odd, c_2odd, c_3odd, c_4odd, c_5odd
		std::vector<std::array<double,13>> trafo_data(samples-1);
		for (unsigned int i=0; i<(samples-1); i++){
				double k_0 = (static_cast<double>(i)+0.5)*M_PI/(max-min);	 //multiply by M_PI to use
				double delta_k = M_PI/(max-min);								 //same FT-convention as in \cite MyThesis
				double F_0e = ft_array_cos[i], F_1e = ft_array_cos[i+1];
				double dF_0e = dft_array_cos[i], dF_1e = dft_array_cos[i+1];
				double d2F_0e = d2ft_array_cos[i], d2F_1e = d2ft_array_cos[i+1];
				double F_0o = ft_array_sin[i], F_1o = ft_array_sin[i+1];
				double dF_0o = dft_array_sin[i], dF_1o = dft_array_sin[i+1];
				double d2F_0o = d2ft_array_sin[i], d2F_1o = d2ft_array_sin[i+1];

				trafo_data[i][0] = k_0;																	//k
				trafo_data[i][1] = F_0e;																//c_0(even)
				trafo_data[i][2] = dF_0e;																//c_1(even)
				trafo_data[i][3] = d2F_0e/2.;															//c_2(even)
				trafo_data[i][4] = 10.*(F_1e-F_0e)/pow(delta_k,3)-(4.*dF_1e+6.*dF_0e)/pow(delta_k,2)
									+(d2F_1e-3.*d2F_0e)/(2.*delta_k);									//c_3(even)
				trafo_data[i][5] = -15.*(F_1e-F_0e)/pow(delta_k,4)+(7.*dF_1e+8.*dF_0e)/pow(delta_k,3)
									-(2.*d2F_1e-3.*d2F_0e)/(2.*pow(delta_k,2));							//c_4(even)
				trafo_data[i][6] = 6.*(F_1e-F_0e)/pow(delta_k,5)-3.*(dF_1e+dF_0e)/pow(delta_k,4)
									+(d2F_1e-d2F_0e)/(2.*pow(delta_k,3));								//c_5(even)

				trafo_data[i][7] = F_0o;																//c_0(odd)
				trafo_data[i][8] = dF_0o;																//c_1(odd)
				trafo_data[i][9] = d2F_0o/2.;															//c_2(odd)
				trafo_data[i][10] = 10.*(F_1o-F_0o)/pow(delta_k,3)-(4.*dF_1o+6.*dF_0o)/pow(delta_k,2)
									+(d2F_1o-3.*d2F_0o)/(2.*delta_k);									//c_3(odd)
				trafo_data[i][11] = -15.*(F_1o-F_0o)/pow(delta_k,4)+(7.*dF_1o+8.*dF_0o)/pow(delta_k,3)
									-(2.*d2F_1o-3.*d2F_0o)/(2.*pow(delta_k,2));							//c_4(odd)
				trafo_data[i][12] = 6.*(F_1o-F_0o)/pow(delta_k,5)-3.*(dF_1o+dF_0o)/pow(delta_k,4)
									+(d2F_1o-d2F_0o)/(2.*pow(delta_k,3));								//c_5(odd)
			}
	
	
	
		

		//clean-up
		fftw_free(ft_array_cos);
		fftw_free(ft_array_sin);
		fftw_free(dft_array_cos);
		fftw_free(dft_array_sin);
		fftw_free(d2ft_array_cos);
		fftw_free(d2ft_array_sin);
		fftw_destroy_plan(cos_trafo);
		fftw_destroy_plan(sin_trafo);
		fftw_destroy_plan(d_cos_trafo);
		fftw_destroy_plan(d_sin_trafo);
		fftw_destroy_plan(d2_cos_trafo);
		fftw_destroy_plan(d2_sin_trafo);

		return trafo_data;
	}

	
	std::vector<std::array<double,13>> cos_sin_trafo(const Spline_fit& spline, unsigned int samples){
		return cos_sin_trafo(spline, samples, spline.min_x(), spline.max_x());
	}
	

	std::vector<double> gammaIntegrals(const Spline_fit& spline, unsigned int samples){
		unsigned int integral_samples = 2*samples;	//increase sampling for function integration
		double stepSize = (spline.max_x()-spline.min_x())/integral_samples;
		std::vector<double> gammas(6,0);
		for (unsigned int i=0; i<integral_samples; i++){
			double x = (static_cast<double>(i)+0.5)*stepSize+spline.min_x();
			double y = spline.y_at(x);
			for (unsigned int j=0; j<6; j++){
				gammas[j] += pow(x,j+2)*y*stepSize;
			}
		}

		return gammas;
	}

	
	
	long int double_factorial( int n){
		long int result = 1;
		while (n>1){
			result *= n;
			n=n-2;
		}
		return result;
	}
	

	long int factorial(int n){
		long int result = 1;
		while (n>1){
			result *= n;
			n=n-1;
		}
		return result;
	}



	double spline_integral(unsigned int m, unsigned int n, double k_a, double k_b){
		double nd = static_cast<double>(n);
		double A = pow(k_a,n+1)/(nd+1.);
		if (m==0){
			double B = pow(k_b,n+1)/(nd+1.);
			return A-B;
		} else {
			double md = static_cast<double>(m);
			return A*pow(k_a-k_b,m)-md/(nd+1.)*spline_integral(m-1,n+1,k_a,k_b);
		}
	}
}




Hankel_trafo::Hankel_trafo(const Spline_fit& spline, unsigned int samples, double min, double max)
		: trafo_data(cos_sin_trafo(spline, samples, min, max)), gammas(gammaIntegrals(spline, samples)) {}


Hankel_trafo::Hankel_trafo(const Spline_fit& spline, unsigned int samples)
		: trafo_data(cos_sin_trafo(spline, samples)), gammas(gammaIntegrals(spline, samples)) {}



Value_pairs Hankel_trafo::perform_hankel_trafo(unsigned int l){

	const unsigned int result_size = static_cast<unsigned int>(trafo_data.size());
	std::vector<double> hankeltransform(result_size, 0);

	//check which integrals have to be calculated
	unsigned int save_size = static_cast<unsigned int>(integral_storage.size());
	
	//calculate the integrals I_n(k) from [1] (if not previously calculated)
	if (save_size <= l){
		for (unsigned int i=save_size; i<=l; i++){
			integral_storage.push_back(integral(i));
		}
	}




	//determine whether l is even or odd and then sum the integrals accordingly (formula see [1])
	if (l%2 == 0){
		unsigned int n = l/2;
		for (unsigned int j=0;j<=n;j++){
			double prefactor = pow(-1,j)*static_cast<double>(double_factorial(2*n+2*j-1));
			prefactor /= (static_cast<double>(double_factorial(2*n-2*j))*static_cast<double>(factorial(2*j)));
			for (unsigned int i=0; i<result_size; i++){
				hankeltransform[i] += prefactor*integral_storage[2*j][i];
			}
		}
	} else {
		unsigned int n=(l-1)/2;
		for (unsigned int j=0; j<=n; j++){
			double prefactor = pow(-1,j)*static_cast<double>(double_factorial(2*n+2*j+1));
			prefactor /= (static_cast<double>(double_factorial(2*n-2*j))*static_cast<double>(factorial(2*j+1)));
			for (unsigned int i=0; i<result_size; i++){
				hankeltransform[i] += prefactor*integral_storage[2*j+1][i];
			}
		}
	}
	
	
	Value_pairs result(result_size);
	for (unsigned int i=0;i<result_size;i++){
		result(i,0) = trafo_data[i][0];      //divide by 2.*M_PI here if you want to return to k=1/x convention
		result(i,1) = hankeltransform[i];
	}



	return result;
}


std::vector<double> Hankel_trafo::integral(unsigned int m){
	
	const unsigned int result_size = static_cast<unsigned int>(trafo_data.size());
	std::vector<double> integral_solution(result_size);
	
	//determine whether l is even or not and use corresponding cosine- / sine-transform data
	//and do integration for small k values
	double md = static_cast<double>(m);	
	int skip_value = 1;		//used to skip even c's in trafoData if integral with uneven n is required

	if (m%2 == 0){
		double k_0 = trafo_data[0][0];
		integral_solution[0] = (gammas[0]*pow(k_0,md+1)/(md+1)-gammas[2]*pow(k_0,md+3)*0.5/(md+3)
								+gammas[4]*pow(k_0,md+5)/(24*(md+5)));  
	} else {
		skip_value = 7;
		double k_0 = trafo_data[0][0];
		integral_solution[0] = (gammas[1]*pow(k_0,md+2)/(md+2)-gammas[3]*pow(k_0,md+4)/(6*(md+4))
								+gammas[5]*pow(k_0,md+6)/(120*(md+6))); 
	}

	//perform the rest of the integration
	for (unsigned int ki=1; ki<result_size; ki++){
		//sum over the components of the integrated spline polynome; 
		//take care to integrate the correct polynome c_0+c_1(t-t_0)+c_2(t-t_0)^2+c_3(t-t_0)^3+... instead of 
		//c_0+c_1t+c_2t^2+c_3t^3+... as done in [1];
		//trafoData[ki][..] = k,c_0(even),c_1(even),c_2(even),c_3(even),c_4(even),c_5(even),
		//						c_0(odd),c_1(odd),c_2(odd),c_3(odd),c_4(odd),c_5(odd)
		double k_lower = trafo_data[ki-1][0];
		double k_upper = trafo_data[ki][0];
		integral_solution[ki]=integral_solution[ki-1];


		//integrate spline to get value of the integral between two result_size;
		//the analytical spolution of the integrated spline polynome is computed by the spline_integral routine
		unsigned int polyOrder = 5;
		for (unsigned int j=0; j<=polyOrder; j++){
			integral_solution[ki] += spline_integral(j, m, k_upper, k_lower)*trafo_data[ki-1][skip_value+j];
		}
		
		//alternative way to compute the analytical solution of the spline polynomes integral
		/*double deltak = kUpper-kLower;
		double A = pow(kUpper,md+1)/(md+1.);		
		double B = pow(kLower,md+1)/(md+1.);
		integralSolution[ki] += (A-B)*trafoData[ki-1][skipValue];
		
		integralSolution[ki] += (A*deltak-1./(md+2.)*(kUpper*A-kLower*B))*trafoData[ki-1][skipValue+1];
		
		integralSolution[ki] += (A*pow(deltak,2)-A*kUpper*2./(md+2.)*deltak+2./((md+2.)*(md+3.))*(A*pow(kUpper,2)-B*pow(kLower,2)))*trafoData[ki-1][skipValue+2];
		
		integralSolution[ki] += (A*pow(deltak,3)-A*kUpper*3./(md+2.)*pow(deltak,2)+A*pow(kUpper,2)*6./((md+2.)*(md+3.))*deltak
									-6./((md+2.)*(md+3.)*(md+4.))*(A*pow(kUpper,3)-B*pow(kLower,3)))*trafoData[ki-1][skipValue+3];

		integralSolution[ki] += (A*pow(deltak,4)-4.*A*kUpper/(md+2.)*pow(deltak,3)+12.*A*pow(kUpper,2)/((md+2.)*(md+3.))*pow(deltak,2)
									-24.*A*pow(kUpper,3)/((md+2)*(md+3)*(md+4))*deltak
									+24.*(A*pow(kUpper,4)-B*pow(kLower,4))/((md+2)*(md+3)*(md+4)*(md+5)))*trafoData[ki-1][skipValue+4];

		integralSolution[ki] += (A*pow(deltak,5)-5.*A*kUpper/(md+2.)*pow(deltak,4)+20.*A*pow(kUpper,2)/((md+2.)*(md+3.))*pow(deltak,3)
									-60.*A*pow(kUpper,3)/((md+2.)*(md+3.)*(md+4.))*pow(deltak,2)
									+120.*A*pow(kUpper,4)/((md+2.)*(md+3.)*(md+4.)*(md+5.))*deltak
									-120.*(A*pow(kUpper,5)-B*pow(kLower,5))/((md+2.)*(md+3.)*(md+4.)*(md+5.)*(md+6.)))*trafoData[ki-1][skipValue+5];*/
	}
	

	//divide by k^(l+1) to get integral value at k	
	for (unsigned int ki=0; ki<result_size; ki++){
		double k = trafo_data[ki][0];
		integral_solution[ki] = integral_solution[ki]/pow(k,md+1);
	}
	
	
	return integral_solution;
}



