#ifndef transpot_shared_data_HPP
#define transpot_shared_data_HPP

#include<string>
#include<vector>

#include<armadillo>

#include"value_pairs.hpp"

/**
 *@file transpot_shared_data.hpp
 *@brief Struct to share input data and results throughout transpot
 */

///Settings for the spin of the atomic electron before and after the ionization
enum class Spin_settings {up_up, down_down, up_down, down_up, none};

///Data read from the config file and from the DFT file
struct Config_data{
	std::string config_filename;			///<Name of config file
	std::string dft_filename;				///<Name of file with DFT results from wavegen
	std::string output_dir_name;			///<Name of the output directory
	unsigned int wavefunction_col_no;		///<Column number of the wave function in the wavegen file with the qunatum number \p n_bound and \p l_bound

	Spin_settings spins;					///<Spin setting of the atomic electron (before and after ionization)
	unsigned int atomic_number;				///<Amount of protons in the nucleus
	unsigned int n_bound;					///<Principal quantum number of the bound atomic electron
	unsigned int l_bound;					///<Azimuthal quantum number of the bound atomic electron
	std::vector<int> mls_bound;				///<Magnetic quantum numbers of the bound atomic electron for which the transition potentials are to be calculated
	double energy_free;						///<Energy of the atomic electron after ionization (=free atomic electron)
	std::vector<unsigned int> ls_free;		///<Azimuthal quantum numbers of the free atomic electron for which the transition potentials are to be calculated
	std::vector<int> mls_free;				///<Magnetic quantum numbers of the free atomic electron for which the transition potentials are to be calculated

	double acc_voltage;						///<Voltage used to accelerate the beam electron (i.e. the electron that is causing the ionization)
	unsigned int x_pixel;					///<Amount of pixel in the transition potential in x-direction
	unsigned int y_pixel;					///<Amount of pixel in the transition potential in y-direction
	double qx_dim;							///<Reciprocal space x-dimension of the transition potential
	double qy_dim;							///<Reciprocal space y-dimension of the transition potential
	double x_dim;							///<Real space x-dimension of the transition potential
	double y_dim;							///<Real space y-dimension of the transition potential
	double amesh;							///<amesh parameter used in the calculation of the wave function of the free atomic electron (see wavegen)
	unsigned int samples_hankeltrafo;		///<Amount of times the spline fitted wave function is sampled for the Hankel transform

	double energy_bound;					///<Energy of the bound atomic electron (from DFT file)
	Value_pairs bound_wavefunction;			///<xy-values of the wave function of the bound atomic electron (from DFT file)
	Value_pairs potential;					///<xy-values of the atomic potential (from DFT file)
};


///Data for a single transition potential
struct Transition_potential_result{
	arma::cx_cube transition_matrix;		///<Transition (four-)potential - SI-unit is \f$\text{\AA}^2\sqrt{eV}/A\f$
	int m_bound;							///<Magnetic quantum number of the atomic electron before the ionization
	unsigned int l_free;					///<Azimuthal quantum number of the (free) atomic electron after the ionization
	int m_free;								///<Magnetic quantum number of the (free) atomic electron after the ionization
};


///Struct for data sharing within the program
struct Transpot_shared_data{
	///Allow construction of shared storage only with a config file
	Transpot_shared_data(const Config_data& config) : cd(config){};

	///Delete copy-assignement operator (move equivalent implicitly deleted)
	Transpot_shared_data & operator = (const Transpot_shared_data&) = delete;

	///Delete copy operator (move equivalent implicitly deleted)
	Transpot_shared_data(const Transpot_shared_data&) = delete;

	///Data read from config file
	const Config_data cd;

	///All transition potentials calculated within a single program run
	std::vector<Transition_potential_result> results;
};



#endif
