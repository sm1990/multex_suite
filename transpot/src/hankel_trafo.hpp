#ifndef hankel_trafo_HPP
#define hankel_trafo_HPP


#include"value_pairs.hpp"
#include"spline_fit.hpp"

/**
 *@file hankel_trafo.hpp
 *@brief Class that performs Hankel transform (header)
 */

///Perform Hankel transforms
/*!
 * Perform Hankel transforms from a given dataset. Intermediate results of each Hankel transform are
 * saved to speed up subsequent Hankel transforms. The algorithm works by writing the Hankel transform
 * as a sum over Fourier transforms. For more details see \cite Toyoda2010 (instead of "Hankel transform",
 * the performed operation is called "spherical Bessel transform" in \cite Toyoda2010).
 */
class Hankel_trafo{
 public:
	///Default copy constructor
	Hankel_trafo(const Hankel_trafo& rhs) = default;
	///Construct from spline fit, sampling the spline \p samples times between \p min and \p max
	Hankel_trafo(const Spline_fit& spline, unsigned int samples, double min, double max);
	///Construct from spline fit, sampling the spline  \p samples times between the maximum and the minimum value in \p spline
	Hankel_trafo(const Spline_fit& spline, unsigned int samples);
	///Perform \p l -th Hankel transform
	Value_pairs perform_hankel_trafo(unsigned int l);


 private:
	const std::vector<std::array<double,13>> trafo_data;
	const std::vector<double> gammas;
	std::vector<std::vector<double>> integral_storage = {};

	std::vector<double> integral(unsigned int m);
};



#endif
