#ifndef spline_fit_HPP
#define spline_fit_HPP

#include<exception>
#include<array>
#include<vector>

#include"value_pairs.hpp"

/**
 *@file spline_fit.hpp
 *@brief Class that performs a spline fit (header)
 */
 
 
///Exceptions in spline_fit
class spline_fit_exception : public std::exception{
public:
	///Construct with error message \p error_msg
	explicit spline_fit_exception(const std::string& error_msg)
			 : error_message(error_msg) {}	
	///Return error message
	virtual const char* what() const throw(){
		return error_message.c_str();	
	}

private:
	const std::string error_message;

};
 

///Perform spline fit
/**Perform spline fit from two columns, one containing the x-values and one
 * containing the y-values of a discretely sampled function. The two columns
 * are saved in a Value_pairs object. Upon construction of a Spline_fit object,
 * the spline coefficients are calculated and the values of the spline fit
 * can be accessed via the Spline_fit::y_at function.
 *
 */
class Spline_fit{
 public:
	///Constructor
	/**The Constructor assumes that \p xy_values is sorted by ascending x-values.
	 * If \p xy_values is not sorted, a transpot_exception is thrown.
	 */
	explicit Spline_fit(const Value_pairs& xy_values);
	///Upper x-border of the spline fit
	double max_x() const;
	///Lower x-border of the spline fit
	double min_x() const;
	///Value of the spline fit at \p x_value
	double y_at(double x_value) const;
	///Value of the derivative of the spline fit at \p x_value
	double dy_at(double x_value) const;
	///Print spline fit to file
	/**Save the spline fit in a file with the name \p filename, sampling
	 * the spline fit \p samples times between its maximum and its minimum
	 * value.
	 *
	 */
	void print_to_file(std::string filename, unsigned int samples) const;

 private:
	const Value_pairs xy_values;
	const std::vector<std::array<double,3>> spline_coeff;

	unsigned int search_x_value(double x_value) const;
	
	

	
	
};





#endif
