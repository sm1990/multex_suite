#ifndef constants_HPP
#define constants_HPP


#include<unordered_map>
#include<string>
#include<math.h>
#include<complex>

#include<boost/math/constants/constants.hpp>

/**
 *@file transpot_constants.hpp
 *@brief Defines physical constants used throughout transpot
 */


///Physical constants and combinations thereof
/*!References used: \cite Mohr2015\n*/
namespace cst {
	const std::complex<double> i = std::complex<double>(0.,1.);					///<Imaginary unit
	const double pi =  boost::math::constants::pi<double>();					///<Ratio of a circle's circumference to its diameter
	constexpr double a_0 = 0.52917721067;										///<Bohr radius in Angstrom
	constexpr double el = 1.6021766208; 										///<\f$\cdot 10^{-19}\rightarrow\f$ electron charge in As
	constexpr double m_e = 9.10938356; 											///<\f$\cdot 10^{-31}\rightarrow\f$ electron rest mass in kg
	constexpr double h = 6.626070040; 											///<\f$\cdot 10^{-34}\rightarrow\f$ Planck constant in Js
	constexpr double hbar = 1.054571800; 										///<\f$\cdot 10^{-34}\rightarrow\f$ reduced Planck constant in Js
	constexpr double c = 2.99792458; 											///<\f$\cdot 10^{8}\rightarrow\f$ speed of light in m/s
	constexpr double c2=8.987551787;											///<\f$\cdot 10^{16}\rightarrow\f$ speed of light squared in \f$\text{m}^2/\text{s}^2\f$
	constexpr double epsilon_0 = 8.854187817; 									///<\f$\cdot 10^{-12}\rightarrow\f$ permittivity of free space in \f$\T{A}^2\T{s}^2/(\T{m}^3\T{kg})\f$
	constexpr double muh_0 = 12.566370614; 										///<\f$\cdot 10^{-7}\rightarrow\f$ magnetic constant in \f$\T{N}/\T{A}^2\f$

	//Derived constants (\AA = Angstrom):
	constexpr double hbarc = hbar*c/el*pow(10,3);								///<\f$\hbar c\f$ in \f$\T{\AA}\T{eV}\f$
	constexpr double m_e2dhbarh2 = 2.*el*m_e/pow(hbar,2)*0.01;					///<\f$2m_e/\hbar^2\f$ in \f$1/(\T{\AA}^2\T{eV})\f$
	constexpr double hbard2m_ec = hbar/(2.*m_e*c)*pow(10,-1);					///<\f$\hbar/(2m_e c)\f$ in \f$\T{\AA}\f$
	constexpr double qhbard2m_ec = pow(hbard2m_ec,2);							///<\f$(\hbar/(2m_e c))^2\f$ in \f$\T{\AA}^2\f$
	constexpr double cemuh_0 = c*muh_0*pow(10,1);								///<\f$c e \mu_0\f$ in eV/A
	constexpr double ce = c*el*pow(10,-1); 										///<\f$c e\f$ in \f$\T{\AA}\f$A
	constexpr double m_ech2 = m_e*c2*1./el*pow(10,4);							///<\f$m_e c^2\f$ in eV

	//Conversion constants:
	constexpr double hart_to_eV = 27.21138602;									///<Conversion factor Hartree to eV
	constexpr double au_to_ang = a_0;											///<Conversion factor atomic units to \f$\T{\AA}\f$

	///Map to convert atomic number to abbreviation
	const std::unordered_map<unsigned int, std::string> elemental_abbrv{
		{1,"H"}, {2,"He"}, {3,"Li"}, {4,"Be"}, {5,"B"}, {6,"C"},
		{7,"N"}, {8,"O"}, {9,"F"}, {10,"Ne"}, {11,"Na"}, {12,"Mg"},
		{13,"Al"}, {14,"Si"}, {15,"P"}, {16,"S"}, {17,"Cl"}, {18,"Ar"},
		{19,"K"}, {20,"Ca"}, {21,"Sc"}, {22,"Ti"}, {23,"V"}, {24,"Cr"},
		{25,"Mn"}, {26,"Fe"}, {27,"Co"}, {28,"Ni"}, {29,"Cu"}, {30,"Zn"},
		{31,"Ga"}, {32,"Ge"}, {33,"As"}, {34,"Se"}, {35,"Br"}, {36,"Kr"},
		{37,"Rb"}, {38,"Sr"}, {39,"Y"}, {40,"Zr"}, {41,"Nb"}, {42,"Mo"},
		{43,"Tc"}, {44,"Ru"}, {45,"Rh"}, {46,"Pd"}, {47,"Ag"}, {48,"Cd"},
		{49,"In"}, {50,"Sn"}, {51,"Sb"}, {52,"Te"}, {53,"I"}, {54,"Xe"},
		{55,"Cs"}, {56,"Ba"}, {57,"La"}, {58,"Ce"}, {59,"Pr"}, {60,"Nd"},
		{61,"Pm"}, {62,"Sm"}, {63,"Eu"}, {64,"Gd"}, {65,"Tb"}, {66,"Dy"},
		{67,"Ho"}, {68,"Er"}, {69,"Tm"}, {70,"Yb"}, {71,"Lu"}, {72,"Hf"},
		{73,"Ta"}, {74,"W"}, {75,"Re"}, {76,"Os"}, {77,"Ir"}, {78,"Pt"},
		{79,"Au"}, {80,"Hg"}, {81,"Tl"}, {82,"Pb"}, {83,"Bi"}, {84,"Po"},
		{85,"At"}, {86,"Rn"}, {87,"Fr"}, {88,"Ra"}, {89,"Ac"}, {90,"Th"},
		{91,"Pa"}, {92,"U"}, {93,"Np"}, {94,"Pu"}, {95,"Am"}, {96,"Cm"},
		{97,"Bk"}, {98,"Cf"}, {99,"Es"}, {100,"Fm"}, {101,"Md"}, {102,"No"},
		{103,"Lr"}
	};
	///Map to convert principal quantum number to abbreviation
	const std::unordered_map<unsigned int, std::string> n_abbrv{
		{1,"K"}, {2,"L"}, {3,"M"}, {4,"N"}, {5,"P"}, {6,"Q"}, {7,"R"}
	};
}



#endif
