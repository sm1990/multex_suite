#include"spherical_harmonics.hpp"

#include<boost/math/special_functions/spherical_harmonic.hpp>



/**
 *@file spherical_harmonics.cc
 *@brief Class for the calculation of spherical harmonics (source)
 */


Spherical_harmonics::Spherical_harmonics(Reciprocal_space_grids q_grids)
	: q_grids(q_grids){}



arma::cx_mat Spherical_harmonics::calc_spherical_harmonic(unsigned int L, int M){
	const std::pair<unsigned int, int> LM = std::make_pair(L,M);
	auto key_search_result = storage.find(LM);
	if (key_search_result == storage.end()){
		const arma::mat& qabs_grid = q_grids.qabs_grid;
		const arma::rowvec& qx_grid = q_grids.qx_grid;
		const arma::colvec& qy_grid = q_grids.qy_grid;
		const double qz = q_grids.qz;

		const unsigned int nx = static_cast<unsigned>(qabs_grid.n_cols);
		const unsigned int ny = static_cast<unsigned>(qabs_grid.n_rows);
		arma::cx_mat spherical_harmonic(ny,nx);
		for (unsigned int iqx=0; iqx<nx; iqx++){
			for (unsigned int iqy=0; iqy<ny; iqy++){
				const double qabs = qabs_grid(iqy,iqx);
				const double qx = qx_grid[iqx];
				const double qy = qy_grid[iqy];
				const double qphi = atan2(qy,qx);
				const double qtheta = acos(qz/qabs);
				spherical_harmonic(iqy,iqx) = boost::math::spherical_harmonic(L,M,qtheta,qphi);
			}
		}
		storage.emplace(std::make_pair(LM, spherical_harmonic));
		return spherical_harmonic;
	} else {
		arma::cx_mat& spherical_harmonic = key_search_result->second;
		return spherical_harmonic;
	}

}
