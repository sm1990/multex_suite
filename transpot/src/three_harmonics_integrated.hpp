#ifndef three_harmonics_integrated_HPP
#define three_harmonics_integrated_HPP

#include<vector>
#include<unordered_map>
#include<unordered_set>

#include <boost/functional/hash.hpp>


/**
 *@file three_harmonics_integrated.hpp
 *@brief Class for the calculation of the integral of three spherical harmonics (header)
 */


///Calculate the integral of three harmonics \f$\braket{ls~ms|Y_{LM}|l~m}\f$
/**Calculate the integral of three harmonics \f$\braket{ls~ms|Y_{LM}|l~m}\f$ for arbitrary \f$L,~M\f$ with
 * set values of \f$ls,~ms,~l\f$ and \f$m\f$. Already calculated results are stored internally. If a
 * previously calculated result is request again, the value is taken from internal storage instead of
 * being recalculated. For details on how \f$\braket{ls~ms|Y_{LM}|l~m}\f$ is calculated see \cite MyThesis.
 */
class Three_harmonics_integrated{
 public:
	///Constructor
	Three_harmonics_integrated(unsigned ls, int ms, int l, int m);

	///Calculate value of \f$\braket{ls~ms|Y_{LM}|l~m}\f$ for \f$L\f$ and \f$M\f$
	double value_for(unsigned int L, int M);

	///Hash required to store std::pair in a std::unordered_set
	using boost_hash = boost::hash<std::pair<unsigned,int>>;
	///Values of \f$L\f$ and \f$M\f$ for which \f$\braket{ls~ms|Y_{LM}|l~m}\f$ is non-zero
	std::unordered_set<std::pair<unsigned, int>,boost_hash> get_possible_LMs();


 private:
	const unsigned int ls;
	const int l,ms, m;
	std::unordered_map< std::pair<unsigned, int>, double, boost_hash> storage;
};




#endif
