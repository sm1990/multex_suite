#include<string>

#include<fftw3.h>

#include"transpot_exceptions.hpp"
#include"transpot_shared_data.hpp"
#include"logger.hpp"
#include"transpot_io.hpp"
#include"spline_fit.hpp"
#include"transition_potential.hpp"


/**
 *@file main.cpp
 *@brief Main file
 */

///Check if \f$|m|\leq l\f$
bool check_if_quantum_numbers_valid(unsigned int l, int m){
	unsigned int m_abs = static_cast<unsigned int>(abs(m));
	if (m_abs <= l){
		return true;
	} else {
		return false;
	}
}

///The main
int main(int argc, char* argv[]){
	std::string config_filename;
	if (argc==1){
		config_filename = "config.json";
	} else if (argc==2) {
		config_filename = std::string(argv[1]);
	} else {
		throw transpot_io_exception("More than one config-file passed as command line argument!");
	}

	auto start_time =  std::chrono::system_clock::now();


	//save on planing time for the Fourier Transforms by importing wisdom
	if (fftw_import_wisdom_from_filename("fftw.wis")==1){logg::out()<<"reading fftw-wisdom...\n";}


	logg::out()<<"reading config data from "<<config_filename<<"...\n";
	Transpot_shared_data tsd(read_input(config_filename));
	
	for (auto m_bound : tsd.cd.mls_bound ){
		bool bound_valid = check_if_quantum_numbers_valid(tsd.cd.l_bound, m_bound);
		if (bound_valid){
			for (auto l_free: tsd.cd.ls_free){
				Transition_potential tpot(m_bound,l_free,tsd);
				for (auto m_free:tsd.cd.mls_free){
					bool free_valid = check_if_quantum_numbers_valid(l_free,m_free);
					if (free_valid){
						logg::out()<<"Performing calculation for quantum numbers l'="<<l_free<<", m'="<<m_free;
						logg::out()<<" and m="<<m_bound<<"\n";
						tpot.calc_transition_potential(m_free,tsd);
					}
				}
			}
		}
	}


	auto end_time = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_time-start_time);
	logg::out()<<"Execution time was "<<elapsed.count()<<" milliseconds.\n";
	logg::out()<<"saving results...\n";
	save_results(tsd);

	//export gathered fftw wisdom
	if (fftw_export_wisdom_to_filename("fftw.wis")==1){logg::out()<<"exporting fftw-wisdom...\n";}

	




}
