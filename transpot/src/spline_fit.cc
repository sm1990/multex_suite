#include"spline_fit.hpp"

#include<algorithm>
#include<math.h>
#include<fstream>

#include"logger.hpp"


/**
 *@file spline_fit.cc
 *@brief Class that performs a spline fit (source)
 */


namespace{
	
	bool check_if_x_sorted(const Value_pairs& xy_data){
		for (unsigned int i=1; i<xy_data.size(); i++){
			if (xy_data(i,0)<xy_data(i-1,0)){
				return false;
			}
		}
		return true;
	}

	///Spline fits a functions described by a x-column and a y=f(x)-column.
	/*!Spline fits the data in \p xy_data to natural cubic splines:\n
	 *\f$S_i(x) = a_i+b_i*(x-x_i)+c_i(x-x_i)^2+d_i(x-x_i)^3\f$ with\n
 	 *  * \f$a_i\f$ = xy_data[i].second
 	 *  * \f$b_i\f$ = spline_coeff[i][0]
 	 *  * \f$c_i\f$ = spline_coeff[i][1]
 	 *  * \f$d_i\f$ = spline_coeff[i][2].
 	 *
 	 * The algorithm is taken from Wikipedia (html and a helpful presentation can be found in the
 	 * documentation folder). Essentially, one uses the conditions provided by a dataset to create
 	 *  an almost diagonal system of linear equations, which can then be solved using two loops.*/
	std::vector<std::array<double,3>> perform_spline_fit(const Value_pairs& xy_data){
		
		if (!check_if_x_sorted(xy_data)){
			throw spline_fit_exception("Dataset for Spline_fit not sorted by ascending x-value.");
		}

		std::vector<std::array<double,3>> spline_coeff;
		auto size = static_cast<unsigned int>(xy_data.size()-1);
		spline_coeff.resize(size);

		//prepare data needed for solution of linear equation system
		std::vector<double> h(size-1), alpha(size-1);
		for (unsigned int i=0; i<size-1; ++i){
			h[i] = xy_data(i+1,0)-xy_data(i,0);
		}
		for (unsigned int i=1; i<size-1; ++i){
			alpha[i]=3.*(((xy_data(i+1,1)-xy_data(i,1))/h[i])
						 -((xy_data(i,1)-xy_data(i-1,1))/h[i-1]));	
		}

		//calculate  the coefficients occuring in the linear equation system
		std::vector<double> l(size), muh(size), z(size);
		l[0]=1, muh[0]=0, z[0]=0;
		for (unsigned int i=1; i<size-1; ++i){
			l[i]=2.*(xy_data(i+1,0)-xy_data(i-1,0))-h[i-1]*muh[i-1];
			muh[i]=h[i]/l[i];
			z[i]=(alpha[i]-h[i-1]*z[i-1])/l[i];	
		}
	

		//solve the linear equation system by backwards substitution and use the solution to calculate
		//the spline coefficients
		l[size-1]=1; z[size-1]=0; spline_coeff[size-1][1]=0;
		for (int j=size-2; j>=0; --j) {
			spline_coeff[j][1] = z[j]-muh[j]*spline_coeff[j+1][1];
			spline_coeff[j][0] = (xy_data(j+1,1)-xy_data(j,1))/h[j]
							-h[j]*(spline_coeff[j+1][1]+2.*spline_coeff[j][1])/3.;
			spline_coeff[j][2] = (spline_coeff[j+1][1]-spline_coeff[j][1])/(3.*h[j]);
		}

		return spline_coeff;
	}
}


Spline_fit::Spline_fit(const Value_pairs& xy_values)
		: xy_values(xy_values), spline_coeff(perform_spline_fit(xy_values)) {}


double Spline_fit::max_x() const{
	return xy_values(static_cast<unsigned int>(xy_values.size()-1),0);
}

double Spline_fit::min_x() const{
	return xy_values(0,0);
}

double Spline_fit::y_at(double x_value) const{
	if (x_value > max_x()){
		return 0;
	}

	unsigned int value_index = search_x_value(x_value);

	double spline_y_value = 0;
	double x_minus_x_0 = x_value-xy_values(value_index,0);
	double a = xy_values(value_index,1), b = spline_coeff[value_index][0];
	double c = spline_coeff[value_index][1], d = spline_coeff[value_index][2];
	spline_y_value = a+x_minus_x_0*b+pow(x_minus_x_0,2)*c+pow(x_minus_x_0,3)*d;
	
	return spline_y_value;
}


double Spline_fit::dy_at(double x_value) const{
	if (x_value > max_x()){
		return 0;
	}

	unsigned int value_index = search_x_value(x_value);
	
	double spline_dy_value = 0;
	double x_minus_x_0 = x_value-xy_values(value_index,0);
	double b = spline_coeff[value_index][0];
	double c = spline_coeff[value_index][1], d = spline_coeff[value_index][2];
	spline_dy_value = b+x_minus_x_0*2.*c+pow(x_minus_x_0,2)*3.*d;

	return spline_dy_value;
}


void Spline_fit::print_to_file(std::string filename, unsigned int samples) const{
	std::ofstream file(filename.c_str());

	double step_size = (max_x()-min_x())/samples;
	for (unsigned int i=0; i<samples; i++){
		double x = min_x()+i*step_size;
		file<<x<<"   "<<y_at(x)<<"\n";
	}

	file.close();	
}





unsigned int Spline_fit::search_x_value(double x_value) const{
	//vl. besser als binary search mit lower bound
	const double max = max_x();
	const double min = min_x();

	if ( (x_value<min) || (x_value>max) ){
		logg::out()<<"x_value: "<<x_value<<"\n";
		logg::out()<<"min: "<<min<<"\n";
		logg::out()<<"max: "<<max<<"\n";
		throw spline_fit_exception("x-value out of dataset bounds in Spline_fit.");
	}


	unsigned int value_index = 0;
	const arma::colvec& x_values = xy_values.column_as_colvec(0);
	auto low = std::upper_bound(x_values.begin(),x_values.end(),x_value);
	value_index = static_cast<unsigned>(std::distance(x_values.begin(),low)-1);
	
	
	
	return value_index;

}






