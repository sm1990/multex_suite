#include"three_harmonics_integrated.hpp"

#include"wignerSymbols.h"

#include"transpot_constants.hpp"

/**
 *@file three_harmonics_integrated.cc
 *@brief Class for the calculation of the integral of three spherical harmonics (source)
 */




Three_harmonics_integrated::Three_harmonics_integrated(unsigned ls, int ms, int l, int m)
	: ls(ls), l(l), ms(ms), m(m){}



double Three_harmonics_integrated::value_for(unsigned int L, int M){
	if (l<0){return 0.;}
	const std::pair<unsigned int, int> LM = std::make_pair(L,M);
	auto key_search_result = storage.find(LM);
	if (key_search_result == storage.end()){
		double result = WignerSymbols::wigner3j(ls,L,l,0,0,0);
		if (result==0){return 0;}
		result *= WignerSymbols::wigner3j(ls,L,l,-ms,M,m);
		result *= pow(-1.,ms)*sqrt((2.*ls+1.)*(2.*L+1.)*(2.*l+1.)/(4.*cst::pi));
		storage.emplace(std::make_pair(LM, result));
		return result;
	} else {
		double result = key_search_result->second;
		return result;
	}
}

///Hash required to store std::pair in a std::unordered_set
using boost_hash = boost::hash<std::pair<unsigned,int>>;
std::unordered_set<std::pair<unsigned, int>,boost_hash> Three_harmonics_integrated::get_possible_LMs(){
	std::unordered_set<std::pair<unsigned int, int>,boost_hash> possible_LMs;
	if (l<0){
		possible_LMs.clear();
		return possible_LMs;
	}
	const int ls_int = static_cast<int>(ls);
	const unsigned int L_min = static_cast<unsigned>(abs(ls_int-l));
	const unsigned int L_max = ls+static_cast<unsigned>(l);
	const int M = ms-m;
	for(unsigned int L=L_min; L<=L_max; L++){
		if (value_for(L,M) != 0){
			possible_LMs.emplace(std::make_pair(L,M));
		}
	}
	return possible_LMs;

}
