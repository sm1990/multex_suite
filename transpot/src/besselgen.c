//additions for compatibility with transpot:
#include<cmath>
#include<vector>
using namespace std;

/**
 *@file besselgen.c
 *@brief Calculates spherical Bessel function for contwave \cite Frigge2011
 */




//*************************************************************************
//  This document contains a function wich calculates the sperical Bessel
//  function and its sencond order(the so called Neumann function).
//
//  This code is originally written by  R.W.B. ARDILL and K.J.M. MORIARTY
// bublished in //  REF. IN COMP. PHYS. COMMUN. 14 (1978) 261

// Adopted  to C++ by Martin Frigge (26.06.2011)
// This code is part of a programm to calculate the energy
// differential cross-section. the main function is defined in the
// file dsigma.cpp.
//************************************************************************



//**************************************************************************
//  SPHBES.  SPHERICAL BESSEL FUNCTIONS JN AND YN OF INTEGER ORDER AND
//  1   REAL ARGUMENT.  R.W.B. ARDILL, K.J.M. MORIARTY.
//  REF. IN COMP. PHYS. COMMUN. 14 (1978) 261
//  JOB(UHAH102,J6,T60)  ARDILL             SPHERICAL BESSEL FUNCTIONS
//  FTN(R=3,SL)
//  MAP(ON)
//  LDSET(PRESET=NGINF)
//  LGO.
//*************************************************************************


//***    ------------------------------------------------------------------

//***    PROGRAM TO CALCULATE SPHERICAL BESSEL FUNCTIONS J(N,X) AND Y(N,X)
//  FOR A NUMBER OF VALUES OF THE ORDER N (POSITIVE OR NEGATIVE
//  INTEGER OR ZERO) AND ARGUMENT X (REAL) USING FORMULAE FROM
//  HANDBOOK OF MATHEMATICAL FUNCTIONS (EDITORS ABRAMOWITZ AND STEGUN)

//  BY R.W.B.ARDILL AND K.J.M.MORIARTY,
//  DEPARTMENT OF MATHEMATICS, ROYAL HOLLOWAY COLLEGE,
//  ENGLEFIELD GREEN, EGHAM, SURREY, TW20 0EX, U.K.
//  NOVEMBER 1977

//     SPHFUN - CALCULATES SIMULTANEOUSLY SPHERICAL BESSEL FUNCTIONS
//              OF THE FIRST AND SECOND KIND J AND Y RESPECTIVELY,
//              AT POSITIVE OR NEGATIVE INTEGER OR ZERO VALUES OF THE
//              ORDER N AND REAL ARGUMENTS X.

double SIGN(double A,double &B){
    double C;
    if(B>=0.){
        C=fabs(A);
        return C;
    }
    else{
        C=-fabs(A);
        return C;
    }
}

double SPHFUN(int N, double X,double &neumann){
    //  EVALUATION OF SPHERICAL BESSEL (fuctionvalue=bessel) AND NEUMANN
    //  (neumann) FUNCTIONS OF ORDER N (INTEGER) AND ARGUMENT X (REAL).

    //  FORMULAE TAKEN FROM N.B.S. HANDBOOK OF MATHEMATICAL FUNCTIONS
    //  P437 FORMULAE (10.1.2) , (10.1.3) , (10.1.4) , (10.1.5) ,
    //                (10.1.8) , (10.1.9)   AND
    //  P439 FORMULA (10.1.15)

    //  INF IS RETURNED ZERO , UNLESS EITHER

    //  1) X IS ZERO.
    //     HERE, IF
    //     N IS NONNEGATIVE, SY IS NEGATIVE INFINITY (N ODD) AND INFINITE
    //     (N EVEN), AND INF IS RETURNED 1,
    //     AND IF
    //     N IS NEGATIVE, SJ IS INFINITE (N ODD) AND NEGATIVE INFINITY
    //     (N EVEN), AND INF IS RETURNED -1.
    //  OR
    //  2) X IS NEAR ZERO.
    //     HERE, IF
    //     N IS NONNEGATIVE, SY IS VERY LARGE AND NEGATIVE (N ODD,
    //     OR N EVEN AND X POSITIVE) AND VERY LARGE AND POSITIVE (N EVEN
    //     AND X NEGATIVE), AND INF IS RETURNED -1.
    //     IF ABS(SJ) (OR ABS(SY) IF N NEGATIVE) IS LESS THAN THE SMALLEST
    //     REAL NUMBER ACCESSIBLE TO THE MACHINE, SJ (OR SY) IS RETURNED
    //     ZERO.
    //
    //  EPSILON IS THE RELATIVE ERROR REQUIRED.

    double bessel; //return value bessle=j(N,X)=SJ and neumann=Y(N,X)=SY
    double ANL,ANU;//,PI;
    int IR2; //IR1;
    double R1=0,R2,ALR1,ALR2,TR,ZZZ; //XXXchanged: R1 -> R1=0
    double V,SN,CN,B=0,SUM1,SUM2,XN1,TERM,XX,T; //XXXchanged: B -> B=0
    vector<double> SCJ(4),SCY(4);
    int I;//,INF;

    //PI= 4.*atan(1.);
    double EPSILON=10e-10;


    //DIMENSION SCJ(4),SCY(4)

    //  ALOG10 OF LOWER AND UPPER BOUNDS TO REAL NUMBERS ACCEPTABLE TO
    //  MACHINE. (VALUES SET BY USER TO CORRESPOND TO MACHINE USED.
    //  VALUES GIVEN HERE APPROPRIATE TO CDC6600 AND CDC7600.
    ANL=-293.;
    ANU=322.;
    //***    INITIALIZE INF
    //INF = 0;

    // *** This part is not needed, because here just positive N relevant
    //**********************************************************************
    //***    ADJUST FOR NEGATIVE ORDERS.
    //NN = N;
    ////***    IF N IS NEGATIVE CALCULATE J(-N-1,X) AND Y(-N-1,X) INSTEAD AND
    //  READUST FOR THE CORRECT ORDER AT THE END OF THIS ROUTINE USING
    //  FORMULA (10.1.15).
    //if(NN<0) N=-N-1;
    //********************************************************************

    //***    SPECIAL CALCULATION FOR X = 0
    if(X==0.){
        //***    SET PARAMETER INF TO 1 TO INDICATE A NEGATIVE INFINITY IN SY.
        //INF = 1; // Y=-oo
        if(N==0.){
           bessel = 1.0;
           return bessel;

        }
        else{//-->N!=0
            bessel= 0.0;
            return bessel;
        }
    }
    else{//-->X!=0
        if(fabs(X)<1.){
         //  ONE COULD CHOOSE A SMALLER NUMBER THAN 1.0 - AND SO BYPASS THESE
         //  TESTS ON ARGUMENTS NEAR ZERO, SPEEDING UP THE PROGRAM.
         //  HOWEVER, THE LARGER THE ORDER IS, THE NEARER 1.0 THE NUMBER NEEDS
         //  TO BE - FOR EXAMPLE, FOR N = 50 THE NUMBER NEEDS TO BE OF THE
         //  ORDER OF 0.0001

         //***    SPECIAL CALCULATION NEAR X = 0.
         // INITIALIZE TEST PARAMETERS IR1 AND IR2 FOR TESTING SIZE OF R1 AND
         //  R2 WHERE R1 = 1.3.5.....(2N-1) AND R2 = 1.3.5.....(2N+1).
            //IR1 = 0;
            IR2 = 0;
         //***    CHECK FOR ZERO ORDER
            if(N==0){
                R1 = 1.0;
                R2 = 1.0;
                ALR1 = 0.0;
                ALR2 = 0.0;
            }
            else{//-->N!=0 AND STILL X!=0; abs(X)<1.
             //***    CHECK FOR LARGE ORDERS
             //  TEST APPROXIMATE SIZE OF R1
             //  APPROXIMATE R1 BY (2N)**N (R1 .LT. (2N)**N , FOR N .GT. 0)
                TR =double(N)*log10(double(2*N));
                if(TR-ANU<0.){
                    // for small R1
                    R1=1.;
                    for(int k=1; k<=N; k++)
                        R1=R1*double(2*k-1);
                    ALR1 = log10(R1);
                }
                else{//-->TR-ANU >=0
                    // for lage R1
                    // SET IR1 = 1 (INDICATES LARGE R1 - FOR LATER USE)
                    //IR1 = 1;
                    ALR1 = 0.;
                    for(int k=1; k<=N; k++)
                        ALR1=ALR1+log10(double(2*k-1));
                }//END of -->TR-ANU >=0
                // TEST APPROXIMATE SIZE OF R2 (R2 = (2N+1)*R1)
                if(TR+log10(double(2*N+1))-min(ANU,fabs(ANL))<0){
                    // for small R2
                    R2 = R1*double(2*N+1);
                    ALR2 = log10(R2);
                }
                else{//--> TR+log10(double(2*N+1))-min(ANU,abs(ANL))>=0
                    // for lage R2
                    // SET IR2 = 1 (INDICATES LARGE R2 - FOR LATER USE)
                    IR2 = 1;
                    ALR2 = ALR1+log10(double(2*N+1));
                }
            }//END of -->N!=0

            //  TEST FOR OVERFLOW IN SY, USING (SEE (10.1.3))
            //  ABS(SY) .GE. ((2N-1).....5.3.1)/ABS(X**(N+1))*(1-0.5X**2/(1-2N)).
            //  HERE DUE TO SMALL X, THE X**2 TERM IS OMITTED.
            ZZZ = ALR1-double(N+1)*log10(fabs(X));
            if(ZZZ>ANU){
                // OVERFLOW. INDICATED BY SETTING INF = 1. SJ ALSO SET 1
                // (IF N = 0) OR 0 (IF N NONZERO).
                //INF = 1;
                if(N==0){
                    bessel=1.;
                    return bessel;
                }
                else{
                    bessel=0.;
                    return bessel;
                }
            }
            else{//--> ZZZ<=ANU
                // TEST FOR UNDERFLOW IN SJ AT SMALL X, USING (SEE (10.1.4))
                // ABS(SJ) .GE. ABS(X**N)*(1-0.5X**2/(2N+3))/((2N+1).....5.3.1)
                // HERE DUE TO SMALL X, THE X**2 TERM IS OMITTED.
                ZZZ = double(N)*log10(fabs(X))-ALR2;
                if(ZZZ<ANL){
                    // UNDERFLOW. SJ RESET ZERO AND SY TREATED AS NEGATIVE INFINITY
                    //INF = 1;
                    bessel=0.;
                    return bessel;
                }
                else{//--> ZZZ>=ANL  AND STILL ZZZ<=ANU
                    //  TEST IF ABS(X)**2 .LT. LEAST REAL NUMBER ACCESSIBLE TO MACHINE.
                    //  IF THIS IS SO, CALCULATE SJ AND SY BY THEIR LIMITING VALUES GIVEN
                    //  IN FORMULAE (10.1.4) AND (10.1.5). THIS AVOIDS AN INFINITY THAT
                    //  WOULD OTHERWISE ARISE IN THE PROGRAM WHEN X**2 IS EVALUATED.
                    if(2.*log10(fabs(X))<=ANL){
                        if(IR2==0)
                            bessel=1./R2*pow(X,double(N));
                        if(IR2==1)
                            bessel=pow(10.,(double(N)*log10(fabs(X))-ALR2))*pow((SIGN(1.,X)),double(N));
                        if(IR2==0)
                            neumann=-R1/pow(X,double(N+1));
                        if(IR2==1)
                            neumann=pow(10.,(ALR1-double(N+1)*log10(fabs(X))))*pow((SIGN(1.,X)),double(N));
                        return bessel;
                    }
                    else{//--> 2.*log10(fabs(X))>ANL
                        //***    TEST FOR THE RELATIVE SIZE OF ORDER TO ARGUMENT

                        V = double(N)/fabs(X)-1.;

                        // CALCULATION PROCEEDS IN TWO WAYS DEPENDING ON WHETHER OR NOT WE
                        // HAVE LARGE ORDERS AND SMALL ARGUMENTS I.E. THE RELATIVE SIZE OF
                        // ORDER TO ARGUMENT.
                        // FORMULAE (10.1.8),(10.1.9) INVOLVING POLYNOMIALS IN 1/X COULD BE
                        // USED IN ALL CASES, BUT WHEN X IS SMALL AND N LARGE IT IS QUICKER
                        // TO USE FORMULAE (10.1.2) AND (10.1.3) INVOLVING INFINITE SERIES IN
                        // (0.5*X**2)

                        //  ABS(ORDER) LESS THAN OR EQUAL TO ABS(ARGUMENT).
                        //  FORMULAE (10.1.8) AND (10.1.9) USED

                        if(V<=0.){
                            //  DETERMINE COS(X-0.5*N*PI) AND SIN(X-0.5*N*PI) , GIVEN BY CN AND SN
                            //  RESPECTIVELY, FOR USE WITH FORMULAE (10.1.8) AND (10.1.9).
                            //  THIS PARTICULAR APPROACH IS ADOPTED TO AVOID MACHINE INACCURACY
                            //  DUE TO A FINITE LENGTH FOR THE REPRESENTATION OF PI IN A MACHINE.
                            //  INACCURACIES BECOME IMPORTANT FOR LARGE N (I.E. LARGE ORDERS),
                            //  FOR EXAMPLE N = 80 AND X = 100 WHERE THE SPHERICAL BESSEL
                            //  FUNCTIONS WOULD BE CALCULATED IN THIS PART OF THE PROGRAM.
                            if(((N-1)/4)*4-(N-1)==0){
                                //***    N=1,5,9,13,.....
                                SN = -cos(X);
                                CN =  sin(X);
                            }
                            else{
                                if(((N-2)/4)*4-(N-2)==0){
                                    //***    N=2,6,10,14,.....                                                 0425
                                    SN = -sin(X);
                                    CN = -cos(X);
                                }
                                else{
                                    if(((N-3)/4)*4-(N-3)==0){
                                        //***    N=3,7,11,15,.....                                                 0430
                                        SN =  cos(X);
                                        CN = -sin(X);
                                    }
                                    else{
                                        //***    N=0,4,8,12,.....                                                  0434
                                        SN =  sin(X);
                                        CN =  cos(X);
                                    }
                                }
                            }
                            //SIN(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                            SCJ[0] = SN;
                            //COS(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                            SCJ[1] = CN;
                            //-SIN(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                            SCJ[2] =-SN;
                            //-COS(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                            SCJ[3] =-CN;
                            //COS(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                            SCY[0] = CN;
                            //-SIN(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                            SCY[1] =-SN;
                            //-COS(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                            SCY[2] =-CN;
                            //SIN(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                            SCY[3] = SN;

                            SUM1 = 0.;
                            SUM2 = 0.;
                            for(I=0;I<=N;I++){
                                I=I-1;
                                for(int j=0;j<4;j++){
                                    I=I+1;
                                    if(I<=N){
                   // B IS NEXT TERM TO BE ADDED INTO P OR Q OF FORMULAE (10.1.8) OR (10.1.9)
                                        if(I!=0)
                                            B=B*double((N+I)*(N-I+1))/double(2*I);
                                        else
                                            B=1.;
                                        SUM1 = (SUM1+B*SCJ[j])*X;
                                        SUM2 = (SUM2+B*SCY[j])*X;
                                    }
                                }
                            }
                            XN1 = pow((1./X),double(N+2));
                            //***    SPHERICAL BESSEL FUNCTION
                            bessel=XN1*SUM1;
                            //***    SPHERICAL NEUMANN FUNCTION (I.E. SPHERICAL BESSEL FUNCTION Y)
                            neumann=-XN1*SUM2;
                            return bessel;
                        }//END of -->V<=0
                        else{//--> V>0
                            //  ABS(ORDER) GREATER THAN ABS(ARGUMENT).
                            //  FORMULAE (10.1.2) AND (10.1.3) USED

                            //***    CALCULATE SPHERICAL BESSEL FUNCTION J USING FORMULA (10.1.2)
            //                I=-1;
            //                SUM2=0.;
                            //***    CALCULATE OUTSIDE FACTOR FOR POWER SERIES
                            TERM = 1.;
                            XX =-0.5*X*X;
                            for(int j=1;j<=N;j++)
                                TERM = TERM*X/double(2*j+1);
                            //***    OUTSIDE FACTOR FOR POWER SERIES
                            B = TERM;
                            //***    CALCULATE POWER SERIES
                            TERM = 1.;
                            SUM1 = 1.;
                            T = 2*N+1;
                            for(int v=1;fabs(TERM)-EPSILON*fabs(SUM1)>=0.;v++){
                                T=T+2.;
                                TERM = TERM*XX/(double(v)*T);
                                SUM1 = SUM1+TERM;
                            }
                            //***    SPHERICAL BESSEL FUNCTION
                            bessel=SUM1*B;

                            //  CALCULATE SPHERICAL NEUMANN FUNCTION (I.E.SPHERICAL BESSEL
                            //  FUNCTION Y) USING FORMULA (10.1.3)
                            //***    CALCULATE OUTSIDE FACTOR FOR POWER SERIES
                            TERM = -1./X;
                            for(int j=1;j<=N;j++)
                                TERM = TERM*double(2*j-1)/X;
                            //***    OUTSIDE FACTOR FOR POWER SERIES
                            B = TERM;
                            //***    CALCULATE POWER SERIES
                            TERM = 1.;
                            SUM1 = 1.;
                            T = double(-2*N-1);
                            for(int v=1;fabs(TERM)-EPSILON*fabs(SUM1)>=0.;v++){
                                T=T+2.;
                                TERM = TERM*XX/(double(v)*T);
                                SUM1 = SUM1+TERM;
                            }
                            //***    SPHERICAL NEUMANN FUNCTION (I.E. SPHERICAL BESSEL FUNCTION Y)
                            neumann=SUM1*B;
                            return bessel;
                        }//END of -->V>0
                    }
                }
            }//END of --> ZZZ<=ANU
        }//END of -->(abs(X)<1.
        else{//-->abs(X)>=1.
            //***    TEST FOR THE RELATIVE SIZE OF ORDER TO ARGUMENT

            V = double(N)/fabs(X)-1.;

            // CALCULATION PROCEEDS IN TWO WAYS DEPENDING ON WHETHER OR NOT WE
            // HAVE LARGE ORDERS AND SMALL ARGUMENTS I.E. THE RELATIVE SIZE OF
            // ORDER TO ARGUMENT.
            // FORMULAE (10.1.8),(10.1.9) INVOLVING POLYNOMIALS IN 1/X COULD BE
            // USED IN ALL CASES, BUT WHEN X IS SMALL AND N LARGE IT IS QUICKER
            // TO USE FORMULAE (10.1.2) AND (10.1.3) INVOLVING INFINITE SERIES IN
            // (0.5*X**2)

            //  ABS(ORDER) LESS THAN OR EQUAL TO ABS(ARGUMENT).
            //  FORMULAE (10.1.8) AND (10.1.9) USED

            if(V<=0.){
                //  DETERMINE COS(X-0.5*N*PI) AND SIN(X-0.5*N*PI) , GIVEN BY CN AND SN
                //  RESPECTIVELY, FOR USE WITH FORMULAE (10.1.8) AND (10.1.9).
                //  THIS PARTICULAR APPROACH IS ADOPTED TO AVOID MACHINE INACCURACY
                //  DUE TO A FINITE LENGTH FOR THE REPRESENTATION OF PI IN A MACHINE.
                //  INACCURACIES BECOME IMPORTANT FOR LARGE N (I.E. LARGE ORDERS),
                //  FOR EXAMPLE N = 80 AND X = 100 WHERE THE SPHERICAL BESSEL
                //  FUNCTIONS WOULD BE CALCULATED IN THIS PART OF THE PROGRAM.
                if(((N-1)/4)*4-(N-1)==0){
                    //***    N=1,5,9,13,.....
                    SN = -cos(X);
                    CN =  sin(X);
                }
                else{
                    if(((N-2)/4)*4-(N-2)==0){
                        //***    N=2,6,10,14,.....                                                 0425
                        SN = -sin(X);
                        CN = -cos(X);
                    }
                    else{
                        if(((N-3)/4)*4-(N-3)==0){
                            //***    N=3,7,11,15,.....                                                 0430
                            SN =  cos(X);
                            CN = -sin(X);
                        }
                        else{
                            //***    N=0,4,8,12,.....                                                  0434
                            SN =  sin(X);
                            CN =  cos(X);
                        }
                    }
                }
                //SIN(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                SCJ[0] = SN;
                //COS(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                SCJ[1] = CN;
                //-SIN(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                SCJ[2] =-SN;
                //-COS(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION J)
                SCJ[3] =-CN;
                //COS(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                SCY[0] = CN;
                //-SIN(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                SCY[1] =-SN;
                //-COS(X-0.5*N*PI)(USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                SCY[2] =-CN;
                //SIN(X-0.5*N*PI) (USED IN CALCULATING SPHERICAL BESSEL FUNCTION Y)
                SCY[3] = SN;

                SUM1 = 0.;
                SUM2 = 0.;
                for(I=0;I<=N;I++){
                    I=I-1;
                    for(int j=0;j<4;j++){
                        I=I+1;
                        if(I<=N){
       // B IS NEXT TERM TO BE ADDED INTO P OR Q OF FORMULAE (10.1.8) OR (10.1.9)
                            if(I!=0)
                                B=B*double((N+I)*(N-I+1))/double(2*I);
                            else
                                B=1.;
                            SUM1 = (SUM1+B*SCJ[j])*X;
                            SUM2 = (SUM2+B*SCY[j])*X;
                        }
                    }
                }
                XN1 = pow(X,(-1.)*double(N+2));
                //***    SPHERICAL BESSEL FUNCTION
                bessel=XN1*SUM1;
                //***    SPHERICAL NEUMANN FUNCTION (I.E. SPHERICAL BESSEL FUNCTION Y)
                neumann=-XN1*SUM2;
                return bessel;
            }//END of -->V<=0
            else{//--> V>0
                //  ABS(ORDER) GREATER THAN ABS(ARGUMENT).
                //  FORMULAE (10.1.2) AND (10.1.3) USED

                //***    CALCULATE SPHERICAL BESSEL FUNCTION J USING FORMULA (10.1.2)
//                I=-1;
//                SUM2=0.;
                //***    CALCULATE OUTSIDE FACTOR FOR POWER SERIES
                TERM = 1.;
                XX =-0.5*X*X;
                for(int j=1;j<=N;j++)
                    TERM = TERM*X/double(2*j+1);
                //***    OUTSIDE FACTOR FOR POWER SERIES
                B = TERM;
                //***    CALCULATE POWER SERIES
                TERM = 1.;
                SUM1 = 1.;
                T = double(2*N+1);
                for(int v=1;fabs(TERM)-EPSILON*fabs(SUM1)>=0.;v++){
                    T=T+2.;
                    TERM = TERM*XX/(double(v)*T);
                    SUM1 = SUM1+TERM;
                }
                //***    SPHERICAL BESSEL FUNCTION
                bessel=SUM1*B;

                //  CALCULATE SPHERICAL NEUMANN FUNCTION (I.E.SPHERICAL BESSEL
                //  FUNCTION Y) USING FORMULA (10.1.3)
                //***    CALCULATE OUTSIDE FACTOR FOR POWER SERIES
                TERM = -1./X;
                for(int j=1;j<=N;j++)
                    TERM = TERM*double(2*j-1)/X;
                //***    OUTSIDE FACTOR FOR POWER SERIES
                B = TERM;
                //***    CALCULATE POWER SERIES
                TERM = 1.;
                SUM1 = 1.;
                T = double(-2*N-1);
                for(int v=1;fabs(TERM)-EPSILON*fabs(SUM1)>=0.;v++){
                    T=T+2.;
                    TERM = TERM*XX/(double(v)*T);
                    SUM1 = SUM1+TERM;
                }
                //***    SPHERICAL NEUMANN FUNCTION (I.E. SPHERICAL BESSEL FUNCTION Y)
                neumann=SUM1*B;
                return bessel;
            }//END of -->V>0
        }//END of -->abs(X)>=1
    }//END of -->X!=0
}//END of vector<double> SPHFUN(int N, double X,double INF,double EPSILON)

