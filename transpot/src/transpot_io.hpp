#ifndef multex_io_HPP
#define multex_io_HPP


#include"transpot_shared_data.hpp"


/**
 *@file transpot_io.hpp
 *@brief Functions for input and output (header)
 */

///Read config file \p config_filename
Config_data read_input(std::string config_filename);

///Save results of program run (which are stored in \p tsd)
void save_results(const Transpot_shared_data& tsd);



#endif
