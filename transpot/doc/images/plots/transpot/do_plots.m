graphics_toolkit("fltk");
set(0, 'defaulttextfontsize', 22)

# style settings #
curve_linewidth = 2;
axis_linewidth = 1.5;
xylabel_fontsize = 22;
axis_fontsize = 18;
legend_fontsize = 18;
plot_window_position_A = [0,0,576,432];
plot_window_position_B = [0,0,768,432];



# get values #
[info,mat,rs_mat] = load_transition("O_200kV_K00_10_10_64x64.bin");
rs_tpot = imag(rs_mat.component_0);

# create xy-grid #
half_x = info.x_dim*0.5;
step_x = info.x_dim/info.x_pixel;
half_y = info.y_dim*0.5;
step_y = info.y_dim/info.y_pixel;
X = -half_x+step_x:step_x:half_x;
Y = -half_y+step_y:step_y:half_y;
[XX,YY] = meshgrid(X,Y);

# do plots #
surf(XX,YY,rs_tpot);
xlim([-3,3]);
ylim([-3,3]);
set(gca, "linewidth",axis_linewidth);
set(gca, "fontsize", axis_fontsize);
xlabel("$x$ in \\AA");
ylabel("$y$ in \\AA");
zlabel('Transition potential (IM) in $\sqrt{\text{eV}}/\text{A}$');
print('-dpdflatex', 'transition_potential');



### clean-up ###
clear all;
