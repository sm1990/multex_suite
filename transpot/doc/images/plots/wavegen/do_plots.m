graphics_toolkit("fltk");
set(0, 'defaulttextfontsize', 22)

### plot speed of the electron for different acceleration voltages ###
# style settings #
curve_linewidth = 2;
axis_linewidth = 1.5;
xylabel_fontsize = 22;
axis_fontsize = 18;
legend_fontsize = 18;
plot_window_position_A = [0,0,576,432];
plot_window_position_B = [0,0,768,432];

# get values #
waveup = load("waveup.dat");


# do plots #
#   potential: #
figure('paperunits','points','papertype', '<custom>',
    'paperposition',[0 0 576 432], 'papersize', [576,432], 'Position', plot_window_position_A);
plot(waveup(:,1), waveup(:,2) , "linewidth", curve_linewidth, 'r');
set(gca, "fontsize", axis_fontsize, "linewidth", axis_linewidth);
xlabel("$r$ in au", "fontsize", xylabel_fontsize);
ylabel("$V(r)$ in Hartree", "fontsize", xylabel_fontsize);
xlim([0,0.3]);
ylim([-2000,0]);
print('-dpdflatex', 'atomic_potential');

#    boundwaves: #
figure('paperunits','points','papertype', '<custom>',
    'paperposition',[0 0 576 432], 'papersize', [576,432], 'Position', plot_window_position_A);
plot(waveup(:,1), waveup(:,3) , "linewidth", curve_linewidth, 'r');
hold on;
plot(waveup(:,1), waveup(:,4) , "linewidth", curve_linewidth, 'k');
plot(waveup(:,1), waveup(:,5) , "linewidth", curve_linewidth, 'b');
hold off;
l = legend("$n=1,l=0$","$n=2,l=0$","$n=2,l=1$");
set(l, 'fontsize', legend_fontsize);
set(gca, "fontsize", axis_fontsize, "linewidth", axis_linewidth);
xlabel("$r$ in au", "fontsize", xylabel_fontsize);
ylabel("$u_{nl}(r)$ in au$^{-0.5}$", "fontsize", xylabel_fontsize);
xlim([0,7]);
print('-dpdflatex', 'boundwaves');



### clean-up ###
clear all;
