#!/bin/bash

#insert used packages to the files 
#exp2a1l15Sol.tex and exp2a1l15Diff.tex

echo "adding missing packages..."
sed -i '1i\
\\documentclass{minimal}\
\\usepackage{amsmath}\
\\usepackage{newtxtext,newtxmath}\
\\usepackage{graphicx,color}\
\\usepackage{units}\
\\usepackage[papersize={576.00bp,432.00bp},text={576.00bp,432.00bp}]{geometry}\
\\begin{document}\
\t\\centering' *.tex

sed -i -e "\$a\ \\\end{document}" *.tex

#-i: edit in-place without creating another file
#1i: following phrase to the first line
#2i: following phrase to second line
#\$a: $ matches last line and a appends
#\\: backslash


