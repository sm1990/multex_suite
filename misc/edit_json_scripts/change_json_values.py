#!/usr/bin/python

import sys
import json
from collections import OrderedDict
import os

#sys.argv[0] would be name of the script...
filename = sys.argv[1]
print filename
with open(filename,'r') as f:
	data = json.load(f,object_pairs_hook=OrderedDict)

	#make changes to json-files here:
	#data['settings']['x_pixel'] = 1024
	#data['settings']['y_pixel'] = 1024
	data['specimen_structure']['x_dim'] = 64
	data['specimen_structure']['y_dim'] = 64
	data['slices'][0]['x_dim_unitcell'] = 64
	data['slices'][0]['y_dim_unitcell'] = 64
	#data['slices'][0]['z_dim'] = 0



with open(filename,'w') as out:
	json.dump(data,out,indent=3)
