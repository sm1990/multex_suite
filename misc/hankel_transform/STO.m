#STO-function from Toyoda paper
# r: radii at which to compute the function
# l: order of the planned spherical Hankel transform
# b: additional free parameter 
function y = STO(r,l,b)
  y = (2*b)^(l+1)*sqrt(2*b/doubleFactorial(2*l+2))*r.^l.*e.^ (-b*r);
end