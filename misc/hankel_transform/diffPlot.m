function diffPlot(filename,a,l)
  A = load(filename);
  if strfind(filename, "exp2")
    y = exp2Sol(A(:,1),a,l);
  else
    y = expSol(A(:,1),a,l);
  end
  figure;
  plot(A(:,1), A(:,2), '*');
  hold on;
  plot(A(:,1), y, 'r');
  hold off;
  z(:,2) = A(:,2)-y;
  z(:,1) = A(:,1);
  figure;
  plot(A(:,1),z(:,2));
  save("-ascii", "diff.txt", 'z');
end
