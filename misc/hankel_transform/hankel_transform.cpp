//Compile with:
//	g++ -std=c++11 -Wall -pedantic -O3 -I ../transpot/algorithm -c ../transpot/algorithm/numFunc.cc ../transpot/algorithm/hankelFunc.cc
//	g++ -std=c++11 -Wall -pedantic -O3 -I ../transpot/algorithm -o hankel_transform hankel_transform.cpp numFunc.o hankelFunc.o -lfftw3


//=================================================================================================
//included dependencies----------------------------------------------------------------------------
#include<iostream>
#include<vector>
#include<array>
#include<fstream>
#include <iomanip>

#include<hankelFunc.h>
#include<numFunc.h>



int main(){
	std::cout<<"Reading input array from file array_in.dat ...\n\n";
	numFunc input = numFunc("array_in.dat",2);

	std::cout<<"Before performing the spherical Hankel transform, the data will be spline fitted and";
	std::cout<<" thereafter sampled in a linear fashion to calculate the Hankel transform.\n";
	std::cout<<"How many times do you want the spline fit to be sampled?\n";
	unsigned int samples=10;
	std::cin>>samples;
	std::cout<<"\n";

	//prepare spherical Hankel transform
	hankelFunc transform(input, samples);

	while(true){		
		std::cout<<"Order of spherical Hankel transform?\n";
		unsigned int l=1;
		std::cin>>l;

		//perform spherical Hankel transform of order l	
		std::array<std::vector<double>,2> output = transform.hankelTrafo(l);

		//save results
		std::string output_file_name = "array_out_l="+std::to_string(l)+".dat";
		std::ofstream out(output_file_name);
		out<<std::setprecision(30);
		for (unsigned int i=0; i<output[0].size(); i++){
			out<<output[0][i]<<"\t"<<output[1][i]<<"\n";		
		}
		out.close();
		
		std::cout<<"Continue with a different order (y/n)?\n";
		char continue_check = 'n';
		std::cin>>continue_check;
		if (continue_check != 'y'){break;}
		std::cout<<"\n";
	}


}
