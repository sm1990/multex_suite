#analytical spherical Hankel transform of the test function f
# q: spatial frequencies at which to compute the function
# l: order of the spherical Hankel transform
# b: additional free parameter
function y = tilde_GTO(q,l,b)
  y = (2*pi*b)^(-0.25)*sqrt((4*b)^(l+2)/doubleFactorial(2*l+1))*sqrt(pi/(4*b))*(2*b)^(-(l+1))*(q).^l.*exp(-(1*q).^2/(4*b));
end