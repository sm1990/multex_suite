# computes the double factorial N!! = N*(N-2)*(N-4)*..
function y = doubleFactorial(N)
   result=1;
   while N>1
    result = result*N;
    N = N-2;
   end
   y = result;
end