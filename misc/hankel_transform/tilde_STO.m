#analytical spherical Hankel transform of the STO-function from Toyoda paper
# q: spatial frequencies at which to compute the function
# l: order of the spherical Hankel transform
# b: additional free parameter
function y = tilde_STO(q,l,b)
  y = (2*b)^(l+1)*sqrt(2*b/doubleFactorial(2*l+2))*2*b./(b^2+q.^2).^2.*(2*q./(b^2+q.^2)).^l;
end