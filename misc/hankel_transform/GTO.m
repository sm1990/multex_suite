#test function for the spherical Hankel transform 
# r: radii at which to compute the function
# l: order of the planned spherical Hankel transform
# b: additional free parameter 
function y = GTO(r,l,b)
  y = (2*pi*b)^(-0.25)*sqrt((4*b)^(l+2)/(doubleFactorial(2*l+1)))*r.^l.*exp(-b*r.^2);
end