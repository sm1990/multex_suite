prefix_of_files_to_evaluate = input("Input prefix of the files to be evaluated...\n","s");

ls_result = ls;

ls_result_row = ls_result(1,:);
for i=2:size(ls_result)(1);
  ls_result_row = cstrcat(ls_result_row, " ");
  ls_result_row = cstrcat(ls_result_row, ls_result(i,:));
endfor
files = strsplit(ls_result_row);


file_counter = 0;
for i = 1:length(files)
  file_name = files{i};
  file_name = strtrim(file_name);
  file_check = exist(file_name);
  
  if (file_check ==2)
    split_file_name = strsplit(file_name, '_');
    if (strcmp(split_file_name{1},prefix_of_files_to_evaluate))
      file_counter += 1;
      # get parameters from file name
      results(file_counter).element_string = split_file_name{1};
      U_acc_string = split_file_name{2}(1:length(split_file_name{2})-2);
      results(file_counter).U_acc = str2double(U_acc_string);
      results(file_counter).start_state_string = split_file_name{3};
      end_state_engy_string = split_file_name{4};
      results(file_counter).end_state_engy = str2double(end_state_engy_string);
      results(file_counter).end_state_l_ml = split_file_name{5};
      # calculate absolute intensity
      results(file_counter).intensity = transition_potential_intensity(file_name);
      fprintf("%s -> %s: \t %d \n", results(file_counter).start_state_string, results(file_counter).end_state_l_ml, results(file_counter).intensity);
    endif
  endif
  
endfor





clear -x 'results';
