//=================================================================================================
//included dependencies----------------------------------------------------------------------------
#include<string>
#include<exception>

#include<octave/oct.h>
#include<octave/ov-struct.h>
#include<octave/CMatrix.h>
#include<armadillo>
#include<fftw3.h>



//=================================================================================================
//load_transition.cpp------------------------------------------------------------------------------
//=================================================================================================
//source file for an octave function -> compile with: 
//	mkoctfile -Wall -pedantic -larmadillo -lfftw3 transition_potential_intensity.cpp


//----------global_constants-----------------------------------------------------------------------
const int fftw_estimate = 64;
const int fftw_forward = 1;
const int fftw_backward = -1;
const int pi = arma::datum::pi;



//=================================================================================================
//---utility-functions-----------------------------------------------------------------------------

//----------qd_FFT---------------------------------------------------------------------------------
///Quick and dirty in-place Fourier transform
/*!Calculates the in-place Fourier transform of an armadillo matrix. The Fourier transform 
   convention is:\n
	\verbatim
    FT^2[f(x,y)] = int_xy f(x,y) e^(iq_xx) e^(iq_yy) dx dy 
    FT^-2[F(q_x,q_y)] = 1/(2pi)^2 int_qxqy F(q_x,q_y) e^(-iq_xx) e^(-iq_yy) dqx dqy .
	\endverbatim
   Caveat: This function is NOT intended for multiple use (too slow!).
   @param[in,out] mat matrix to be Fourier transformed
   @param[in] x_dim real space x-dimension of the matrix
   @param[in] y_dim real space y-dimension of the matrix
   @param[in] direction perform Fourier transform in forward (direction = 1) 
		   or backward (direction = -1) direction*/  
void qd_FFT(arma::cx_mat& mat, double x_dim, double y_dim, int direction ){		
	unsigned int nx = static_cast<unsigned int>(mat.n_cols);
	unsigned int ny = static_cast<unsigned int>(mat.n_rows);	

	//calculate norm (Fourier -> Real space or Real -> Fourier space depending on direction)
	double norm=0;
	if (direction==fftw_backward){	
		double qx_extent = 2.*pi*nx/x_dim;
		double qstepX = qx_extent/nx; 
		double qy_extent = 2.*pi*ny/y_dim;
		double qstepY = qy_extent/ny; 
		norm=qstepX*qstepY/pow(2.*M_PI,2); 
	} else if (direction==fftw_forward) {
		//norm for forward transform might be wrong (untested)
		double stepX = x_dim/nx;			 
		double stepY = y_dim/ny;			 
		norm=stepX*stepY;					 
	} else {
		throw std::logic_error("FFT-direction not recognized!\n");		
	}
	
	
	//prepare FFT
	fftw_complex* in_out = reinterpret_cast<fftw_complex*>(mat.memptr());
	fftw_plan plan = fftw_plan_dft_2d(nx,ny,in_out,in_out,direction,fftw_estimate); 
	
	fftw_execute(plan);	

	mat*=norm;

	//clean-up
	fftw_destroy_plan(plan);	
}








//=================================================================================================
//---octave-function-------------------------------------------------------------------------------
DEFUN_DLD(transition_potential_intensity, args, ,
	"-*- texinfo -*-\n\
@deftypefn {Function File} {@var{intensity} =} transition_potential_intensity (@var{filename})\n\n\
Load the transition data saved in the binary file @var{filename} and calculate the summed absolute squared \
of the real space transition potential in Angstrom^2*eV/(A^2). \n\n\
@end deftypefn"){
	
	//check for correct input
	int nargin = args.length();
	if (nargin !=1){
		print_usage();	
	} else if ( !args(0).is_sq_string() && !args(0).is_dq_string() ){
		error("load_transition: expecting argument to be a string");	
	}
	

	//try to read input file
	//try to read input file
	arma::cx_cube input;
	arma::field<arma::cx_cube> input_field;
	arma::cx_mat temp;
	if (!error_state){
		std::string filename = args(0).string_value();
		if (!input.load(filename, arma::arma_binary)){
			if (!input_field.load(filename,arma::arma_binary)){
				std::string error_msg = "load_transition: could not read file "+filename;			
				error(error_msg.c_str());		
			}	
			temp = input_field(0).slice(0);
			temp.resize(input_field(1).n_rows, input_field(1).n_cols);	
			input = input_field(1);
			input.insert_slices(0,1);
			input.slice(0) = temp;	
		}
	}
	

	//produce output - if it fails show error_msg
	std::string error_msg_fs = "load_transition: unkown file structure: ";
	octave_value_list return_val;
	
	//get info about the transition described in the file	
	double x_dim=0, y_dim=0;
	if (!error_state){
		try{
			//size in x-direction?
			x_dim = real(input(3,0,0));
			//size in y-direction?
			y_dim = real(input(4,0,0));
		} catch (const std::exception& ex){
			error_msg_fs += ex.what();
			error(error_msg_fs.c_str());	
		}
	}


	//compute absolute value
	double absolute_value = 0;
	if (!error_state){
		try{
			//arma::mat abs(arma::size(input.slice(1)),arma::fill::zeros);			
			unsigned int ns = input.n_slices;
			for (unsigned int is=1; is<ns; is++){
				arma::cx_mat component = input.slice(is);
				qd_FFT(component,x_dim,y_dim,fftw_backward);
				if (is == 1){			
					absolute_value += arma::accu(pow(arma::abs(component),2));
					//abs += pow(arma::abs(component),2);
				} else {
					absolute_value += arma::accu(pow(arma::abs(component),2));
					//abs -= pow(arma::abs(component),2);
					//absolute_value -= arma::accu(pow(arma::abs(component),2));
				}
					
			}
			//absolute_value = arma::accu(abs);
			const double nx = static_cast<double>(input.slice(0).n_cols);
			const double ny = static_cast<double>(input.slice(0).n_rows);
			const double pixelsize_factor = x_dim*y_dim/(nx*ny);	
			absolute_value *= pixelsize_factor;	
		} catch (const std::exception& ex){
			std::string error_msg = "load_transition: Fourier transform failed";
			error_msg += ex.what();
			error(error_msg.c_str());
		}	
	}
	
			
		
	if (!error_state){
		return_val(0) = absolute_value;
	}


	return return_val;
}
