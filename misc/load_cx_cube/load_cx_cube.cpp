//=================================================================================================
//included dependencies----------------------------------------------------------------------------
#include<exception>

#include<octave/oct.h>
#include<octave/CMatrix.h>
#include<octave/ov-struct.h>
#include<armadillo>


//=================================================================================================
//load_cx_cube.cpp------------------------------------------------------------------------------
//=================================================================================================
//source file for an octave function -> compile with: 
//	mkoctfile -Wall -pedantic -larmadillo load_cx_cube.cpp



//----------copy_cxmat_complexmatrix---------------------------------------------------------------
///copy the complex armadillo matrix \p in to a complex octave matrix
/*!Function used to convert a complex armadillo matrix cx_mat into a complex octave matrix
   ComplexMatrix.
   @param[in] in cx_mat to be converted to ComplexMatrix*/
static ComplexMatrix copy_cxmat_complexmatrix(const arma::cx_mat& in){
	octave_idx_type nx = in.n_cols;
	octave_idx_type ny = in.n_rows;
	
	ComplexMatrix out(ny,nx);
	//copy matrix 				
	for (int iy=0; iy<ny; iy++){
		for (int ix=0; ix<ny; ix++){			
			out(iy,ix) = in(iy,ix);
		}
	}

	return out;
}



//=================================================================================================
//---octave-function-------------------------------------------------------------------------------
DEFUN_DLD(load_cx_cube, args, ,
	"-*- texinfo -*-\n\
@deftypefn {Function File} {@var{cx_cube} =} load_cx_cube (@var{filename})\n\n\
Load a complex armadillo cube from the file @var{filename} and save it within the structure \
@var{cx_mat}. The structure @var{cx_mat} contains a field n_slices which is an integer value \
giving the number of slices in the cube and a complex matrix for every slice of the cube. \
The j-th slice is saved in the field slice_j of @var{cx_cube}.\
@end deftypefn"){	

	//check for correct input
	int nargin = args.length();
	if (nargin !=1){
		print_usage();	
	} else if ( !args(0).is_sq_string() && !args(0).is_dq_string() ){
		error("load_cx_cube: expecting argument to be a string");	
	}
	

	//try to read input file
	arma::cx_cube input;
	if (!error_state){
		std::string filename = args(0).string_value();		
		if (!input.load(filename)){
			std::string error_msg = "load_cx_cube: could not read file "+filename;			
			error(error_msg.c_str());		
		}	
	}
	


	//convert complex armadillo cube to octave struct with complex matrices
	octave_value_list return_val;
	if (!error_state){
		octave_scalar_map cxcube;
		try{		
			unsigned int ns = input.n_slices;
			cxcube.assign("n_slices", ns);
					
			for (unsigned int is=0; is<ns; is++){
				//copy matrix in the is-th slice
				ComplexMatrix temp = copy_cxmat_complexmatrix(input.slice(is));				
				std::string key = "slice_"+std::to_string(is);
				cxcube.assign(key,temp);			
			}	
			return_val(0) = cxcube;
		} catch (const std::exception& ex){
			const char error_msg[] = "load_cx_cube: failed to copy cube to octave memory";			
			error(error_msg);
		}
	}



	return return_val;
}
