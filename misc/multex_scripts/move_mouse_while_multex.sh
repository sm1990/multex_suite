#!/bin/bash

time_before_moving=60
time_between_moves=120
time_before_shutdown=120

xdotool sleep $time_before_moving
while pgrep -x "multex" > /dev/null
do
	echo "moving mouse..."
	xdotool mousemove_relative 5 5
	xdotool sleep $time_between_moves
done
echo "cancel to avoid shutdown in $time_before_shutdown seconds..."
shutdown

