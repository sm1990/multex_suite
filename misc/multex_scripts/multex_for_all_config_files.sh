#!/bin/bash


for f in *.json
do
    [ -f "$f" ] || break #keeps loop from doing a single iteration if no file found
    echo "Processing $f ..."
	./multex $f > /dev/null   #>/dev/null: redirects output to null device which discards information
done



# adding "2>&1" after "/dev/null" would redirect the standard error stream (2) to the standard output stream (1) so that it is also discarded
