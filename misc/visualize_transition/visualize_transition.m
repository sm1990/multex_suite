if exist("filename", "var") != 1
	filename = input("Name of input file?\n","s");
endif

filename = strtrim(filename);
[info,mat,rs_mat] = load_transition(filename);

## Plot data ##
n_components = 0;
if (info.relativistic)
  n_components = 4;
else
  n_components = 1;
endif

display(filename);
for comp = 0:n_components-1
  key = strcat("component_",num2str(comp));

  plot_mat = real(rs_mat.(key));
  imagesc(plot_mat);
  colorbar();
  title_str = strcat("Re(A_",num2str(comp),"(x,y))");
  title(title_str, 'Interpreter', 'none');
  figure();
  
  plot_mat = imag(rs_mat.(key));
  imagesc(plot_mat);
  colorbar();
  title_str = strcat("Im(A_",num2str(comp),"(x,y))");
  title(title_str, 'Interpreter', 'none');
  if (comp<n_components-1)
    figure();
  endif
endfor




clear("filename","n_components","key","comp","plot_mat","title_str");
