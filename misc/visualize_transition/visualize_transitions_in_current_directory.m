#get filenames of transitions
ls_result = ls;
ls_result_row = ls_result(1,:);
for i=2:size(ls_result)(1);
  ls_result_row = cstrcat(ls_result_row, " ");
  ls_result_row = cstrcat(ls_result_row, ls_result(i,:));
endfor
transition_filenames = strsplit(ls_result_row);

graphics_toolkit('qt');
for i=1:length(transition_filenames)
	printf("Displaying transition %d of %d\n", i, length(transition_filenames));    
	filename = transition_filenames{i};
    visualize_transition;
    disp ("press return to continue");
    pause ();
	close all;
endfor
