//=================================================================================================
//included dependencies----------------------------------------------------------------------------
#include<string>
#include<exception>

#include<octave/oct.h>
#include<octave/ov-struct.h>
#include<octave/CMatrix.h>
#include<armadillo>
#include<fftw3.h>

#include<iostream>



//=================================================================================================
//load_transition.cpp------------------------------------------------------------------------------
//=================================================================================================
//source file for an octave function -> compile with: 
//	mkoctfile -Wall -pedantic -larmadillo -lfftw3 load_transition.cpp


//----------global_constants-----------------------------------------------------------------------
const int fftw_estimate = 64;
const int fftw_forward = 1;
const int fftw_backward = -1;
const int pi = arma::datum::pi;



//=================================================================================================
//---utility-functions-----------------------------------------------------------------------------

//----------qd_FFT---------------------------------------------------------------------------------
///Quick and dirty in-place Fourier transform
/*!Calculates the in-place Fourier transform of an armadillo matrix. The Fourier transform 
   convention is:\n
	\verbatim
    FT^2[f(x,y)] = int_xy f(x,y) e^(iq_xx) e^(iq_yy) dx dy 
    FT^-2[F(q_x,q_y)] = 1/(2pi)^2 int_qxqy F(q_x,q_y) e^(-iq_xx) e^(-iq_yy) dqx dqy .
	\endverbatim
   Caveat: This function is NOT intended for multiple use (too slow!).
   @param[in,out] mat matrix to be Fourier transformed
   @param[in] x_dim real space x-dimension of the matrix
   @param[in] y_dim real space y-dimension of the matrix
   @param[in] direction perform Fourier transform in forward (direction = 1) 
		   or backward (direction = -1) direction*/  
void qd_FFT(arma::cx_mat& mat, double x_dim, double y_dim, int direction ){		
	unsigned int nx = static_cast<unsigned int>(mat.n_cols);
	unsigned int ny = static_cast<unsigned int>(mat.n_rows);	

	//calculate norm (Fourier -> Real space or Real -> Fourier space depending on direction)
	double norm=0;
	if (direction==fftw_backward){	
		double qx_extent = 2.*pi*nx/x_dim;
		double qstepX = qx_extent/nx; 
		double qy_extent = 2.*pi*ny/y_dim;
		double qstepY = qy_extent/ny; 
		norm=qstepX*qstepY/pow(2.*M_PI,2); 
	} else if (direction==fftw_forward) {
		//norm for forward transform might be wrong (untested)
		double stepX = x_dim/nx;			 
		double stepY = y_dim/ny;			 
		norm=stepX*stepY;					 
	} else {
		throw std::logic_error("FFT-direction not recognized! -> Aborting\n");		
	}
	
	
	//prepare FFT
	fftw_complex* in_out = reinterpret_cast<fftw_complex*>(mat.memptr());
	fftw_plan plan = fftw_plan_dft_2d(nx,ny,in_out,in_out,direction,fftw_estimate); 
	
	fftw_execute(plan);	

	mat*=norm;

	//clean-up
	fftw_destroy_plan(plan);	
}


//----------find_mid-------------------------------------------------------------------------------
///find the integer middle of the number \p n
/*!If n is an even number, n/2 is returned. If n is an odd number, (n+1)/2 is returned.
   @param[in] n number to be halfed*/
unsigned int find_mid(unsigned int n){
	if (n%2==0){return n/2;} else {return static_cast<unsigned>((n+1)/2);}
}


//----------p_acc----------------------------------------------------------------------------------
///calculate correct access indices for a one-dimensional array with periodic boundary conditions
/*!Returns an decreased or increased \p index to adress and array of size \p size, which can be 
   used to access arrays with periodic boundary conditions. If index is lesser than size or
   greater equal two times \p size a range_error-exception is thrown.
   @param[in] index index to be used in a periodic manner
   @param[in] size size of the array the index is to be used with*/
unsigned int p_acc(int index, unsigned int size){
	int size_int = static_cast<int>(size);	
	
	//move index back into array	
	if (index<0){
		index+=size;
	} else if (index>=size_int){
		index-=size;	
	}
	
	//check if new index is within array range
	if ( (index<0) || (index>=size_int) ){
		//throw std::range_error("Periodic Index loops more than once! -> Aborting\n");	
		return p_acc(index,size);
	} else {
		return static_cast<unsigned int>(index);
	}
}


//----------quadrantSwap---------------------------------------------------------------------------
///swaps the quadrants of \p in to counteract fftw
/*!Since fftw requires zero-frequency to be in the top left corner of the matrix in Fourier space,
   the transformed matrix has its' (x,y) = (0,0) point in the top left corner. By quadrant swapping
   the matrix after the Fourier transform the atomic center goes from (0,0) to the matrix center, 
   which is more convenient for later calculations and more pleasent for human viewers.
   @param[in] in matrix to be quadrant swapped*/
static arma::cx_mat quadrant_swap(const arma::cx_mat& in){
	unsigned int nx = static_cast<unsigned int>(in.n_cols);
	unsigned int ny = static_cast<unsigned int>(in.n_rows);
	
	//find  matrix center
	unsigned int x_mid = find_mid(nx);
	unsigned int y_mid = find_mid(ny);

	//prepare output matrix
	arma::cx_mat swapped(ny,nx);
	
	//swap quadrants
	for (unsigned int iy=0; iy<ny; iy++){
		for (unsigned int ix=0; ix<nx; ix++){
			swapped(iy,ix) = in(p_acc(iy-y_mid, ny),p_acc(ix-x_mid,nx));		
		}	
	}

	return swapped;
}


//----------copy_cxmat_complexmatrix---------------------------------------------------------------
///copy the complex armadillo matrix \p in to a complex octave matrix
/*!Function used to convert a complex armadillo matrix cx_mat into a complex octave matrix
   ComplexMatrix.
   @param[in] in cx_mat to be converted to ComplexMatrix*/
static ComplexMatrix copy_cxmat_complexmatrix(const arma::cx_mat& in){
	octave_idx_type nx = in.n_cols;
	octave_idx_type ny = in.n_rows;
	
	ComplexMatrix out(ny,nx);
	//copy matrix 				
	for (int iy=0; iy<ny; iy++){
		for (int ix=0; ix<ny; ix++){			
			out(iy,ix) = in(iy,ix);
		}
	}

	return out;
}





//=================================================================================================
//---octave-function-------------------------------------------------------------------------------
DEFUN_DLD(load_transition, args, ,
	"-*- texinfo -*-\n\
@deftypefn {Function File} {[@var{info},@var{mat},@var{rs_mat}] =} load_transition (@var{filename})\n\n\
Load the transition data saved in the binary file @var{filename}. \n\n\
General infos about the transition potential contained in the file are saved in the @var{info} \
structure which contains the following fields:\n\n\
@table @samp\n\
@item relativistic\n\
Boolean indicating whether the transition potential calculation was relativistic.\n\n\
@item Z\n\
Atomic number of the ionized atom.\n\n\
@item Uacc\n\
Kinetic energy of the beam electrons in keV.\n\n\
@item x_dim\n\
Size of the transition potential in x-direction in Angstrom.\n\n\
@item y_dim\n\
Size of the potential in y-direction in Angstrom.\n\n\
@item x_pixel\n\
Amount of pixel in the transition potential in x-direction.\n\n\
@item y_pixel\n\
@end table\n\
Amount of pixel in the transition potential in y-direction.\n\n\
The complex matrices actually contained in the file are saved within \
the @var{mat}-structure, where the j-th saved matrix corresponds to the \
'component_j'-field. If the transition potential stems from a \
non-relativistic calculation, there will be only one component \
(i.e. mat.component_0), while for a transition potential from a relativistic \
calculation @var{mat} will contain four components. \
The SI-unit of the matrices in @var{mat} is Angstrom^2*sqrt(eV)/A.\n\n\
While running load_transition, the corresponding matrices in real space are \
also calculated via inverse Fourier transform and saved within the @var{rs_mat} \
structure. The SI-unit of these matrices is sqrt(eV)/A.\
@end deftypefn"){
	
	//check for correct input
	int nargin = args.length();
	if (nargin !=1){
		print_usage();	
	} else if ( !args(0).is_sq_string() && !args(0).is_dq_string() ){
		error("load_transition: expecting argument to be a string");	
	}
	

	//try to read input file
	arma::cx_cube input;
	arma::field<arma::cx_cube> input_field;
	arma::cx_mat temp;
	if (!error_state){
		std::string filename = args(0).string_value();
		if (!input.load(filename, arma::arma_binary)){
			if (!input_field.load(filename,arma::arma_binary)){
				std::string error_msg = "load_transition: could not read file "+filename;			
				error(error_msg.c_str());		
			}	
			temp = input_field(0).slice(0);
			temp.resize(input_field(1).n_rows, input_field(1).n_cols);	
			input = input_field(1);
			input.insert_slices(0,1);
			input.slice(0) = temp;	
		}
	}

	//produce output - if it fails show error_msg
	const char error_msg_fs[] = "load_transition: unkown file structure";
	octave_value_list return_val;
	
	//get info about the transition described in the file	
	octave_scalar_map info;	
	double x_dim=0, y_dim=0;
	if (!error_state){
		std::cout<<"input.n_slices: "<<input.n_slices<<"\n";
		//is it from a relativistic calculation?		
		if (input.n_slices==5) {
			info.assign("relativistic", true);
		} else if (input.n_slices==2) {
			info.assign("relativistic", false);		
		} else {error(error_msg_fs);}
		
		try{
			//atomic number?			
			info.assign("Z", real(input(0,0,0)) );
			//acceleration voltage?
			info.assign("Uacc", real(input(1,0,0)) );
			//energy loss?
			info.assign("engy_loss", real(input(2,0,0)) );
			//size in x-direction?
			x_dim = real(input(3,0,0));
			info.assign("x_dim", x_dim);
			//size in y-direction?
			y_dim = real(input(4,0,0));
			info.assign("y_dim",y_dim);
			//pixel in x-direction?
			info.assign("x_pixel", input.n_cols);
			//pixel in y-direction?
			info.assign("y_pixel", input.n_rows);
		} catch (const std::exception& ex){
			error(error_msg_fs);	
		}
	}
	
	//read transition matrix
	octave_scalar_map mat;	
	if (!error_state){
		try{		
			unsigned int ns = input.n_slices;		
			for (unsigned int is=1; is<ns; is++){
				//copy matrix in the is-th slice
				ComplexMatrix temp = copy_cxmat_complexmatrix(input.slice(is));				
				std::string key = "component_"+std::to_string(is-1);
				mat.assign(key,temp);			
			}	
		} catch (const std::exception& ex){
			error(error_msg_fs);
		}
	}


	//transform matrix to real space and quadrant swap
	octave_scalar_map realspace_mat;
	if (!error_state){
		try{
			unsigned int ns = input.n_slices;
			for (unsigned int is=1; is<ns; is++){
				arma::cx_mat component = input.slice(is);
				qd_FFT(component,x_dim,y_dim,fftw_backward);
				component = quadrant_swap(component);			
				ComplexMatrix temp = copy_cxmat_complexmatrix(component);
				std::string key = "component_"+std::to_string(is-1);
				realspace_mat.assign(key,temp);
			}
		} catch (const std::exception& ex){
			const char error_msg[] = "load_transition: Fourier transform failed";
			error(error_msg);
		}	
	}
	
			
		
	if (!error_state){
		return_val(0) = info;
		return_val(1) = mat;
		return_val(2) = realspace_mat;
	}


	return return_val;
}
