pixel = 1024;
dim = 16;

pixel_size = dim/pixel;
save_precision(20)


values = (-dim+pixel_size)/2: pixel_size : (dim-pixel_size)/2;

x_values = ones(size(values))'*values;
y_values = values' * ones(size(values));

figure();

ex =  -dx_projV_Z14(x_values,y_values);
#ex += -dx_projV_Z14(x_values+dim, y_values);
#ex += -dx_projV_Z14(x_values-dim, y_values);
#ex += -dx_projV_Z14(x_values, y_values+dim);
#ex += -dx_projV_Z14(x_values, y_values-dim);
#ex += -dx_projV_Z14(x_values+dim, y_values+dim);
#ex += -dx_projV_Z14(x_values-dim, y_values+dim);
#ex += -dx_projV_Z14(x_values+dim, y_values-dim);
#ex += -dx_projV_Z14(x_values-dim, y_values-dim);
save -ascii ex.dat ex;
imagesc(ex);
colorbar();
figure();


ey = -dy_projV_Z14(x_values,y_values);
#ey += -dy_projV_Z14(x_values+dim,y_values);
#ey += -dy_projV_Z14(x_values-dim,y_values);
#ey += -dy_projV_Z14(x_values,y_values+dim);
#ey += -dy_projV_Z14(x_values,y_values-dim);
#ey += -dy_projV_Z14(x_values+dim,y_values+dim);
#ey += -dy_projV_Z14(x_values-dim,y_values+dim);
#ey += -dy_projV_Z14(x_values+dim,y_values-dim);
#ey += -dy_projV_Z14(x_values-dim,y_values-dim);
save -ascii ey.dat ey;
imagesc(ey);
colorbar();
figure();

pot = projV_Z14(x_values, y_values);
#pot += projV_Z14(x_values+dim, y_values);
#pot += projV_Z14(x_values-dim, y_values);
#pot += projV_Z14(x_values, y_values+dim);
#pot += projV_Z14(x_values, y_values-dim);
#pot += projV_Z14(x_values+dim, y_values+dim);
#pot += projV_Z14(x_values-dim, y_values+dim);
#pot += projV_Z14(x_values+dim, y_values-dim);
#pot += projV_Z14(x_values-dim, y_values-dim);
save -ascii pot.dat pot;
imagesc(pot);
colorbar();


clear all;

