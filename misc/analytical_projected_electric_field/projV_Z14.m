# projected electrostatic scattering potential 
# of silicon  in Volt*Angstrom
function y = projV_Z14(x,y)
  #fit parameters for Z=14 (see Kirkland p.254):
  a_1 = 1.06543892e0; #Angstrom^(-1)
  b_1 = 1.04118455e0; #Angstrom^(-2)
  a_2 = 1.20143691e-1; #Angstrom^(-1)
  b_2 = 6.87113368e1; #Angstrom^(-2)
  a_3 = 1.80915263e-1; #Angstrom^(-1)
  b_3 = 8.87533926e-2; #Angstrom^(-2)
  c_1 = 1.12065620e0; #Angstrom
  d_1 = 3.70062619e0; #Angstrom^2
  c_2 = 3.05452816e-2; #Angstrom
  d_2 = 2.14097897e-1; #Angstrom^2
  c_3 = 1.59963502e0; #Angstrom
  d_3 = 9.99096638e0; #Angstrom^2
  
  a_br = 0.52917721067; #Bohr radius in Angstrom
  e = 14.4; #elementary charge in Volt*Angstrom (see Kirkland p.253)
  
  r = sqrt(x.^2+y.^2);
  
  constl = 4.*pi^ 2*a_br*e;
  suml = a_1*besselk(0,2.*pi*r*sqrt(b_1));
  suml += a_2*besselk(0,2.*pi*r*sqrt(b_2));
  suml += a_3*besselk(0,2.*pi*r*sqrt(b_3));
  
  constr = 2.*pi^2*a_br*e;
  sumr = c_1/d_1*exp(-pi^2*r.^2/d_1);
  sumr += c_2/d_2*exp(-pi^2*r.^2/d_2);
  sumr += c_3/d_3*exp(-pi^2*r.^2/d_3);
  
  y = constl*suml+constr*sumr;
  
endfunction