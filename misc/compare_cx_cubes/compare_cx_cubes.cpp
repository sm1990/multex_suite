//=================================================================================================
//included dependencies----------------------------------------------------------------------------
#include<exception>

#include<octave/oct.h>
#include<octave/CMatrix.h>
#include<octave/ov-struct.h>
#include<armadillo>


//=================================================================================================
//load_cx_cube.cpp------------------------------------------------------------------------------
//=================================================================================================
//source file for an octave function -> compile with: 
//	mkoctfile -Wall -O2 -pedantic -larmadillo compare_cx_cubes.cpp







//=================================================================================================
//---octave-function-------------------------------------------------------------------------------
DEFUN_DLD(compare_cx_cubes, args, ,
	"-*- texinfo -*-\n\
@deftypefn {Function File} {} compare_cx_cube (@var{filename_cube_a}, @var{filename_cube_b})\n\n\
Loads two complex armadillo cubes (cube_a and cube_b) from the files @var{filename_cube_a} \
and @var{filename_cube_b} and compares each cube-slice of the two cubes, outputting the \
maximal and minmal difference found in both the real and the imagninary parts of the \
two cubes.\n\n \
If one cube has more cube-slices than the other, all the cube-slices up to the last \
slice of the smaller cube will be compared. If the amount of columns and rows in \
the cubes does not match, an error is thrown. For this function to work, both input \
files must stem from an armadillo save command.\
@end deftypefn"){	

	//check for correct argument input
	int nargin = args.length();
	if (nargin !=2){
		print_usage();	
	} else if ( !args(0).is_sq_string() && !args(0).is_dq_string() ){
		error("compare_cx_cubes: expecting arguments to be strings");	
	} else if ( !args(1).is_sq_string() && !args(1).is_dq_string() ){
		error("compare_cx_cubes: expecting arguments to be strings");
	}
	

	//try to read input files
	arma::cx_cube input_a, input_b;
	if (!error_state){
		std::string filename_a = args(0).string_value();		
		if (!input_a.load(filename_a)){
			std::string error_msg = "compare_cx_cubes: could not read file "+filename_a;			
			error(error_msg.c_str());		
		}
		std::string filename_b = args(1).string_value();
		if (!input_b.load(filename_b)){
			std::string error_msg = "compare_cx_cubes: could not read file "+filename_b;			
			error(error_msg.c_str());		
		}	
	}

	//check if cube-slices are comparable
	if (!error_state){
		if ( (input_a.n_rows != input_b.n_rows) || (input_a.n_cols != input_b.n_cols) ){
			std::string error_msg = "compare_cx_cubes: x/y-dimensions of the cubes do not match";
			error(error_msg.c_str());	
		}	
	}

	//compare the two cubes
	if (!error_state){
		unsigned int slices_a = input_a.n_slices;
		unsigned int slices_b = input_b.n_slices;	
		unsigned int slice_compare_max;
		if (slices_a>=slices_b){
			slice_compare_max = slices_b;		
		} else {
			slice_compare_max = slices_a;		
		}

		octave_stdout<<"\nComparing "<< slice_compare_max<<" / "<<slices_a<<" cube-slices ";
		octave_stdout<<"in cube_a to "<<slice_compare_max<<" / "<<slices_b<<" cube_slices ";
		octave_stdout<<"in cube_b.\n\n";

		arma::cx_mat diff;
		arma::mat real_diff, imag_diff;		
		for (unsigned int is=0; is<slice_compare_max; is++){
			diff = input_a.slice(is)-input_b.slice(is);
			real_diff = arma::real(diff);
			imag_diff = arma::imag(diff);
			octave_stdout<<"Cube-slice "<<is+1<<":\n";
			octave_stdout<<"Maximum real difference: "<<real_diff.max()<<"\n";
			octave_stdout<<"Minimal real difference: "<<real_diff.min()<<"\n";
			octave_stdout<<"Maximum imaginary difference: "<<imag_diff.max()<<"\n";
			octave_stdout<<"Minimal imaginary difference: "<<imag_diff.min()<<"\n";
			octave_stdout<<"\n\n";		
		}
	}
	


	return octave_value_list ();


	
}
