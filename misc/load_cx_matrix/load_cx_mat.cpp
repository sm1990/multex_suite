//=================================================================================================
//included dependencies----------------------------------------------------------------------------
#include<octave/oct.h>
#include<octave/CMatrix.h>
#include<armadillo>


//=================================================================================================
//load_cx_mat.cpp------------------------------------------------------------------------------
//=================================================================================================
//source file for an octave function -> compile with: 
//	mkoctfile -Wall -pedantic -larmadillo load_cx_mat.cpp



//----------copy_cxmat_complexmatrix---------------------------------------------------------------
///copy the complex armadillo matrix \p in to a complex octave matrix
/*!Function used to convert a complex armadillo matrix cx_mat into a complex octave matrix
   ComplexMatrix.
   @param[in] in cx_mat to be converted to ComplexMatrix*/
static ComplexMatrix copy_cxmat_complexmatrix(const arma::cx_mat& in){
	octave_idx_type nx = in.n_cols;
	octave_idx_type ny = in.n_rows;
	
	ComplexMatrix out(ny,nx);
	//copy matrix 				
	for (int iy=0; iy<ny; iy++){
		for (int ix=0; ix<ny; ix++){			
			out(iy,ix) = in(iy,ix);
		}
	}

	return out;
}



//=================================================================================================
//---octave-function-------------------------------------------------------------------------------
DEFUN_DLD(load_cx_mat, args, ,
	"-*- texinfo -*-\n\
@deftypefn {Function File} {@var{cx_mat} =} load_cx_mat (@var{filename})\n\n\
Load a complex armadillo matrix from the file @var{filename} and save it within the complex \
octave matrix @var{cx_mat}. \
@end deftypefn"){	

	//check for correct input
	int nargin = args.length();
	if (nargin !=1){
		print_usage();	
	} else if ( !args(0).is_sq_string() && !args(0).is_dq_string() ){
		error("load_cx_mat: expecting argument to be a string");	
	}
	

	//try to read input file
	arma::cx_mat input;
	if (!error_state){
		std::string filename = args(0).string_value();		
		if (!input.load(filename)){
			std::string error_msg = "load_cx_mat: could not read file "+filename;			
			error(error_msg.c_str());		
		}	
	}
	
	
	//convert complex armadillo matrix to complex octave matrix
	octave_value_list return_val;
	if (!error_state){	
		ComplexMatrix output = copy_cxmat_complexmatrix(input);
		return_val(0) = output;
	}


	return return_val;
}
