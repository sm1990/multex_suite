#include<fstream>
#include<cmath>
#include<cassert>
#include<iostream>

#include<armadillo>
#include<fftw3.h>

//Compile with 
//g++ -std=c++0x -Wall -pedantic -o qd_FFT-Test qd_FFT-Test.cpp -larmadillo -lfftw3



//----------qd_FFT---------------------------------------------------------------------------------
/*quick and dirty in-place Fourier transform of the matrix mat with the real (direction=forward=1)
  or reciprocal (direction=backward=-1) space dimensions x_size and y_size;
  the Fourier transform convention is
	FT^2[f(x,y)] = int_xy f(x,y) e^(iq_xx) e^(iq_yy) dx dy
	FT^-2[F(q_x,q_y)] = 1/(2pi)^2 int_qxqy F(q_x,q_y) e^(-iq_xx) e^(-iq_yy) dqx dqy;
  Caveat: This function is NOT intended for multiple use (too slow!)*/
void qd_FFT(arma::cx_mat& mat, double x_size, double y_size, int direction ){		
	unsigned int nx = static_cast<unsigned int>(mat.n_cols);
	unsigned int ny = static_cast<unsigned int>(mat.n_rows);	

	//calculate norm (Fourier -> Real space or Real -> Fourier space depending on direction)
	double norm=0;
	if (direction==-1){	
		double qstepX =x_size/nx; 
		double qstepY = y_size/ny; 
		norm=qstepX*qstepY/pow(2.*M_PI,2); 
	} else if (direction==1) {
		//norm for forward transform might be wrong (untested)
		double stepX = x_size/nx;			 
		double stepY = y_size/ny;			 
		norm=stepX*stepY;					 
	} else {
		std::cout<<"FFT-direction not recognized! -> Aborting\n";
		std::terminate();	
	}
	
	
	//prepare FFT
	fftw_complex* in_out = reinterpret_cast<fftw_complex*>(mat.memptr());
	fftw_plan plan = fftw_plan_dft_2d(ny,nx,in_out,in_out,direction,FFTW_ESTIMATE);
	
	fftw_execute(plan);	

	mat*=norm;

	//clean-up
	fftw_destroy_plan(plan);	
}



//periodic array access
static inline unsigned int pat(int index, unsigned int size){		
	if (index<0){
		assert("This bug means: pat index too low" && static_cast<unsigned int>(abs(index))<size);
		return size+index;	
	} else if (static_cast<unsigned int>(abs(index))>size){
		assert("This bug means: pat index too high" && static_cast<unsigned int>(index)<2*size);
		return index-size;
	} else {
		return index;
	}	
}

//fourier transform of a gaussian
double FTgauss(double rho, double a){
	return M_PI/a*exp(-pow(rho,2)/(4.*a));
}

//gaussian
double gauss(double rho, double a){
	return exp(-a*pow(rho,2));
}


//the program shows that an image in fourier space with qx=qy=0 in the upper left corner is tranformed
//to real space with x=y=0 in the upper left corner
int main(){
	unsigned int nx = 128, ny=128;
	//dimensions of lattic in fourier space
	double qxExtent = 14, qyExtent = 14; 
	double qxStep = qxExtent/nx, qyStep = qyExtent/ny;
	double a=0.1;


	//initialize arrays
	arma::cx_mat in(ny,nx);
	arma::cx_mat comp(ny,nx);


	int xstart,xend,ystart,yend;
	if (nx%2==0){
		xstart = -static_cast<int>(nx)/2;
		xend = -xstart-1;	
	} else {
		xstart = -(static_cast<int>(nx)-1)/2;
		xend = -xstart;
	}
	if (ny%2==0){
		ystart = -static_cast<int>(ny)/2;
		yend = -ystart-1;		
	} else {
		ystart = -(static_cast<int>(ny)-1)/2;
		yend = -ystart;		
	}
	


	//assign start values to in
	for (int iy=ystart; iy<=yend; iy++){
		for (int ix=xstart; ix<=xend; ix++){
			double rho = sqrt(pow(ix*qxStep,2)+pow(iy*qyStep,2));			
			in(pat(iy,ny),pat(ix,nx)) = FTgauss(rho,a);			
		}
	}


	//forward FFT
	qd_FFT(in, qxExtent, qyExtent, -1);	

	//calculate real-space dimensions	
	double xExtent =2.*M_PI*nx/(qxExtent);
	double yExtent =2.*M_PI*ny/(qyExtent);
	double xStep = xExtent/nx;
	double yStep = yExtent/ny;

	//calculate comp-array for comparison purposes
	for (int iy=ystart; iy<=yend; iy++){
		for (int ix=xstart; ix<=xend; ix++){
			double rho = sqrt(pow(ix*xStep,2)+pow(iy*yStep,2));			
			comp(pat(iy,ny),pat(ix,nx)) = 	gauss(rho,a);		
		}
	}

	arma::mat diff(ny,nx);
	diff = arma::real(comp)-arma::real(in);
	diff.save("diff.txt", arma::raw_ascii);

	

}
