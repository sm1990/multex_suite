# Introduction
The program `multex/MultEx` ("Multislice Extended") was written during the phd-thesis \cite MyThesis and is intended to be used for simulating images taken with a Transmission Electron Microscope (TEM). `MultEx` can use several different multislice algorithms for this purpose, all of which with varying degrees of correctness with regards to special relativity. 

# MultEx setup
## Requirements and recommendations
Aside from a current C++ compiler (e.g. the [GNU Compiler Collection - GCC](https://gcc.gnu.org/)), `multex` requires the 
following libraries:
* [Armadillo](http://arma.sourceforge.net/) \cite Sanderson2016 \n
  A linear algebra library for C++ that `multex` uses for almost all matrix calculations.
* [FFTW](http://www.fftw.org/)  \cite Frigo2005 \n
  The library with the fastest Fourier transform in the west powers the multitude of Fourier transforms that are
  required for any multislice simulation in `multex`.
* [Boost](http://www.boost.org/) \n
  A host of useful libraries for C++. `MultEx` heavily utilizes the Property Tree and the Filesystem library for
  user input and output.

Although technically not required, [cmake](https://cmake.org/) is very helpful when building multex since `multex` comes with its own `CMakeLists.txt` file. To visualize the TEM images resulting from a `multex` run, the programs [ImageJ](https://imagej.net/Welcome) \cite Schneider2012 or [octave](https://www.gnu.org/software/octave/) \cite Eaton2015 (or any other program that can visualize text images) can be used. Hardware-wise, a fast CPU is recommended. For the simulation of elemental maps `multex` can utilize several processor cores at once, calculating the wave functions resulting from inelastic scattering in parallel for each atom in the current slice. Additionally, `multex` requires enough RAM to store several matrices of double values with pixel dimension corresponding to the 
pixel dimensions of the resulting image at once. Since the amount of matrices that have to be stored and the desired pixel dimensions vary depending on the input parameters, giving a general RAM requirement for `multex` is not possible.

## Building multex
To build `multex`, using `cmake` is highly recommended. If the required components (i.e. GCC, Armadillo, FFTW and Boost) are properly installed, `multex` can be compiled by performing the following steps:
1. Make sure Armdaillo, FFTW and Boost are in your path.
2. Open your shell of choice.
3. Navigate to the `multex/build` directory.
4. Run the command
~~~~~~~~~~~~~~~~~~~~~~~ 
	cmake -G "Unix Makefiles" ..
~~~~~~~~~~~~~~~~~~~~~~~ 
	to generate a makefile for multex.
5. Compile `multex` by running the make command:
~~~~~~~~~~~~~~~~~~~~~~~ 
	make
~~~~~~~~~~~~~~~~~~~~~~~


# How to use MultEx
## The config file
`MultEx` reads the parameters required for a simulation from a config file. The config file has to be in json-format and
is read by `MultEx` using the json parser of `boost::property_tree`. The config file has to contain at least three objects:
* `settings`\n
  General information about the simulation (e.g. which algorithm is used and what is the name of the output directory).
* `frozen_phonon` (optional)\n
  Values needed to consider the thermal oscillations of the specimen atoms in the simulation with the frozen phonon
  approximation.
* `specimen_structure`\n
  Describes how the slices of the `slices`-object make up the specimen.
* `slices`\n
  Contains information about the different slices which make up the specimen.


### Fields of the "settings"-object 
* `path_to_transition_files` (default: "")\n
  Path that is added to the names of the transition files (i.e. the files that contain the transition potentials
  calculated with `transpot`). If no value if given, `multex` assumes that the transition files are located
  within the execution directory.
* `out_dir_name` (mandatory!)\n
  Name of the directory to which the simulation results are to be saved. If the output directory does not exist within
  the execution directory, multex will create it. If the output directory already exists, multex will create a new
  directory with a number appended to it. For example, if the output directory name is `multex_simulation` and the
  the folder `multex_simulation` is already present in the execution directory, multex will save the results of the
  simulation run in the directory `multex_simulation1`. 
* `no_output` (default: `false`)\n
  Prevent `multex` from saving any results. If this value is set to `true`, no output directory will be created and 
  no results will be saved, which can be useful for debugging purposes. 
* `algorithm` (mandatory!)\n
  Name of the multislice algorithm to be used. Valid options are `conventional`, `relativistic` and 
  `simplified_relativistic`. For details on the different algorithms see \cite MyThesis.
* `incident_beam_spin` (default: `none`)\n
  Spin of the electron beam incident on the specimen. Valid values are `up`, `down` and `none`. Since the algorithms
  `conventional` and `simplified_relativistic` do not consider the spin of the beam electron, this field has to be
  set to `none` for these algorithms. If the algorithm `relativistic` is used, this field can be either `up` or `down`,
  but not `none`. 
* `transition_wrap_around` (default: `true`)\n
  If set to `true`, transition potentials that protrude over the image edges are wrapped around, enforcing periodic
  boundary conditions. Setting this value to `false` can be helpful to avoid multiple wrap arounds of transition
  potentials with sizes more than twice of the image size.\n
  Caveat: This option does NOT influence the wrap around of the  elastic scattering potential. Owing to the periodicity
  implied in the use of the FFT, the elastic scattering potential always wraps around the image edges.
* `reduce_transition_sizes` (default: no size reduction)\n
  This option can be set to a double value \f$x\f$ to reduce the size of all transition potentials to \f$x\f$ times the
  image size (=viewing area). This can be useful for transition potentials larger than the image size that are zero in
  a significant part of the area in which they were calculated. Removing unnecessary zero-pixels 
  by transition potential size reduction increases the speed of the simulation and reduces the RAM demand of `multex`,
  without impacting the accuracy of the simulation. However, care must be taken to not remove any non-zero parts of the
  transition potentials, since this would lead to the simulation results being less accurate.
* `parallelism` (default: `false`)\n
  Turns parallel computation of the inelastic scattering from different atoms on and off. Dramatically increases the speed
  of simulation runs in which inelastic scattering is considered and multiple atoms are located within the slices making
  up the specimen, provided multiple CPU threads are available. Note that turning on parallel processing will not increase
  the speed of simulations in which only elastic scattering is considered and simulations in which the specimen slices
  contain only a single atom each. 
* `acc_voltage` (mandatory!)\n
  Acceleration voltage of the TEM in kV.
* `diffraction_space` (default: `false`)\n
  If set to `true`, `multex` will output the results in diffraction space by leaving out the last Fourier transform after
  application of the objective transfer function. Note that this field will be ignored if the objective is turned off, i.e.
  is any of the fields `aperture_angle`, `C1`, `C3` and `C5` are empty. To get the (unaltered) diffraction pattern of the
  sample, set `aperture_angle` to a high value and set `C1`, `C3` and `C5` to zero.
* `aperture_angle` (default: turn off objective)\n
  Angle of the TEM's objective aperture in mrad. If no value is provided, `multex` turns off the objective and
  outputs the electron intensity in the specimen exit plane instead of in the image plane.
* `C1`, `C3` and `C5` (default: turn off objective)\n
  Spherical aberration coefficients \f$C_1\f$ in \f$\T{\AA}\f$ and \f$C_3,~C_5\f$ in mm. If one of these values is missing,
  `multex` turns off the objective and outputs the electron intensity in the specimen exit plane instead of the image plane.
* `convergence_angle` (default: neglect illumination angle spread)\n
  Consider the incoherence of the incident electron beam by simulating several illumination angles within the convergence
  angle given here. The sampling of the illumination cone is determined by `multex` on the basis of the size of a pixel in
  reciprocal space. Note that each sampled illumination angle requires a separate multislice calculation. Consequently,
  setting a high convergence angle here can significantly increase the time required for the completion of the simulation.
  If no value is given for the convergence angle, the incident electron beam is assumed to be perfectly parallel.
* `hollow_cone_inner_angle` (default: 0mrad)\n
  Set a minimal angle for the illumination cone defined by the `convergence_angle` setting, thereby simulating hollow cone
  illumination. If `convergence_angle` is not set, this field will be completely ignored. Note that the angular sampling of the 
  illumination hollow cone is still determined by the pixel size in reciprocal space. Consequently, the difference between
  the value of the inner (`hollow_cone_inner_angle`) and outer (`convergence_angle`) angle of the hollow cone must be large
  enough to allow a reasonable amount of sampled angles to fall between the two values.
* `x_pixel` and `y_pixel` (mandatory!)\n
  Amount of image pixels in \f$x\f$- and \f$y\f$-direction. Powers of two are recommended, since this makes the
  FFTs used in `multex` run faster.

### Fields of the "frozen_phonon"-object
The `frozen_phonon` object is optional. If it is not present in the configuration file (or its mandatory fields are not filled completely), no thermal vibrations of the atoms will be considered. The fields of this object are:
* `configurations` (mandatory!)\n
  Amount of different phonon configurations to be calculated by `multex`. The concrete number of phonon configurations
  required for an accurate simulation depends on the mean thermal displacements of the atoms. There are cases 
  where a value as low as 16 can be sufficient \cite Loane1991. Because each phonon configuration requires a full 
  multislice simulation, the time requirement of the simulation increases by the amount of phonon configurations.
* `thermal_displacements` (mandatory!)\n
  List of thermal square root mean squared displacements of the different atomic species within the specimen in 
  Angstrom. For each atomic species, the list should contain two entries, one specifying the atomic number and one
  the thermal square root mean squared displacement in \f$\T{\AA}\f$. To given an example, if the specimen contains
  oxygen with a thermal square root mean squared displacement of \f$\unit[0.0963]{\text{\AA}}\f$ and titanium with
  a thermal square root mean squared displacement of \f$\unit[0.0746]{\text{\AA}}\f$, the `thermal_displacements`
  field should be `[8,0.0963,22,0.0746]`.
* `sim_id` (default: random number)\n 
  Integer value to be used as a seed for the random number generator. If this field is not present, the seed will
  be set to a random number instead. Setting this field to an integer value can be useful to perform simulations
  with the exact same frozen phonon displacements multiple times. If relativistic and conventional simulations
  are to be compared, it is important that this field is set to avoid mistaking the difference between the 
  frozen phonon configurations for a relativistic effect.
  
### Fields of the "specimen_structure"-object
* `x_dim` and `y_dim` (mandatory!)\n
  Size of the specimen in \f$x\f$- and \f$y\f$-direction in Angstrom. The specimen has to be bigger than or equal to
  the size of the unit cell given in the `slices`-object. If the unit cell is smaller than the specimen, it will
  be mirrored (in whole numbers) in both \f$x\f$- and \f$y\f$-direction. 
* `slice_sequence` (mandatory!)\n
  List that gives the sequence of slices in the specimen. The entry "0" refers to the first entry of the `slices`-
  list, the entry "1" to the second entry, and so on. To given an example, if `slice_sequence` were `[0,1]`, this would
  mean that specimen is made up of two different slices in an alternating manner (i.e. slice 0, slice 1, slice 0, slice 1, ...).
  The `slices`-list has to contain one slice for each number used in `slice_sequence` (i.e. two slices for the example).
* `repetitions` (mandatory!)\n
  Amount of times the sequence in `slice_sequence` is repeated. For example, if `slice_sequence` is `[0,1,0,2]` and `repetitions`
  is 10, the whole specimen would consist of 40 slices with the overall sequence being 0,1,0,2,0,1,0,2,0,1,0,2,...
* `append_slice_sequence` (mandatory!)\n
  List of slices that is to be appended to the specimen after repeating `slice_sequence` `repeatitions`-times. If 
  no extra slices should be appended, the field can be left empty (i.e. `"append_slices_sequence": [],`).

### The "slices"-object
The `slices` object is a list in which each entry contains the information describing a single slice of a multislice
calculation. The fields of each entry in this list are:
* `x_dim_unitcell` and `y_dim_unitcell` (mandatory!)\n
  Size of the unit cell in \f$x\f$- and \f$y\f$-direction in \f$\T{\AA}\f$. The size of the unit cell has to be smaller
  than or equal to the size of the whole specimen given in the `specimen_structure`-object. If the unit cell
  is smaller than the specimen, it will be mirrored (in whole numbers) in both \f$x\f$- and \f$y\f$-direction.
* `z_dim` (mandatory!)\n
  Thickness of the slice in Angstrom. 
* `atoms` (mandatory!)\n
  List containing the different atoms in the unit cell. The fields of each entry in the `atoms`-list are:
  * `atomic_number` (mandatory)\n
    Atomic number \f$Z\f$ of the atom. The maximum atomic number allowed is \f$Z=103\f$.
  * `occupancy` (default: 1.00)\n
    If several atoms are in the same slice and in the same \f$(x,y)\f$-position in the unit cell (i.e. directly
    behind each other in \f$z\f$-direction), this can be taken into account by setting their `occupancy` to a
    value different from 1.00, for example 2.00 for two atoms. 
  * `rel_x_pos` and `rel_y_pos` (mandatory!)\n
    \f$(x,y)\f$ -position in the unit cell relative to its size in \f$x\f$- and \f$y\f$-direction. Has to be between
    0.00 and 1.00. 
  * `transitions` (mandatory!)\n
    List containing the names of the files with the transition potentials for the transitions that are supposed
    to be considered for this atom. The transition potentials have to be calculated with the transpot program in
    a relativistic fashion for the algorithms `relativistic` and `simplified_relativistic` and in a conventional
    fashion for the algorithm `conventional`. To consider only elastic scattering from this atom, leave the list
    empty, i.e. `"transitions":[],`.


## An example
Let us assume we want to simulate an EFTEM image \f$\unit[10]{eV}\f$ above the oxygen K-edge for a \f$\text{SrTiO}_3\f$ specimen. As input, we need transition potentials for the oxygen K-edge from `transpot`. We assume that these have already been calculated (see the `transpot` manual) and reside in a directory called `transpot_results`. To perform a multislice simulation with `multex`, the following steps have to be executed:
1. Add the directory `transpot_results` to the execution directory of `multex`.
2. Create a config file for `multex`. In this example, the config file is called `config.json`.
3. Fill out the config file (details below).
4. Open a terminal and run `multex`
~~~~~~~~~~~~~~~~~~~~~~~ 
	./multex config.json
~~~~~~~~~~~~~~~~~~~~~~~ 

The config file that we used in this example, `config.json`, is shown in fig. \ref fig_config_json "1.1". Because the transition potentials in `transpot_results` were calculated in a conventional fashion, i.e. the `spin_setting` field in `transpot` was set to the value `none`, they can only be used by `multex` if the `algorithm` field is set to `conventional` and `incident_beam_spin` is set to `none`. Visualizing the transition potentials, for example with the script `visualize_transition.m' contained in the `multex_suite/miscellaneous/visualize_transition`-directory, shows that it is sufficient to consider the transition potential in a box measuring the image dimensions around the atoms, which is why the `reduce_transition_size` field is set to 1. Allowing a `convergence_angle` of \f$\unit[2.5]{mrad}\f$ means that, in this specific case, 9 multislice runs have to be performed. Additionally, 25 phonon configurations have to be calculated, since the `configurations` field in the `frozen_phonon` object is set to 25. This means that, overall, \f$9\times 25 = 225\f$ multislice runs are performed in this simulation. For testing purposes, one can remove the `convergence_angle` field and the `frozen_phonon` object from the `config.json` file to dramatically decrease the time required for the simulation. 
\anchor fig_config_json
\image html config_json.jpg "Content of the file config.json."
\image latex config_json.pdf "Content of the file config.json."
After `multex` execution, the results of the multislice simulation described by the file `config.json` should be in a directory called `example`. This directory contains a copy of the config file, a log file of the console outputs of `multex` (called `MultEx.log`), a file called `el_image.dat` with the TEM image created by exclusively elastically scattered electrons (i.e. the zero-loss image), three files containing the TEM images that result from the individual transitions and a file named `O_520eV.dat` containing the TEM image that is obtained by summing all oxygen transitions with an energy-loss of \f$\unit[520]{eV}\f$ (in this case these transitions are the three other transitions files in the directory, `O_200kV_K00_10_1-1_64x64.dat`, `O_200kV_K00_10_10_64x64.dat` and `O_200kV_K00_10_11_64x64.dat`). Consequently, the image contained in the file `O_520eV.dat`, shown in fig. \ref fig_mapA "1.2", is our final result, the simulated EFTEM image of the oxygen K-edge. 
\anchor fig_mapA
\image html map_A.jpg "EFTEM image 10eV above the oxygen K-edge."
\image latex map_A.pdf "EFTEM image 10eV above the oxygen K-edge."







