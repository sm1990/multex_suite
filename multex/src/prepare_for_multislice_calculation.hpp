#ifndef prepare_for_multislice_calculation_HPP
#define prepare_for_multislice_calculation_HPP

#include"shared_storage_container.hpp"

/**
 *@file prepare_for_multislice_calculation.hpp
 *@brief Function to prepare as much as possible before starting the multislice calculation (header)
 */

///Computes as much as possible in advance before starting the multislice calculation.
/*!Everything that can possibly be done before starting the multislice algorithm
 * is handled by this function. The function calculates the wavenumbers for
 * the elastic and the inelastic channels, the reciprocal space coordinates
 * of the image, the maximum symmetrical resolution and a corresponding
 * mask for bandwidth limiting and the propagation, transition and
 * objective transfer functions. Since the transmission functions
 * have to be recalculated for each phonon configuration (since the atoms are
 * shifted randomly), they are calculated in the multislice method
 * instead of being calculated before the algorithm starts.
 * The results of the preparation are saved in \p ssc.pd and a filled
 * \p ssc.cd field is required for this function to execute correctly.
 */
void prepare_for_multislice_calculation(Shared_storage_container& ssc);


#endif
