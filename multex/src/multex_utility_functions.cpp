#include"multex_utility_functions.hpp"

#include<cmath>

#include<armadillo>
#include<fftw3.h>

#include"multex_exceptions.hpp"
#include"multex_constants.hpp"


/**
 *@file multex_utility_functions.cpp
 *@brief Utility functions used throughout multex (source)
 */




unsigned int find_mid(unsigned int n){
	if (n%2==0){return n/2;} else {return static_cast<unsigned>((n+1)/2);}
}



unsigned int p_acc(int index, unsigned int size){
	int size_int = static_cast<int>(size);

	//move index back into array
	if (index<0){
		index+=size;
	} else if (index>=size_int){
		index-=size;
	}

	//check if new index is within array range
	if ( (index<0) || (index>=size_int) ){
		//throw std::range_error("Periodic Index loops more than once! -> Aborting\n");
		return p_acc(index,size);
	} else {
		return static_cast<unsigned int>(index);
	}
}


double electron_energy_in_eV(double k_iAng){
	return sqrt( cst::hbar2c2*pow(k_iAng,2)+ pow(cst::E_0,2) );
}


//U_kV: acceleration voltage in kV
//formula: lambda = h/sqrt(2*m_0*E*(1+E/(2*E_0))) [Reimer]
double electron_wavelength_in_angstrom(double U_kV){
	return 0.1*cst::h/sqrt(2.*cst::m_e*U_kV*0.1*cst::el*(1+0.1*(U_kV*cst::el)/(2.*cst::m_e*cst::c2)));
}



void qd_FFT(arma::cx_mat& mat, double x_dim, double y_dim, int direction ){
	unsigned int nx = static_cast<unsigned int>(mat.n_cols);
	unsigned int ny = static_cast<unsigned int>(mat.n_rows);

	//calculate norm (Fourier -> Real space or Real -> Fourier space depending on direction)
	double norm=0;
	if (direction==cst::fft_backward){
		double qx_extent = 2.*cst::pi*nx/x_dim;
		double qstepX = qx_extent/nx;
		double qy_extent = 2.*cst::pi*ny/y_dim;
		double qstepY = qy_extent/ny;
		norm=qstepX*qstepY/pow(2.*M_PI,2);
	} else if (direction==cst::fft_forward) {
		//norm for forward transform might be wrong (untested)
		double stepX = x_dim/nx;
		double stepY = y_dim/ny;
		norm=stepX*stepY;
	} else {
		throw std::logic_error("FFT-direction not recognized! -> Aborting\n");
	}


	//prepare FFT
	fftw_complex* in_out = reinterpret_cast<fftw_complex*>(mat.memptr());
	fftw_plan plan = fftw_plan_dft_2d(nx,ny,in_out,in_out,direction,cst::fftw_estimate);

	fftw_execute(plan);

	mat*=norm;

	//clean-up
	fftw_destroy_plan(plan);
}




arma::rowvec calc_fourier_coordinates(unsigned int pixel, double real_space_dim){
	#ifndef NDEBUG
	if (pixel == 0){
		throw multex_prep_exception("Zero pixel in calc_fourier_coordinates!-> Aborting");
	}
	#endif

	double rez_pixel = 2.*cst::pi/real_space_dim;
	unsigned int mid =0;
	mid = find_mid(pixel);

	arma::rowvec fourier_coor(pixel);
	//fill array according to the pattern [0,...,rez_pixel*pixel,-rez_pixel*pixel,...,-rez_pixel]
	int j=0;
	for (unsigned int i=0; i<pixel; i++){
		if (i<mid){j=i;} else {j=i-pixel;}
		fourier_coor(i) = rez_pixel*j;
	}

	return fourier_coor;
}


