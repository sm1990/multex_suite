#include"spinor.hpp"

#include<cmath>

#include"multex_exceptions.hpp"
#include"multex_constants.hpp"
#include"shared_storage_container.hpp"
#include"multex_utility_functions.hpp"
#include"transmission_function.hpp"



/**
 *@file spinor.cc
 *@brief Class that represents a spinor (source)
 */

namespace{


	///calculate the absolute squared of a Dirac spinor in chi-fromat
	/*!Calculates the absolute squared of the substituted Dirac spinor Chi (see folder). The formula is:\n
   	   /verbatim
		abs2(chi)=2*(|chi_(a,0)|^2+|chi_(a,1)|^2+|chi_(b,0)|^2|chi_(b,1)|^2
   	   /verbatim
   	   where chi_a and chi_b are the two possible solutions of the alternative Dirac equation.
   	   @param[in] chi chi wave function to be absolute squared*/
	inline arma::mat chi_abs2(arma::cx_cube chi){
		return 2.*(pow(arma::abs(chi.slice(0)),2)+pow(arma::abs(chi.slice(1)),2)+pow(arma::abs(chi.slice(2)),2)+pow(arma::abs(chi.slice(3)),2));
	}


}



Spinor::Spinor(const Shared_storage_container& ssc)
			   : algorithm(ssc.cd.algorithm), s(ssc.cd.incident_beam_spin),
				 acc_voltage(ssc.cd.acc_voltage), x_dim(ssc.cd.x_dim), y_dim(ssc.cd.y_dim){
	const unsigned int nx = ssc.cd.x_pixel;
	const unsigned int ny = ssc.cd.y_pixel;
	switch (algorithm){
		case Algorithm::relativistic:{
			spor = arma::cx_cube(ny,nx,4);
		} break;
		case Algorithm::simplified_relativistic: case Algorithm::conventional:{
			spor = arma::cx_cube(ny,nx,1);
		} break;
	}
	plan_ffts();
	set_plane_wave();
	normalize();

}

Spinor::Spinor(const Spinor& spinor_in)
			   : algorithm(spinor_in.algorithm), s(spinor_in.s),
				 acc_voltage(spinor_in.acc_voltage){
	spor = spinor_in.spor;
	x_dim = spinor_in.x_dim;
	y_dim = spinor_in.y_dim;
	plan_ffts();

}

//XXX ueberlegen ob so gut dass algorithm etc. nicht übertragen werden
Spinor& Spinor::operator=(const Spinor& spinor_in){
	spor = spinor_in.spor;
	return *this;
}



bool Spinor::operator==(const Spinor& spinor_in){
	bool spor_approx_equal = arma::approx_equal(spor,spinor_in.spor, "reldiff", 0.001);
	return ( spor_approx_equal && (algorithm == spinor_in.algorithm) &&
			 (x_dim == spinor_in.x_dim) && (y_dim == spinor_in.y_dim) );
}

bool Spinor::operator!=(const Spinor& spinor_in){
	return !operator ==(spinor_in);
}



arma::mat Spinor::abs2() const{
	switch(algorithm){
		case Algorithm::relativistic:
			return chi_abs2(spor);
		case Algorithm::simplified_relativistic:
			return pow(arma::abs(spor.slice(0)),2);
		case Algorithm::conventional:
			return pow(arma::abs(spor.slice(0)),2);
		default:
			throw multex_spinor_exception("Unkonwn algorithm in Spinor::abs2 -> Aborting!\n");
	}
}





double Spinor::average_intensity() const{
	const double nx = static_cast<double>(spor.n_cols);
	const double ny = static_cast<double>(spor.n_rows);
	return arma::accu(abs2())/(nx*ny);
}



void Spinor::set_tilted_plane_wave(double x_tilt, double y_tilt){
	set_plane_wave();
	const double wavelen = electron_wavelength_in_angstrom(acc_voltage);
	const unsigned int components = static_cast<unsigned int>(spor.n_slices);
	const unsigned int x_pixel = static_cast<unsigned int>(spor.n_cols);
	const unsigned int y_pixel = static_cast<unsigned int>(spor.n_rows);
	const double x_tilt_factor = 2.*cst::pi*x_tilt/wavelen*x_dim/x_pixel;
	const double y_tilt_factor = 2.*cst::pi*y_tilt/wavelen*y_dim/y_pixel;
	for (unsigned int is = 0; is<components; is++){
		for (unsigned int ix = 0; ix<x_pixel; ix++){
			for (unsigned int iy = 0; iy<y_pixel; iy++){
				const double expo = ix*x_tilt_factor+iy*y_tilt_factor;
				const std::complex<double> tilt_factor = std::complex<double>{cos(expo),sin(expo)};
				spor(iy,ix,is)*=tilt_factor;
			}
		}
	}
	normalize();
}



void Spinor::transmit(const Transmission_function& tmission_func){
	tmission_func.apply_to(spor);
}


void Spinor::propagate(const Propagation_function& prop_func){
	fft(cst::fft_forward);
	prop_func.apply_to(spor);
	fft(cst::fft_backward);
}

void Spinor::transition(std::pair<double,double> atom_pos, const Transition_function& tsition_func,const Spinor& elastic, const Spinor_derivative& elastic_derv){
	spor.fill(std::complex<double>(0,0));
	tsition_func.apply_to(atom_pos,elastic.spor,elastic_derv,spor);
}



void Spinor::pass_through_objective(const Objective_transfer_function& obj_trans_func, double x_tilt, double y_tilt){
	fft(cst::fft_forward);
	obj_trans_func.apply_to(spor, x_tilt, y_tilt);
	fft(cst::fft_backward);
}

void Spinor::go_to_diffraction_space(const Objective_transfer_function& obj_trans_func, double x_tilt, double y_tilt){
	fft(cst::fft_forward);
	obj_trans_func.apply_to(spor, x_tilt, y_tilt);
}





Spinor_derivative Spinor::propagate_and_derive(const Propagation_function& prop_func,
														const Pre_multislice_results& p_m_r){
	fft(cst::fft_forward);
	prop_func.apply_to(spor);

	//calculate x- and y-derivative of the wave function (if simulation is relativistic)
	const unsigned int nx = static_cast<unsigned int>(spor.n_cols);
	const unsigned int ny = static_cast<unsigned int>(spor.n_rows);
	const arma::rowvec& qx_fourier_coor = p_m_r.qx_fourier_coor;
	const arma::colvec& qy_fourier_coor = p_m_r.qy_fourier_coor;
	arma::cx_cube dx_spor;
	arma::cx_cube dy_spor;
	switch(algorithm){
		case Algorithm::relativistic: case Algorithm::simplified_relativistic:{
			dx_spor = spor;
			dy_spor = spor;
			for (unsigned int is=0; is<spor.n_slices; is++){
				dx_spor.slice(is) %= (-cst::i*arma::ones(ny,1)*qx_fourier_coor);
				qd_FFT(dx_spor.slice(is), x_dim, y_dim, cst::fft_backward);
				dy_spor.slice(is) %= (-cst::i*qy_fourier_coor*arma::ones(1,nx));
				qd_FFT(dy_spor.slice(is), x_dim, y_dim, cst::fft_backward);
			}
		} break;
		case Algorithm::conventional:{
			dx_spor = 0;
			dy_spor = 0;
		} break;
	}

	fft(cst::fft_backward);

	return Spinor_derivative(std::move(dx_spor),std::move(dy_spor));
}








void Spinor::plan_ffts(){
	unsigned int components = static_cast<unsigned int>(spor.n_slices);
	unsigned int nx = static_cast<unsigned int>(spor.n_cols);
	unsigned int ny = static_cast<unsigned int>(spor.n_rows);
	#pragma omp critical
	{
		fftw_fw_plans.resize(components);
		fftw_bw_plans.resize(components);
		for (unsigned int i=0; i<components; i++){
			fftw_complex* spor_mem = reinterpret_cast<fftw_complex*>(spor.slice(i).memptr());
			fftw_fw_plans[i] = fftw_plan_dft_2d(nx,ny,spor_mem,spor_mem,cst::fft_forward,cst::fftw_global);
			fftw_bw_plans[i] = fftw_plan_dft_2d(nx,ny,spor_mem,spor_mem,cst::fft_backward,cst::fftw_global);
		}
	}

}




void Spinor::fft(int direction){
	unsigned int nx = static_cast<unsigned int>(spor.n_cols);
	unsigned int ny = static_cast<unsigned int>(spor.n_rows);

	//calculate norm (Fourier -> Real space or Real -> Fourier space depending on direction)
	double norm=0;
	if (direction==cst::fft_backward){
		double qx_extent = 2.*cst::pi*nx/x_dim;
		double qstepX = qx_extent/nx;
		double qy_extent = 2.*cst::pi*ny/y_dim;
		double qstepY = qy_extent/ny;
		norm=qstepX*qstepY/pow(2.*M_PI,2);
	} else if (direction==cst::fft_forward) {
		//norm for forward transform might be wrong (untested)
		double stepX = x_dim/nx;
		double stepY = y_dim/ny;
		norm=stepX*stepY;
	} else {
		throw std::logic_error("FFT-direction not recognized! -> Aborting\n");
	}

	execute_fftw_plans(direction);

	//correct for missing normalization in fftw
	spor *= norm;
}



void Spinor::execute_fftw_plans(int direction){
	if (direction == cst::fft_backward){
		for (unsigned int i=0; i<fftw_bw_plans.size(); i++){
			fftw_execute(fftw_bw_plans[i]);
		}
	} else if (direction == cst::fft_forward) {
		for (unsigned int i=0; i<fftw_fw_plans.size(); i++){
			fftw_execute(fftw_fw_plans[i]);
		}
	} else {
		throw multex_spinor_exception("Unknown FFT-direction -> Aborting!");
	}
}




void Spinor::set_plane_wave(){
	const double engy = acc_voltage*pow(10,3)+cst::E_0; //electron energy in eV
	const double pc_by_sqEpmc2 = sqrt(pow(engy,2)-pow(cst::E_0,2))/(engy+cst::E_0); //p*c/sqrt(E+mc^2)

	switch (s){
		case Spin::up:{
			spor.slice(0).fill(0.5*(1.-pc_by_sqEpmc2));
			spor.slice(1).fill(0.0);
			spor.slice(2).fill(0.5*(1.+pc_by_sqEpmc2));
			spor.slice(3).fill(0.0);
		} break;
		case Spin::down:{
			spor.slice(0).fill(0.0);
			spor.slice(1).fill(0.5*(1.+pc_by_sqEpmc2));
			spor.slice(2).fill(0.0);
			spor.slice(3).fill(0.5*(1.-pc_by_sqEpmc2));
		} break;
		case Spin::none:{
			spor.slice(0).fill(1.0);
		} break;
	}



	//const double norm = 1./sqrt(average_intensity());
	//spor*=norm;
	//normalize();
}








//normalize wave so that the sum over all entries is the amount of pixels
void Spinor::normalize(){
	const double norm = 1./sqrt(average_intensity());
	spor*=norm;
}




