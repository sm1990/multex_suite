#include"multislice.hpp"

#include<string>
#include<boost/format.hpp>
#ifdef _OPENMP
	# include <omp.h>
#else
	/*!If openMP is not found, om_set_num_threads does nothing.*/
	#define omp_set_num_threads(i)
	/*!If openMP is not found, omp_get_thread_num is always 0.*/
	#define omp_get_thread_num() 0
#endif

#include"logger.hpp"
#include"multex_exceptions.hpp"
#include"multex_constants.hpp"
#include"multex_utility_functions.hpp"
#include"spinor.hpp"
#include"spinor_derivative.hpp"
#include"transmission_function.hpp"
#include"propagation_function.hpp"
#include"transition_function.hpp"
#include"objective_transfer_function.hpp"
#include"image.hpp"


/**
 *@file multislice.cpp
 *@brief Function to perform the multislice calculation (source)
 */

namespace{
	inline Spinor_derivative pass_through_slice_and_derive(unsigned int slice_number, Spinor& s,
		                                                   const Shared_storage_container& ssc){
		const std::vector<Transmission_function>& t_funcs = ssc.pd.tmission_funcs;
		const std::vector<Propagation_function>& p_funcs = ssc.pd.elastic_prop_funcs;
		const unsigned int slice_id = ssc.cd.full_slice_sequence[slice_number];


		if (ssc.cd.frozen_phonon_enabled){
			s.transmit(t_funcs[slice_number]);
		} else{
			//use transmission functions over and over if no frozen lattice is requested
			s.transmit(t_funcs[slice_id]);
		}

		return s.propagate_and_derive(p_funcs[slice_id],ssc.pd);
	}


	inline void do_multislice_to_exit_plane(unsigned int slice_number, Spinor& s, const std::string& tsition_id,
			                                const Shared_storage_container& ssc){

		const std::vector<Transmission_function>& t_funcs = ssc.pd.tmission_funcs;
		const std::vector<Propagation_function>& prop_funcs = ssc.pd.inelastic_prop_funcs.at(tsition_id);
		const unsigned int specimen_slices = static_cast<unsigned int>(ssc.cd.full_slice_sequence.size());

		//skip slice in which transition occured
		const unsigned int multislice_start = slice_number+1;

		for (unsigned int islice=multislice_start; islice<specimen_slices; islice++){
			const unsigned int slice_id = ssc.cd.full_slice_sequence[islice];
			if (ssc.cd.frozen_phonon_enabled){
				s.transmit(t_funcs[slice_number]);
			} else{
				//use transmission functions over and over if no frozen lattice is requested
				s.transmit(t_funcs[slice_id]);
			}
			s.propagate(prop_funcs[slice_id]);
		}
	}

	inline void add_image_to_map(std::unordered_map<std::string, Image>& map, const Image& image,
			                     const std::string& key){
		auto key_search_result = map.find(key);
		if (key_search_result == map.end()){
			map.emplace(std::make_pair(key, image));
		} else {
			Image& image_at_key =  key_search_result->second;
			image_at_key += image;
		}
	}


	inline void add_right_map_to_left_map(std::unordered_map<std::string,Image>& left_map,
	                                const std::unordered_map<std::string,Image>& right_map){
		for (const auto& right_map_entry : right_map){
			add_image_to_map(left_map, right_map_entry.second, right_map_entry.first);
		}
	}


	void calculate_inelastic_signals(unsigned int slice_number,
								     const Spinor& elastic, const Spinor_derivative& ds, Shared_storage_container& ssc, double x_tilt = 0, double y_tilt = 0){


		const unsigned int slice_id = ssc.cd.full_slice_sequence[slice_number];
		const Slice_data& slice = ssc.cd.slices[slice_id];
		const Pre_multislice_results& pmr = ssc.pd;

		#pragma omp parallel
		{
			std::unordered_map<std::string, Image> inel_images_local;
			Spinor inelastic_spinor(ssc);

			for (const Atom_type_data& atom_type : slice.atoms){
				for (const std::string& tsition_id : atom_type.transitions){
					const std::vector<std::pair<double,double>>& xy_pos = atom_type.xy_pos_specimen;
					#pragma omp for nowait schedule(dynamic)
					//for (std::pair<double,double> atom_pos : atom_type.xy_pos_specimen){
					for ( auto p = xy_pos.begin(); p<xy_pos.end(); p++){
						const std::pair<double, double> atom_pos = *p;
						const Transition_function& tsition = pmr.tsition_funcs.at(tsition_id);
						inelastic_spinor.transition(atom_pos,tsition,elastic,ds);
						do_multislice_to_exit_plane(slice_number,inelastic_spinor,tsition_id,ssc);
						if (ssc.cd.objective_lens_enabled){
							const Objective_transfer_function& otf = pmr.inelastic_obj_transfer_funcs.at(tsition_id);
							if (ssc.cd.diffraction_space == false){
								inelastic_spinor.pass_through_objective(otf, x_tilt, y_tilt);
							} else {
								inelastic_spinor.go_to_diffraction_space(otf, x_tilt, y_tilt);
							}
						}

						//add_image_to_map(inel_images_local, Image(inelastic_spinor), tsition_id);
						Image temp_image = Image(inelastic_spinor);
						temp_image.multiply_by(atom_type.occ);
						add_image_to_map(inel_images_local, temp_image,tsition_id);
						//add_image_to_map(inel_images_local, Image(inelastic_spinor), tsition_id);
					}
				}
			}
			#pragma omp critical
			{
				add_right_map_to_left_map(ssc.mr.inel_images, inel_images_local);
			}
		}
	}


	//----------accumulate_inelastic_signals-----------------------------------------------------------
	//computes sum of the inelastic signals with the same energy loss from the same element
	void accumulate_inelastic_signals(Shared_storage_container& ssc){

		std::unordered_map<std::string, Image>& inelastic_images = ssc.mr.inel_images;
		const std::unordered_map<std::string, Tsition_data>& tss = ssc.cd.tsitions;
		//add new entries to inelastic map
		for (const std::pair<std::string, Tsition_data>& ts : tss){
			const Tsition_data& td = ts.second;

			//write key for the entry corresponding to the transition
			std::string ts_key = cst::elemental_abbrv.at(td.atomic_number)+"_";
			const double engy_loss_ev = td.engy_loss*pow(10,3);
			ts_key+=boost::str(boost::format("%1.0feV.dat")%engy_loss_ev);

			//if entry does not exist create it, else add to existing entry
			add_image_to_map(ssc.mr.inel_images, inelastic_images.at(ts.first), ts_key);
		}
	}

	//transmission functions for exclusively elastically scattered electrons:
	void prepare_transmission_funcs(Shared_storage_container& ssc){

		const std::vector<unsigned int>& specimen_slices = ssc.cd.full_slice_sequence;
		const std::vector<Slice_data>& unique_slices = ssc.cd.slices;


		if (ssc.cd.frozen_phonon_enabled){
			ssc.pd.tmission_funcs.clear();
			ssc.pd.tmission_funcs.reserve(specimen_slices.size());
			for (const unsigned int slice_id : specimen_slices){
				const Slice_data& slice = unique_slices[slice_id];
				Transmission_function tmission_func(slice,ssc);
				ssc.pd.tmission_funcs.emplace_back(tmission_func);
			}

		} else {
			ssc.pd.tmission_funcs.reserve(unique_slices.size());
			for (const auto& slice : unique_slices){
				Transmission_function tmission_func(slice,ssc);
				ssc.pd.tmission_funcs.emplace_back(tmission_func);
			}
		}


	}


	Image single_multislice_run(Shared_storage_container& ssc, double x_tilt=0, double y_tilt=0){

		prepare_transmission_funcs(ssc);


		const std::vector<unsigned int>& slice_seq = ssc.cd.full_slice_sequence;
		const Objective_transfer_function& elastic_obj_trans_func = ssc.pd.elastic_obj_transfer_func;
		Spinor elastic_spinor(ssc);
		if ( (x_tilt != 0) || (y_tilt != 0) ){
			elastic_spinor.set_tilted_plane_wave(x_tilt,y_tilt);
		}

		Spinor_derivative elastic_derv(ssc);
		double max_intensity, min_intensity;
		for (unsigned int islice=0; islice < slice_seq.size(); islice++){
			calculate_inelastic_signals(islice,elastic_spinor,elastic_derv,ssc,x_tilt,y_tilt);

			elastic_derv = pass_through_slice_and_derive(islice,elastic_spinor,ssc);
			const double curr_intensity = elastic_spinor.average_intensity();
			if (islice == 0){
				max_intensity = curr_intensity;
				min_intensity = curr_intensity;
			} else {
				max_intensity = (curr_intensity > max_intensity) ? curr_intensity : max_intensity;
				min_intensity = (curr_intensity < min_intensity) ? curr_intensity : min_intensity;
			}

			if ( (!ssc.cd.frozen_phonon_enabled) && (!ssc.cd.illumination_angle_spread) ){
				mlog::out()<<"intensity after slice "<<islice<<" is "<<curr_intensity<<"\n";
			}
		}


		if (ssc.cd.objective_lens_enabled){
			if (ssc.cd.diffraction_space == false){
				elastic_spinor.pass_through_objective(elastic_obj_trans_func, x_tilt, y_tilt);
			} else {
				elastic_spinor.go_to_diffraction_space(elastic_obj_trans_func, x_tilt, y_tilt);
			}
		}

		mlog::out()<<"maximal intensity of multislice run: "<<max_intensity<<"\n";
		mlog::out()<<"minimal intensity of multislice run: "<<min_intensity<<"\n";


		return Image(elastic_spinor);
	}

	Image multislice_runs_with_multiple_beam_tilts(Shared_storage_container& ssc){
		const double conv_mrad_to_rad = 0.001;
		const double illum_max = ssc.cd.convergence_angle*conv_mrad_to_rad;
		const double illum_min = ssc.cd.hollow_cone_inner_angle*conv_mrad_to_rad;
		const double wavelen = electron_wavelength_in_angstrom(ssc.cd.acc_voltage);
		const double x_sample = wavelen/ssc.cd.x_dim;
		const int nx_illum = static_cast<int>(std::round(illum_max/x_sample));
		mlog::out()<<nx_illum<<" illumination angles in x-direction with a sampling of "<<1000*x_sample<<" mrad\n";
		const double y_sample = wavelen/ssc.cd.y_dim;
		const int ny_illum = static_cast<int>(std::round(illum_max/y_sample));
		mlog::out()<<ny_illum<<" illumination angles in y-direction with a sampling of "<<1000*y_sample<<" mrad\n";
		Image summed_elastic_img(ssc);

		unsigned int n_tilt = 0;
		for (int ixt = -nx_illum; ixt <= nx_illum; ixt++){
			for (int iyt = -ny_illum; iyt <= ny_illum; iyt++){
				const double x_tilt = ixt*x_sample;
				const double y_tilt = iyt*y_sample;
				const double tilt = sqrt(pow(x_tilt,2)+pow(y_tilt,2));
				if ( (tilt <= illum_max) && (tilt >= illum_min) ){
					n_tilt++;
					mlog::out()<<"-- tilt x-direction [mrad]: "<<x_tilt*1000;
					mlog::out()<<" tilt y-direction [mrad]: "<<y_tilt*1000<<" --\n";
					summed_elastic_img += single_multislice_run(ssc,x_tilt,y_tilt);
				}
			}
		}
		
		if (n_tilt == 0){
			std::string err_msg = "No sampled tilt angles within the illumination cone ";
			err_msg += "- please choose (slightly) different illumination angles.\n";
			throw multex_multislice_exception(err_msg);
		}

		//scale elastic images:
		mlog::out()<<n_tilt<<" different tilt angles were considered\n";
		const double n_tilt_double = static_cast<double>(n_tilt);
		summed_elastic_img.divide_by(n_tilt_double); 


		return summed_elastic_img;
	}


	void scale_inelastic_images(Shared_storage_container& ssc){
		double scale_factor = 1;
		if (ssc.cd.frozen_phonon_enabled){
			scale_factor *= ssc.cd.fp_data.configurations;
		}
		

		if (ssc.cd.illumination_angle_spread){
			const double conv_mrad_to_rad = 0.001;
			const double illum_max = ssc.cd.convergence_angle*conv_mrad_to_rad;
			const double illum_min = ssc.cd.hollow_cone_inner_angle*conv_mrad_to_rad;
			const double wavelen = electron_wavelength_in_angstrom(ssc.cd.acc_voltage);
			const double x_sample = wavelen/ssc.cd.x_dim;
			const int nx_illum = static_cast<int>(std::round(illum_max/x_sample));
			const double y_sample = wavelen/ssc.cd.y_dim;
			const int ny_illum = static_cast<int>(std::round(illum_max/y_sample));

			unsigned int n_tilt = 0;
			for (int ixt = -nx_illum; ixt <= nx_illum; ixt++){
				for (int iyt = -ny_illum; iyt <= ny_illum; iyt++){
					const double x_tilt = ixt*x_sample;
					const double y_tilt = iyt*y_sample;
					const double tilt = sqrt(pow(x_tilt,2)+pow(y_tilt,2));
					if ( (tilt <= illum_max) && (tilt >= illum_min) ){
						n_tilt++;
					}
				}
			}

			scale_factor *= n_tilt;
			std::cout<<"inelastic scale factor is: "<<scale_factor<<"\n";
		}

		for ([[maybe_unused]] auto&  [key, inel_image] : ssc.mr.inel_images){
			std::ignore = key; //suppress unused variable warning for gcc 7.2.0
			inel_image.divide_by(scale_factor);
		}
	}

}


void do_multislice_calculation(Shared_storage_container& ssc){

	if (ssc.cd.frozen_phonon_enabled){
		const unsigned int configus = ssc.cd.fp_data.configurations;
		mlog::out()<<"---frozen lattice configuration 1 of "<<configus<<"---\n";
		if (ssc.cd.illumination_angle_spread){
			ssc.mr.elastic_image = multislice_runs_with_multiple_beam_tilts(ssc);
		} else {
			ssc.mr.elastic_image = single_multislice_run(ssc);
		}

		for (unsigned int i=1; i<configus; i++){
			mlog::out()<<"---frozen lattice configuration "<<i+1<<" of "<<configus<<"---\n";
			if (ssc.cd.illumination_angle_spread){
				ssc.mr.elastic_image += multislice_runs_with_multiple_beam_tilts(ssc);
			} else {
				ssc.mr.elastic_image += single_multislice_run(ssc);
			}
		}

		//scale elastic  images
		ssc.mr.elastic_image.divide_by(configus);

		
	} else {
		if (ssc.cd.illumination_angle_spread){
			ssc.mr.elastic_image = multislice_runs_with_multiple_beam_tilts(ssc);
		} else {
			ssc.mr.elastic_image = single_multislice_run(ssc);
		}
	}

	scale_inelastic_images(ssc);
	accumulate_inelastic_signals(ssc);
}
