#include<iostream>
#include<fstream>
#include<unordered_map>

#include<armadillo>
#include<fftw3.h>

#include"logger.hpp"
#include"multex_exceptions.hpp"
#include"shared_storage_container.hpp"
#include"multex_io.hpp"
#include"prepare_for_multislice_calculation.hpp"
#include"multislice.hpp"

#ifdef _OPENMP
	# include <omp.h>
#else
	/*!If openMP is not found, om_set_num_threads does nothing.*/
	#define omp_set_num_threads(i)
	/*!If openMP is not found, omp_get_thread_num is always 0.*/
	#define omp_get_thread_num() 0
#endif


/**
 *@file main.cpp
 *@brief Main file
 */


///The main
int main(int argc, char* argv[]){
	std::string config_filename;
	if (argc==1){
		config_filename = "config.json";
	} else if (argc==2) {
		config_filename = std::string(argv[1]);
	} else {
		throw multex_io_exception("More than one config-file passed as command line argument!");
	}


	auto start_time =  std::chrono::system_clock::now();

	mlog::out()<<"This is MultEx version "<<VERSION<<"\n";

	//save on planing time for the Fourier Transforms by importing wisdom
	if (fftw_import_wisdom_from_filename("fftw.wis")==1){mlog::out()<<"reading fftw-wisdom...\n";}

	mlog::out()<<"reading config data from "<<config_filename<<"...\n";
	Config_data input = get_input(config_filename);
	Shared_storage_container ssc(input);

	if(ssc.cd.tpot_reduced){
		mlog::out()<<"The size of all transition potentials is reduced to "<<ssc.cd.tpot_reduction;
		mlog::out()<<" x the viewing area.\n";
	}

	if (!ssc.cd.parallelism){
		mlog::out()<<"restricting execution to a single thread...\n";
		omp_set_num_threads(1);
	}

	if (ssc.cd.frozen_phonon_enabled){
		mlog::out()<<"frozen phonon calculation is enabled...\n";
	}
	
	if (ssc.cd.diffraction_space){
		mlog::out()<<"output of results in diffraction space...\n";
	}

	mlog::out()<<"\npreparing for the multislice calculation...\n";
	prepare_for_multislice_calculation(ssc);

	mlog::out()<<"\nperforming multislice calculation...\n";
	do_multislice_calculation(ssc);

	auto end_time = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_time-start_time);
	mlog::out()<<"Execution time was "<<elapsed.count()<<" milliseconds.\n";
	mlog::out()<<"saving results...\n";
	save_results(ssc);


	//export gathered fftw wisdom
	if (fftw_export_wisdom_to_filename("fftw.wis")==1){mlog::out()<<"exporting fftw-wisdom...\n";}

	//fftw-cleanup
	fftw_cleanup();



}
