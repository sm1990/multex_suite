#ifndef multislice_HPP
#define multislice_HPP

#include"shared_storage_container.hpp"

/**
 *@file multislice.hpp
 *@brief Function to perform the multislice calculation (header)
 */


///Execute the multislice calculation
/*!Executes the multislice calculation with the data stored in
 * \p ssc. To use this function, the \p ssc.cd-field and the
 * \p ssc.pd-field have to be filled by the config-data reading
 * routine and the preparation routine, respectively.
 * The results of the calculation are saved to the multislice
 * results field of \p ssc.
 */
void do_multislice_calculation(Shared_storage_container& ssc);


#endif
