#include"image.hpp"

#include"multex_utility_functions.hpp"
#include"multex_constants.hpp"
#include"shared_storage_container.hpp"

/**
 *@file image.cc
 *@brief Class that represents a TEM image (source)
 */





Image::Image(const Spinor& spinor) : image_matrix(spinor.abs2()){}
Image::Image(const Shared_storage_container& ssc){
	const unsigned int nx = ssc.cd.x_pixel;
	const unsigned int ny = ssc.cd.y_pixel;
	image_matrix = arma::mat(ny,nx);
	image_matrix.fill(0);
}



Image& Image::operator=(const Image& rhs){
	image_matrix = rhs.image_matrix;
	return *this;
}


bool Image::operator==(const Image& rhs){
	bool approx_equal = arma::approx_equal(image_matrix,rhs.image_matrix, "reldiff", 0.001);
	return approx_equal;
}


bool Image::operator!=(const Image& rhs){
	return !operator==(rhs);
}


Image& Image::operator+=(const Image& rhs){
	image_matrix+=rhs.image_matrix;
	return *this;
}

void Image::divide_by(double divisor){
	image_matrix/= divisor;
}

void Image::multiply_by(double multiplicand){
	image_matrix*= multiplicand;
}


void Image::save(const std::string& filename) const{
	image_matrix.save(filename, arma::raw_ascii);
}

