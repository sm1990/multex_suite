#ifndef transmission_function_H
#define transmission_function_H

#include<string>
#include<armadillo>

/**
 *@file transmission_function.hpp
 *@brief Class that represents a transmission function (header)
 */


//forward-declare classes from "shared_storage_container.hpp" and then include in source file
struct Shared_storage_container;
struct Slice_data;
enum class Algorithm;



///Transmission function of a single slice
class Transmission_function{
 public:
	///Constructor
	/*!Construct a transmission function for a specific slice in the specimen. The parameters
	 * are the Slice for which the transmission function is constructed \p slice and the
	 * pixel and real space dimensions of the specimen from \p ssc.
     */
	explicit Transmission_function(const Slice_data& slice, const Shared_storage_container& ssc);
	///Apply the transmission function to a spinor matrix \p spinor_mat
	void apply_to(arma::cx_cube& spinor_mat) const;
	//void save(std::string filename) const {arma::mat out = arma::real(tmission_func.slice(0)); out.save(filename, arma::raw_ascii);}
 
 private:
	arma::cx_cube tmission_func;			/*!<In the relativistic case, each pixel corresponds to a 2x2-matrix which is stored as
												00=slice(0), 01=slice(1), 10=slice(2), 11=slice(3). In the non-relativistic case each pixel
												is just a pixel stored in 0th slice of the arma::cube*/
	const Algorithm algorithm;

};



#endif
