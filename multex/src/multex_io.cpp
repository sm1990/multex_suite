#include"multex_io.hpp"

#include<vector>
#include<iostream>
#include<unordered_map>

#include<armadillo>
#include<boost/property_tree/ptree.hpp>
#include<boost/property_tree/json_parser.hpp>
#include<boost/filesystem.hpp>
#include<boost/optional.hpp>

#include"multex_exceptions.hpp"
#include"path_switcher.hpp"
#include"logger.hpp"
#include"shared_storage_container.hpp"
#include"multex_constants.hpp"
#include"image.hpp"

/**
 *@file multex_io.cpp
 *@brief Functions for input and output (source)
 */


namespace{

	std::unordered_map<unsigned int, double> sort_thermal_displacements(std::vector<double> th_disp){
		//check whether there is a displacement for every atomic number
		if (th_disp.size()%2 != 0){
			throw multex_io_exception("Atomic number without thermal displacement -> Aborting!\n");		
		}	
		
		std::unordered_map<unsigned int, double> sorted_thermal_displacements;
		for (unsigned int i=0; i<th_disp.size(); i+=2){
			unsigned int key = static_cast<unsigned int>(th_disp[i]);
			sorted_thermal_displacements[key] = th_disp[i+1];
		}

		return sorted_thermal_displacements;
	}
	

	Config_data read_config_file(std::string config_filename){
		namespace pt = boost::property_tree;
		pt::ptree config;
		pt::read_json(config_filename.c_str(), config);
		Config_data cd;

		//settings:
		cd.config_filename = config_filename;
		cd.path_to_tsitions = config.get("settings.path_to_transition_files", "");
		cd.output_dir_name = config.get<std::string>("settings.out_dir_name");
		cd.no_output = config.get("settings.no_output", false);
		const std::string algorithm = config.get<std::string>("settings.algorithm");
		if (algorithm == "conventional"){
			cd.algorithm = Algorithm::conventional;
		} else if (algorithm == "relativistic"){
			cd.algorithm = Algorithm::relativistic;
		} else if (algorithm == "simplified_relativistic"){
			cd.algorithm = Algorithm::simplified_relativistic;
		} else {
			throw multex_io_exception("Unrecognized algorithm -> Aborting!\n");
		}
		const std::string spin = config.get("settings.incident_beam_spin", "none");
		if (spin == "up"){
			cd.incident_beam_spin = Spin::up;
		} else if (spin == "down"){
			cd.incident_beam_spin = Spin::down;
		} else if (spin == "none"){
			cd.incident_beam_spin = Spin::none;
		} else {
			throw multex_io_exception("Unrecognized incident_beam_spin -> Aborting!\n");
		}
		cd.transition_wrap_around = config.get("settings.transition_wrap_around", true);
		boost::optional<double> op_reduce_tpot = config.get_optional<double>("settings.reduce_transition_sizes");
		if (op_reduce_tpot){
			//read data for restriction of transition potential size to x times viewing area
			cd.tpot_reduced = true;
			cd.tpot_reduction = *op_reduce_tpot;
		} else {
			cd.tpot_reduced = false;
		}
		cd.parallelism = config.get("settings.parallelism", false);
		cd.acc_voltage = config.get<double>("settings.acc_voltage");
		cd.diffraction_space = config.get("settings.diffraction_space", false);


		//if data for an objective lens is specified, read it and set objective_lens_enabled to true
		boost::optional<double> op_aperture = config.get_optional<double>("settings.aperture_angle");
		boost::optional<double> op_C1 = config.get_optional<double>("settings.C1");
		boost::optional<double> op_C3 = config.get_optional<double>("settings.C3");
		boost::optional<double> op_C5 = config.get_optional<double>("settings.C5");
		if (op_aperture && op_C1 && op_C3 && op_C5){
			cd.objective_lens_enabled = true;
			cd.aperture_angle = *op_aperture*pow(10,-3); //convert from mrad to rad
			cd.C_1 = *op_C1;
			cd.C_3 = *op_C3*pow(10,7); //convert from mm to Angstrom
			cd.C_5 = *op_C5*pow(10,7); //convert from mm to Angstrom
		} else {
			cd.objective_lens_enabled = false;
		}

		//if data for illumination angle spread is specified, read it and set
		boost::optional<double> op_convergence_angle = config.get_optional<double>("settings.convergence_angle");
		if (op_convergence_angle){
			cd.illumination_angle_spread = true;
			cd.convergence_angle = *op_convergence_angle;
			cd.hollow_cone_inner_angle = config.get("settings.hollow_cone_inner_angle", 0.);
		} else {
			cd.illumination_angle_spread = false;
		}




		//if frozen phonon data is specified, read it and set frozen_phonon_enabled to true
		boost::optional<unsigned> op_configus = config.get_optional<unsigned>("frozen_phonon.configurations");
		boost::optional<unsigned> op_sim_id = config.get_optional<unsigned>("frozen_phonon.sim_id");
		if (op_configus ){
			cd.frozen_phonon_enabled = true;
			cd.fp_data.configurations = *op_configus;
			std::vector<double> raw_thermal_displacements;
			for (const auto& entries : config.get_child("frozen_phonon.thermal_displacements")){
				raw_thermal_displacements.push_back(entries.second.get_value<double>());			
			}
			cd.fp_data.thermal_displacements = sort_thermal_displacements(raw_thermal_displacements);
			if (op_sim_id){
				cd.fp_data.sim_id = static_cast<int>(*op_sim_id);
			} else {
				cd.fp_data.sim_id = cst::no_sim_id;
			}
		} else {
			cd.frozen_phonon_enabled = false;
		}


		//specimen structure:
		for (const auto& entry: config.get_child("specimen_structure.slice_sequence")){
			cd.full_slice_sequence.push_back(entry.second.get_value<unsigned int>());
		}
		//	if present read append sequence
		auto op_entries = config.get_child_optional("specimen_structure.append_slice_sequence");
		if (op_entries){
			for (const auto& entry : *op_entries){
				boost::optional<unsigned> op_append_val = entry.second.get_optional<unsigned>("");
				if (op_append_val){
					cd.append_slice_sequence.push_back(*op_append_val);
				}
			}
		} else {
			cd.append_slice_sequence.clear();
		}
		/*for (const auto& entries: config.get_child("specimen_structure.append_slice_sequence")){
			boost::optional<unsigned> op_append_val = entries.second.get_optional<unsigned>("");
			//cd.append_slice_sequence.push_back(entries.second.get_value<unsigned int>());
		}*/
		cd.repetitions = config.get<unsigned int>("specimen_structure.repetitions");
		cd.x_pixel = config.get<unsigned int>("settings.x_pixel");
		cd.y_pixel = config.get<unsigned int>("settings.y_pixel");
		cd.x_dim = config.get<double>("specimen_structure.x_dim");
		cd.y_dim = config.get<double>("specimen_structure.y_dim");
		for (auto& slices: config.get_child("slices")){
			Slice_data s_data;
			pt::ptree slice = slices.second;
			s_data.z_dim = slice.get<double>("z_dim");
			s_data.x_dim_unitcell = slice.get<double>("x_dim_unitcell");
			s_data.y_dim_unitcell = slice.get<double>("y_dim_unitcell");
			for (auto& atoms: slice.get_child("atoms")){
				Atom_type_data a_data;
				pt::ptree atom = atoms.second;
				a_data.occ = atom.get("occupancy", 1.00);
				a_data.atomic_number = atom.get<unsigned int>("atomic_number");
				a_data.rel_x_pos_unitcell = atom.get<double>("rel_x_pos");
				a_data.rel_y_pos_unitcell = atom.get<double>("rel_y_pos");
				for (auto& entries: atom.get_child("transitions")){
					a_data.transitions.push_back(entries.second.get_value<std::string>());
				}
				s_data.atoms.push_back(a_data);
			}
		cd.slices.push_back(s_data);
		}
		for (unsigned int slice_number = 0; slice_number<cd.slices.size(); slice_number++){
			cd.slices[slice_number].slice_sequence_number = slice_number;
		}


		return cd;
	}




	void repeat_slice_sequence(Config_data& cd){
		const std::vector<unsigned int> unrepeated_slice_sequence = cd.full_slice_sequence;
		unsigned int slices_per_repetition = static_cast<unsigned int>(unrepeated_slice_sequence.size());
		unsigned int slices_in_specimen = cd.repetitions*slices_per_repetition;

		cd.full_slice_sequence.resize(slices_in_specimen);
		for (unsigned int i=0; i<cd.repetitions; i++){
			for (unsigned int j=0; j<slices_per_repetition; j++){
				cd.full_slice_sequence[i*slices_per_repetition+j]=unrepeated_slice_sequence[j];
			}
		}
	}

	void append_slices_to_slice_sequence(Config_data& cd){
		std::vector<unsigned int>& s_seq = cd.full_slice_sequence;
		const std::vector<unsigned int>& append = cd.append_slice_sequence;
		if (!append.empty()){
			s_seq.reserve(s_seq.size()+append.size());
			s_seq.insert(s_seq.end(), append.begin(), append.end());
		}
	}



	std::pair<unsigned int,unsigned int> mirror_atom(Atom_type_data& atom, double x_specimen,
													  double y_specimen, double x_unitcell,
													  double y_unitcell){

		//calculate relative size of the unitcell
		double x_uc_rel = std::abs(x_unitcell/x_specimen);
		double y_uc_rel = std::abs(y_unitcell/y_specimen);

		//check unitcell size
		if ( (x_uc_rel>1) || (y_uc_rel>1) ){
			throw multex_io_exception("Unitcell larger than specimen! -> Aborting\n");
		}

		//calculate necessary mirrorings in x- and y-direction (fractional unitcells not supported!)
		unsigned int x_mirror = static_cast<unsigned int>(floor(std::abs(1./x_uc_rel)));
		unsigned int y_mirror = static_cast<unsigned int>(floor(std::abs(1./y_uc_rel)));
		mlog::out()<<"\t -> mirroring atom with Z="<<atom.atomic_number;
		mlog::out()<<" by ("<<x_mirror<<","<<y_mirror<<")\n";

		//perform mirroring by shifting the x and y values by unitcells in every direction
		std::vector<std::pair<double,double>>& xy_positions = atom.xy_pos_specimen;
		for (unsigned int y_mirr =0; y_mirr<y_mirror; y_mirr++){
			for(unsigned int x_mirr=0; x_mirr<x_mirror; x_mirr++){
				std::pair<double,double> xy_pos;
				xy_pos.first = (atom.rel_x_pos_unitcell+x_mirr)*x_unitcell;
				xy_pos.second = (atom.rel_y_pos_unitcell+y_mirr)*y_unitcell;
				xy_positions.emplace_back(xy_pos);
			}
		}

		return std::pair<unsigned int, unsigned int>(x_mirror, y_mirror);
	}



	void mirror_atom_types(Config_data& cd){
		for (Slice_data& slice : cd.slices){
			for (Atom_type_data& atom : slice.atoms){
				const double x_uc = slice.x_dim_unitcell;
				const double y_uc = slice.y_dim_unitcell;
				std::pair<unsigned int, unsigned int> mirroring;
				mirroring = mirror_atom(atom,cd.x_dim, cd.y_dim,x_uc,y_uc);

			}
		}
	}



	void read_transition_files(Config_data& config){
		for (const auto& slice: config.slices){
			for (const auto& atom: slice.atoms){
				for (const std::string& tsition_id: atom.transitions){
					//check if transition already loaded
					if (config.tsitions.find(tsition_id)==config.tsitions.end()){
						const std::string tsition_filename = config.path_to_tsitions+tsition_id;

						arma::field<arma::cx_cube> in_field;
						arma::cx_cube in;
    					std::cout.setstate(std::ios_base::failbit);
						bool file_not_read = !in_field.load(tsition_filename, arma::arma_binary);
						std::cout.clear();
						if (file_not_read){
							//allow cx_cube as input for backwards compatibility							
							if (!in.load(tsition_filename, arma::arma_binary)){
								std::string er_msg;
								er_msg = "Could not read transition-file "+tsition_id+"! -> Aborting\n";
								throw multex_io_exception(er_msg);
							}
						} else {
							arma::cx_mat temp = in_field(0).slice(0);
							temp.resize(in_field(1).n_rows, in_field(1).n_cols);	
							in = in_field(1);
							in.insert_slices(0,1);
							in.slice(0) = temp;
						}

						Tsition_data td;

						//read bonus information from first cube-slice
						td.atomic_number = static_cast<unsigned int>(in(0,0,0).real());
						td.acc_voltage = in(1,0,0).real();  //acceleration voltage in kV
						td.engy_loss = in(2,0,0).real();	//energy-loss in keV
						td.x_dim = in(3,0,0).real();		//x-dimension in Angstrom
						td.y_dim = in(4,0,0).real();		//y-dimension in Angstrom

						if ( !(in.n_slices==2) && !(in.n_slices==5)){
							std::string er_msg;
							er_msg = "Wrong number of cube slices in "+tsition_id+"! -> Aborting";
							throw multex_io_exception(er_msg);
						}

						//get Fourier transformed transition potential from other cube-slices
						td.ft_tsition_pot = in.subcube(0,0,1, in.n_rows-1, in.n_cols-1, in.n_slices-1);

						config.tsitions[tsition_id] = std::move(td);
					}

				}
			}
		}

	}



	void check_config(const Config_data& config){
		//give warning if diffraction space is wanted while objective transfer function is not set
		if ((config.diffraction_space==true) && (config.objective_lens_enabled == false)){
			mlog::out()<<"Warning: Objective lens is disabled, diffraction space=true will be ignored!\n";
		}
		

		//check whether incident_beam_spin is possible for selected multislice algorithm
		if ( (config.incident_beam_spin == Spin::none) && (config.algorithm == Algorithm::relativistic) ){
			std::string error_msg = "The relativistic multislice algorithm requires that";
			error_msg += " settings.incident_beam_spin is set to either 'up' or 'down'.\n";
			throw multex_io_exception(error_msg);
		}
		if ( (config.incident_beam_spin != Spin::none) && (config.algorithm != Algorithm::relativistic) ){
			std::string error_msg = "The selected algorithm does not consider the spin of the beam";
			error_msg += " electron. Please remove setting.incident_beam_spin from the config file or";
			error_msg += " set it to 'none'.\n";
			throw multex_io_exception(error_msg);
		}


		const std::vector<unsigned int>& f_s_sqn = config.full_slice_sequence;
		if (f_s_sqn.size() == 0) {
			throw multex_io_exception("Full_slice_sequence not filled properly!-> Aborting\n");
		}


		//check whether all slices named in slice_order are there
		size_t max_index = max_element(f_s_sqn.begin(), f_s_sqn.end())-f_s_sqn.begin();
		unsigned int max_slice = f_s_sqn[max_index];
		size_t slice_amount = config.slices.size();
		if (max_slice > slice_amount-1){
			throw multex_io_exception("Not enough slices to fill slice_order! -> Aborting\n");
		}
		
		
		//check whether hollow cone settings are correct
		if (config.illumination_angle_spread){
			if (config.hollow_cone_inner_angle>config.convergence_angle){
				throw multex_io_exception("For hollow cone, inner angle must not be larger than convergence angle.");
			}
		}



		//check whether all calculated atom positions lie within the specimen
		bool atom_out_of_bound = false;
		for (const Slice_data& slice : config.slices){
			for (const Atom_type_data& atom : slice.atoms){
				for (const std::pair<double,double> pos : atom.xy_pos_specimen){
					if ( (pos.first>config.x_dim) || (pos.first<0) ){
						atom_out_of_bound = true;
					}
					if ( (pos.second>config.y_dim) || (pos.second<0) ){
						atom_out_of_bound = true;
					}

				}
			}
		}
		if (atom_out_of_bound){
			throw multex_io_exception("Atom position outside of specimen! -> Aborting\n");
		}


		//for frozen lattice simulations, check whether displacements for all atomic numbers 
		//occurring in the specimen are available
		if (config.frozen_phonon_enabled){
			for (const Slice_data& slice : config.slices){
				for (const Atom_type_data& atom : slice.atoms){
					const std::unordered_map<unsigned int, double>& thm_disp = config.fp_data.thermal_displacements;
					if (thm_disp.find(atom.atomic_number) == thm_disp.end()){
						std::string err_msg = "Thermal displacement for atomic number ";
						err_msg += std::to_string(atom.atomic_number);
						err_msg += " missing -> Aborting!";
						throw multex_io_exception(err_msg);
					}
				}
			}
		}

		//check tsition data
		for (const Slice_data& slice: config.slices){
			for (const auto& atom: slice.atoms){

				//check whether atomic positions are filled
				if (atom.xy_pos_specimen.size()==0){
					throw multex_io_exception("Atom without positions! -> Aborting\n");
				}

				//check whether relative unit cell position is between 0 and 1
				if ( (atom.rel_x_pos_unitcell<0) || (atom.rel_x_pos_unitcell>1) ){
					throw multex_io_exception("Atom relative x-pos outside unitcell -> Aborting");
				}
				if ( (atom.rel_y_pos_unitcell<0) || (atom.rel_y_pos_unitcell>1) ){
					throw multex_io_exception("Atom relative y-pos outside unitcell -> Aborting");
				}

				for (const std::string& tsition: atom.transitions){

					const Tsition_data& td = config.tsitions.at(tsition);

					//check if atomic numbers match
					if (atom.atomic_number!=td.atomic_number){
						throw multex_io_exception("Atomic number of "+tsition+" does not match:\n "
						+std::to_string(td.atomic_number)+" vs. "+std::to_string(atom.atomic_number)+"!\n");
					}

					//check if acceleration voltage matches
					if (config.acc_voltage != td.acc_voltage){
						throw multex_io_exception("Acceleration voltage of "+tsition+" does not match:\n "
						+std::to_string(config.acc_voltage)+"kV vs. "+std::to_string(td.acc_voltage)+"kV!\n");
					}
					//check if acceleration voltage smaller than energy loss
					if (config.acc_voltage <= td.engy_loss){
						throw multex_io_exception("Acceleration voltage smaller than energy loss!\n");
					}

					//check if there are 4 potential components for a relativistic calculation (if requested)
					if ( (config.algorithm == Algorithm::relativistic) && (td.ft_tsition_pot.n_slices!=4) ){
						throw multex_io_exception("FT-transition potential in "+tsition+" does not contain four "
						+"components as required for a relativistic multislice calculation!\n");
					}

					//check if there are 4 potential components for a simplified relativistic calculation (if requested)
					if ( (config.algorithm == Algorithm::simplified_relativistic) && (td.ft_tsition_pot.n_slices!=4) ){
						throw multex_io_exception("FT-transition potential in "+tsition+" does not contain four "
						+"components as required for a simplified relativistic multislice calculation!\n");
					}

					//check if there is 1 potential component for a non-relativistic calculation (if requested)
					if ( (config.algorithm == Algorithm::conventional) && (td.ft_tsition_pot.n_slices!=1) ){
						throw multex_io_exception("FT-transition potential in "+tsition+" does not contain one "
						+"component as required for a conventional multislice calculation!\n");
					}


				}
			}
		}
	}


	///creates the output directory \p output_dir_name in \p work_path
	/*!An output_directory with the name \p output_dir_name is created
	   in the work_path \p work_path. If the directory already exists, a one, two, etc. is
	   appended to the directory name instead of a zero. If more than a hundred directories
	   exist, an exception is thrown.
	   @param[in] output_dir_name name of the created output directory (without appended zero)
	   @param[in] work_path path from which the program is run*/
	boost::filesystem::path create_output_directory(std::string output_dir_name,
														   boost::filesystem::path work_path){
		namespace bfs = boost::filesystem;

		//output directory path if this is the first simulation with the output_dir_name
		unsigned int sim_iteration=0;
		std::string no_output_dir_name = output_dir_name;
		bfs::path out_path= work_path / bfs::path(no_output_dir_name);

		//determine how many simulation result directories with the given output_dir_name are in the
		//working directory and set out_path accordingly
		while (exists(out_path)){
			sim_iteration++;
			no_output_dir_name = output_dir_name+std::to_string(sim_iteration);
			out_path = work_path / bfs::path(no_output_dir_name);
			if (sim_iteration>100){
				std::string error_msg = "More than a hundred result directories of the same name!\n"
										"Clean up your working directory! -> Aborting";
				throw multex_io_exception(error_msg);
			}
		}

		//create output directory
		if ( !bfs::create_directory(out_path) ){
			throw multex_io_exception("Could not create output directory!->Aborting");
		}

		return out_path;
	}



	std::string replace_file_extension(const std::string& filename, const std::string& new_extension){
		std::string new_filename;
		size_t lastdot = filename.find_last_of(".");
		if (lastdot == std::string::npos){
			new_filename = filename;
		} else {
			new_filename = filename.substr(0,lastdot);
		}
		new_filename += "."+new_extension;

		return new_filename;
	}


	std::string remove_path_specifiers_from_filename(const std::string& filename){
		std::string new_filename;
		size_t last_slash = filename.find_last_of("/");
		if (last_slash == std::string::npos){
			new_filename = filename;
		} else {
			new_filename = filename.substr(last_slash+1);
		}

		return new_filename;
	}



	void write_logfile(const std::string& filename){
		std::ofstream out_file(filename);
		out_file<<mlog::out().get_log();
		out_file.close();
	}

}



Config_data get_input(std::string config_filename){
	Config_data config = read_config_file(config_filename);
	repeat_slice_sequence(config);
	append_slices_to_slice_sequence(config);

	mlog::out()<<"mirroring unitcell-atoms:\n";
	mirror_atom_types(config);

	read_transition_files(config);

	check_config(config);

	return config;

}



void save_results(const Shared_storage_container& ssc){
	namespace bfs = boost::filesystem;

	bfs::path work_path = bfs::current_path();
	bfs::path out_path = create_output_directory(ssc.cd.output_dir_name, work_path);

	//switch current path to output directory
	//(switched back upon destruction of out_switcher)
	Path_switcher out_switcher(out_path);


	const Multislice_results& mr = ssc.mr;
	mr.elastic_image.save("el_image.dat");

	for (const std::pair<const std::string,Image>& inel_image: mr.inel_images){
		std::string out_file_name = replace_file_extension(inel_image.first,"dat");
		out_file_name = remove_path_specifiers_from_filename(out_file_name);
		inel_image.second.save(out_file_name);

		//uncomment for back-up of transition potential files
		//copy transition potential input file (if entry in inel_image is not internally created)
		/*if (inel_image.first.substr(lastdot,std::string::npos)!=".dat"){
			bfs::path tsition_file_wpath( work_path / bfs::path(inel_image.first) );
			bfs::path tsition_file_opath( out_path / bfs::path(inel_image.first) );
			bfs::copy_file(tsition_file_wpath,tsition_file_opath,bfs::copy_option::fail_if_exists);
		}*/
	}


	bfs::path config_file_wpath( work_path / bfs::path(ssc.cd.config_filename) );
	bfs::path config_file_opath( out_path / bfs::path(ssc.cd.config_filename) );
	bfs::copy_file(config_file_wpath,config_file_opath,bfs::copy_option::fail_if_exists);

	write_logfile("MultEx.log");


}



