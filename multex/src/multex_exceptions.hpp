#ifndef multex_exceptions_HPP
#define multex_exceptions_HPP

#include<exception>


/**
 *@file multex_exceptions.hpp
 *@brief Exceptions for multex
 */

///Exceptions in MultEx
class multex_exception : public std::exception{
public:
	///Construct with error message \p error_msg
	explicit multex_exception(const std::string& error_msg)
			 : error_message(error_msg) {}	
	///Return error message
	virtual const char* what() const throw(){
		return error_message.c_str();	
	}

private:
	const std::string error_message;

};

///Input/output-exceptions in multex
class multex_io_exception : public multex_exception{
 public:
	///Construct with error message \p error_msg
	explicit multex_io_exception(const std::string& error_msg)
			 : multex_exception(error_msg) {}
};


///Exceptions in multislice preparation
class multex_prep_exception : public multex_exception{
 public:
	///Construct with error message \p error_msg
	explicit multex_prep_exception(const std::string& error_msg)
			 : multex_exception(error_msg) {}
};


///Exceptions thrown by Spinor class
class multex_spinor_exception : public multex_exception{
 public:
	///Construct with error message \p error_msg
	explicit multex_spinor_exception(const std::string& error_msg)
			: multex_exception(error_msg){}
};

///Exceptions in multislice execution
class multex_multislice_exception : public multex_exception{
 public:
	///Construct with error message \p error_msg
	explicit multex_multislice_exception(const std::string& error_msg)
			: multex_exception(error_msg){}
};


#endif
