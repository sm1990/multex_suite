#ifndef objective_transfer_function_HPP
#define objective_transfer_function_HPP

#include<armadillo>
#include <boost/functional/hash.hpp>
#include <unordered_map>

/**
 *@file objective_transfer_function.hpp
 *@brief Class that represents the TEM objective (header)
 */

//forward-declare classes from "shared_storage_container.hpp"
struct Shared_storage_container;

///Transfer function of the objective lens and aperture
class Objective_transfer_function{
 public:
	///Constructor
	/*!Construct an objective aperture transfer function. The parameter
	 * \p wavenumber is the wave number of the electrons the objective transfer function
	 * is to be applied to.  The function requires the amount of pixel in the TEM image and the
	 * Fourier space coordinates from the storage container \p ssc.
	 */
	explicit Objective_transfer_function(double wavenumber, const Shared_storage_container& ssc);
	///Default constructor to enable construction of a vector of Objective_transfer_function
	Objective_transfer_function() = default;
	///Apply the objective transfer function to the spinor-matrix \p spinor_mat
	void apply_to(arma::cx_cube& spinor_mat, double x_tilt = 0, double y_tilt = 0) const;

 private:
	std::unordered_map<std::pair<double,double>, arma::cx_mat, boost::hash<std::pair<double, double>>> obj_trans_func;

};





#endif
