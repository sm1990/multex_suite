#ifndef spinor_derivative_HPP
#define spinor_derivative_HPP

#include<armadillo>


/**
 *@file spinor_derivative.hpp
 *@brief Class that holds the x- and y-derivatives of a spinor (header)
 */


struct Shared_storage_container;

///Holds the x- and y-derivative of a spinor
class Spinor_derivative{
 public:
	///Construct an empty object with the dimensions appropriate for the pixels and algorihm in \p ssc
	Spinor_derivative(const Shared_storage_container& ssc);

	///Construct from the x-derivative \p dx and the y-derivative \p dy
	Spinor_derivative(arma::cx_cube&& dx, arma::cx_cube&& dy);

	///Set equal to another Spinor_derivative object
	void operator=(const Spinor_derivative& spinor_derv_in);

	///Return x-derivative
	const arma::cx_cube& dx() const;

	///Return y-derivative
	const arma::cx_cube& dy() const;
 
 private:	
	arma::cx_cube dx_spinor_mat;
	arma::cx_cube dy_spinor_mat;

};





#endif
