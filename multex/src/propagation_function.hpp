#ifndef propagation_function_HPP
#define propagation_function_HPP

#include<armadillo>

/**
 *@file propagation_function.hpp
 *@brief Class that represents a propagation function of the multislice calculation (header)
 */

//forward-declare Shared_storage_container
struct Shared_storage_container;


///Class that can represent the propagation function of a relativistic or a conventional multislice calculation
class Propagation_function{
 public:
	///Constructor
	/*!Construct a propagation function from the wave number of the beam electron \p k, the slice thickness
	 * \p delta_z and bandwidth limit and the pixel and reciprocal size dimensions stored in \p ssc.
	 */
	explicit Propagation_function(double k, double delta_z, const Shared_storage_container& ssc);
	///Apply the propagation function to a spinor matrix in fourier space \p ffted_spinor_mat.
	void apply_to(arma::cx_cube& ffted_spinor_mat) const;
 
 private:
	arma::cx_mat prop_func;

};



#endif
