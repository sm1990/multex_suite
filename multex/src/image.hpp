#ifndef image_HPP
#define image_HPP

#include<armadillo>

#include"spinor.hpp"

//forward-declare classes from "shared_storage_container.hpp" and then include in source file
struct Shared_storage_container;


/**
 *@file image.hpp
 *@brief Class that represents a TEM image (header)
 */

///Represents the electron intensity in the image plane of a TEM (i.e. a TEM image).
class Image{
 public:
	///Construct from a spinor by taking the absolute squared
	Image(const Spinor& spinor);
	///Construct an empty image with pixel dimensions stored in \p ssc
	Image(const Shared_storage_container& ssc);
	///Construct an empty image with pixel dimensions of 0x0
	Image() = default;

	///Set equal to \p rhs
	Image& operator=(const Image& rhs);
	///Check for approximate equality with \p rhs
	bool operator==(const Image& rhs);
	///Check for approximate inequality with \p rhs
	bool operator!=(const Image& rhs);
	///Add image intensity of \p rhs
	Image& operator+=(const Image& rhs);

	///Divide image intensity by \p divisor
	void divide_by(double divisor);
	///Multiply image intensity by \p multiplicand
	void multiply_by(double multiplicand);

	///Save image intensity as an ASCII-file with the filename \p filename
	void save(const std::string& filename) const;

 private:
	arma::mat image_matrix;

};


///Add two images, \p lhs and \p rhs
inline Image operator+(Image lhs, const Image& rhs){
  lhs += rhs;
  return lhs;
}




#endif
