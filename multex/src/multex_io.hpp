#ifndef multex_io_HPP
#define multex_io_HPP

#include<string>

#include"shared_storage_container.hpp"

/**
 *@file multex_io.hpp
 *@brief Functions for input and output (header)
 */

///Loads input parameters from a config file
/*!Loads the input parameters from a json-format
 * config file named \p config_filename. The parameters
 * read are stored in a Config_data object, which is
 * returned by the function.
 */
Config_data get_input(std::string config_filename);


///Save results in a the output folder specified in \p ssc
/*!The simulation results, stored in the storage container
 * \p ssc, are saved to the folder which is specified under \p
 * ssc.cd.output_dir_name. If the folder is already present,
 * a folder with an appended number is created and used instead.
 * This works up to a hundred folders, at which point an exception
 * will be thrown.
 */
void save_results(const Shared_storage_container& ssc);



#endif
