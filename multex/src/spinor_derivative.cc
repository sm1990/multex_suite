#include"spinor_derivative.hpp"

#include"shared_storage_container.hpp"

/**
 *@file spinor_derivative.cc
 *@brief Class that holds the x- and y-derivatives of a spinor (source)
 */

Spinor_derivative::Spinor_derivative(const Shared_storage_container& ssc){
	const unsigned int nx = ssc.cd.x_pixel;
	const unsigned int ny = ssc.cd.y_pixel;
	unsigned int components=1;
	if (ssc.cd.algorithm == Algorithm::relativistic){
		components = 4;
	}
	dx_spinor_mat = arma::cx_cube(ny,nx,components,arma::fill::zeros);
	dy_spinor_mat = arma::cx_cube(ny,nx,components,arma::fill::zeros);
}




Spinor_derivative::Spinor_derivative(arma::cx_cube&& dx, arma::cx_cube&& dy) 
			      : dx_spinor_mat(std::move(dx)), dy_spinor_mat(std::move(dy)) {}


void Spinor_derivative::operator=(const Spinor_derivative& spinor_derv_in){
	dx_spinor_mat = spinor_derv_in.dx_spinor_mat;
	dy_spinor_mat = spinor_derv_in.dy_spinor_mat;
}


const arma::cx_cube& Spinor_derivative::dx() const{
	return dx_spinor_mat;
}


const arma::cx_cube& Spinor_derivative::dy() const{
	return dy_spinor_mat;
}
 



