#include"propagation_function.hpp"

#include<cmath>

#include"multex_constants.hpp"
#include"shared_storage_container.hpp"

/**
 *@file propagation_function.cc
 *@brief Class that represents a propagation function of the multislice calculation (source)
 */



Propagation_function::Propagation_function(double k, double delta_z, const Shared_storage_container& ssc){
	const arma::rowvec& qx_vec = ssc.pd.qx_fourier_coor;
	const arma::colvec& qy_vec = ssc.pd.qy_fourier_coor;
	const unsigned int x_pix = ssc.cd.x_pixel;
	const unsigned int y_pix = ssc.cd.y_pixel;
	const arma::cx_mat& bwl = ssc.pd.bandwidth_limit_mask;

	prop_func = bwl%exp(-cst::i*0.5*delta_z/k*(arma::ones(y_pix,1)*pow(qx_vec,2.)+pow(qy_vec,2.)*arma::ones(1,x_pix)));

}



void Propagation_function::apply_to(arma::cx_cube& ffted_spinor_mat) const{
	const unsigned int components = static_cast<unsigned int>(ffted_spinor_mat.n_slices);
	for (unsigned int i=0; i<components; i++){
		ffted_spinor_mat.slice(i)%=prop_func;
	}
}
