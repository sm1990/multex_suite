#ifndef spinor_HPP
#define spinor_HPP

#include<armadillo>
#include<fftw3.h>

#include"transmission_function.hpp"
#include"propagation_function.hpp"
#include"transition_function.hpp"
#include"spinor_derivative.hpp"
#include"objective_transfer_function.hpp"

/**
 *@file spinor.hpp
 *@brief Class that represents a spinor (header)
 */


//forward-declare classes from "shared_storage_container.hpp" and then include in source file
struct Shared_storage_container;
struct Pre_multislice_results;
enum class Algorithm;
enum class Spin;
///Represents a spinor (i.e. a Schrödinger wave function for the conventional algorithm or a four-component spinor for the relativistic algorithm)
class Spinor{
 public:
	explicit Spinor(const Shared_storage_container& ssc);			///<Construct plane wave Spinor with appropriate dimensions from \p ssc
	Spinor(const Spinor& spinor_in);								///<Copy-constructor
	Spinor& operator=(const Spinor& spinor_in);						///<Set equal to other Spinor
	bool operator==(const Spinor& spinor_in);						///<Check if approximately equal to other Spinor
	bool operator!=(const Spinor& spinor_in);						///<Check if approximately unequal to other Spinor

	arma::mat abs2() const;											///<Absolute squared of the Spinor
	double average_intensity() const;								///<Average intensity of the Spinor (as control parameter outputted during multislice)
	
	///Set spinor to tilted plane wave
	/*!Set spinor to a tilted plane wave. The parameters \p x_tilt
	 * and \p y_tilt are the (small) tilt angles in rad.
	 */
	void set_tilted_plane_wave(double x_tilt, double y_tilt);
	void transmit(const Transmission_function& tmission_func);		///<Transmit the Spinor with the transmission function \p tmission_func
	void propagate(const Propagation_function& prop_func);			///<Propagate the Spinor with the propagation function \p prop_func
	
	///Propagate and derive the Spinor
	/*!Propagate the Spinor and calculate its x- and y-derivatives
	 * from the Fourier transform that is performed during the
	 * propagation process. The x- and y-derivatives of the Spinor
	 * are required for the relativistic multislice algorithm.
	 */
	Spinor_derivative propagate_and_derive(const Propagation_function& prop_func,
													const Pre_multislice_results& p_m_r);
	///Calculate the Spinor after an inner-shell ionization
	/*!Fills the Spinor with values corresponding to the Spinor that is obtained if
	 * the elastic Spinor \p elastic causes an inner-shell ionization, mediated
	 * by the transition function \p tsition_func, of the atom at position \p atom_pos.
	 */
	void transition(std::pair<double,double> atom_pos, const Transition_function& tsition_func, const Spinor& elastic, const Spinor_derivative& elastic_derv);
	void pass_through_objective(const Objective_transfer_function& obj_trans_func, double x_tilt = 0, double y_tilt = 0);  ///<Transfer the Spinor through the objective of the TEM
	void go_to_diffraction_space(const Objective_transfer_function& obj_trans_func, double x_tilt = 0, double y_tilt = 0); ///<Fourier transform and apply objective transfer function



 
 private:
	void plan_ffts();
	void fft(int direction);
	void execute_fftw_plans(int direction);
	void set_plane_wave();
	void normalize();

	arma::cx_cube spor; //in the relativistic case, the upper two slices are the '+'-solution
	std::vector<fftw_plan> fftw_fw_plans;
	std::vector<fftw_plan> fftw_bw_plans;
	const Algorithm algorithm;
	const Spin s;
	const double acc_voltage;//in kV
	double x_dim;
	double y_dim;
};



#endif
