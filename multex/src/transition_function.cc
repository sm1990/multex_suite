#include"transition_function.hpp"

#include<math.h>

#include<armadillo>

#include"multex_exceptions.hpp"
#include"shared_storage_container.hpp"
#include"logger.hpp"
#include"multex_constants.hpp"
#include"multex_utility_functions.hpp"


#include<iostream>


/**
 *@file transition_function.cc
 *@brief Class that represents a transition function (source)
 */


namespace{
	///swaps the quadrants of \p in to counteract fftw
	/*!Since fftw requires zero-frequency to be in the top left corner of the matrix in Fourier space,
   	   the transformed matrix has its' (x,y) = (0,0) point in the top left corner. By quadrant swapping
   	   the matrix after the Fourier transform the atomic center goes from (0,0) to the matrix center,
   	   which is more convenient for later calculations and more pleasent for human viewers.
   	   @param[in] in matrix to be quadrant swapped*/
	arma::cx_mat quadrant_swap(const arma::cx_mat& in){
		unsigned int nx = static_cast<unsigned int>(in.n_cols);
		unsigned int ny = static_cast<unsigned int>(in.n_rows);

		//find  matrix center
		unsigned int x_mid = find_mid(nx);
		unsigned int y_mid = find_mid(ny);

		//prepare output matrix
		arma::cx_mat swapped(ny,nx);

		//swap quadrants
		for (unsigned int ix=0; ix<nx; ix++){
			for (unsigned int iy=0; iy<ny; iy++){
				swapped(iy,ix) = in(p_acc(iy-y_mid, ny),p_acc(ix-x_mid,nx));
			}
		}

		return swapped;
}




	///calculate a mask for bandwidth limiting a matrix
	/*!Calculates a mask for bandwidth limiting. Each entry of the output matrix is either 1+0i (for a
   	   value within the bandwidth limit) or 0+0i (for a value outside of the bandwidth limit). Another
   	   cx_mat in FOURIER space (with real space dimensions x_dim and y_dim) can be bandwidth limited by
   	   multiplying each of its' entries with the matrix returned by this function.
   	   @param[in] x_dim real space x-dimension of the matrix to be bandwidth limited
   	   @param[in] y_dim real space y-dimension of the matrix to be bandwidth limited
   	   @param[in] x_pixel number of pixels in x-direction of the matrix to be bandwidth limited
   	   @param[in] y_pixel number of pixels in y-direction of the matrix to be bandwidth limited*/
	arma::cx_mat calc_bw_mask(double x_dim, double y_dim, unsigned int x_pixel, unsigned int y_pixel){
		//calculate grid of fourier space coordinates (zero frequency at the top left corner)
		arma::rowvec qx_fourier_coor = calc_fourier_coordinates(x_pixel, x_dim);
		arma::colvec qy_fourier_coor = arma::trans(calc_fourier_coordinates(y_pixel, y_dim));

		//calculate maximal symmetrical resolution (see [1] for more details)
		double q2_max=0;
		double q2x_max = pow(x_pixel*cst::pi/x_dim,2);
		double q2y_max = pow(y_pixel*cst::pi/y_dim,2);
		if (q2x_max>q2y_max) {q2_max = q2y_max;} else {q2_max = q2x_max;}


		//calculate mask for bandwith limitation of the propagation functions
		arma::cx_mat bw_mask = arma::cx_mat(y_pixel, x_pixel, arma::fill::ones);
		double bw_limit = pow(2./3.,2)*q2_max;
		for (unsigned int ix=0; ix<x_pixel; ix++){
			for (unsigned int iy=0; iy<y_pixel; iy++){
				double q2 = pow(qx_fourier_coor(ix),2)+pow(qy_fourier_coor(iy),2);
				if (q2>bw_limit){bw_mask(iy,ix) = 0.;}
			}
		}

		return bw_mask;
	}




	///rescale an arma::cube in Fourier space
	/*!Rescales a set of matrices in an arma::cx_cube of dimensions \p x_dim, \p y_dim to have the same
   	   pixel size as the specimen described by the \p sd parameter. If the resulting matrix is very
   	   small (i.e. smaller than 8 pixel in x or y-direction) a warning is given. It is assumed that the
   	   zero frequency is in the top left corner of the matrices.
   	   @param[in] in cube containing (Fourier space) matrices
   	   @param[in] x_dim size of the input cube in x-direction
   	   @param[in] y_dim size of the input cube in y-direction
   	   @param[in] sd data describing the specimen to which \p cube is to be scaled*/
	arma::cx_cube scale_ft(arma::cx_cube in, double x_dim, double y_dim, const Config_data& c_d){
		//bandwidth limit input data
		unsigned int nx_in = static_cast<unsigned int>(in.n_cols);
		unsigned int ny_in = static_cast<unsigned int>(in.n_rows);
		unsigned int ns = static_cast<unsigned int>(in.n_slices);
		arma::cx_mat bw_mask_in = calc_bw_mask(x_dim, y_dim, nx_in, ny_in);
		for (unsigned int is=0; is<ns; is++){in.slice(is)%=bw_mask_in;}

		//determine amount of pixels in output cube
		unsigned nx_out = static_cast<unsigned>(std::round(x_dim*static_cast<double>(c_d.x_pixel)/c_d.x_dim));
		unsigned ny_out = static_cast<unsigned>(std::round(y_dim*static_cast<double>(c_d.y_pixel)/c_d.y_dim));
		arma::cx_cube out(ny_out,nx_out,ns, arma::fill::zeros);
		if ( (nx_out<8) || (ny_out<8) ){mlog::out()<<"Warning, rescaled FT might be undersampled!\n";}

		//find middle positions for the quadrants in the input and the output array
		//x_mid_a/x_mid_b are the two stopping points in the larger matrix and
		//the middle/the middle+1 for the smaller image
		unsigned int x_mid_a_in=0, x_mid_b_in=0, y_mid_a_in=0, y_mid_b_in=0;
		unsigned int x_mid_a_out=0, x_mid_b_out=0, y_mid_a_out=0, y_mid_b_out=0;
		bool no_additional_bw_necessary = true;
		if (nx_out<=nx_in){
			x_mid_a_out = find_mid(nx_out)-1;
			x_mid_b_out = find_mid(nx_out);
			x_mid_a_in = x_mid_a_out;
			x_mid_b_in = nx_in-(nx_out-x_mid_b_out);
			no_additional_bw_necessary = false;
		} else {
			x_mid_a_in = find_mid(nx_in)-1;
			x_mid_b_in = find_mid(nx_in);
			x_mid_a_out = x_mid_a_in;
			x_mid_b_out = nx_out-(nx_in-x_mid_b_in);
		}
		if (ny_out<=ny_in){
			y_mid_a_out = find_mid(ny_out)-1;
			y_mid_b_out = find_mid(ny_out);
			y_mid_a_in = y_mid_a_out;
			y_mid_b_in = ny_in-(ny_out-y_mid_b_out);
			no_additional_bw_necessary = false;
		} else {
			y_mid_a_in = find_mid(ny_in)-1;
			y_mid_b_in = find_mid(ny_in);
			y_mid_a_out = y_mid_a_in;
			y_mid_b_out = ny_out-(ny_in-y_mid_b_in);
		}


		//copy as much frequency data as possible from the input cube to
		//the output cube where zero frequency is top left corner
		out.subcube(0,0,0,y_mid_a_out,x_mid_a_out,ns-1)						//upper left quadrant
				= in.subcube(0,0,0,y_mid_a_in,x_mid_a_in,ns-1);

		out.subcube(0,x_mid_b_out,0,y_mid_a_out,nx_out-1,ns-1)				//upper right quadrant
				= in.subcube(0,x_mid_b_in,0,y_mid_a_in,nx_in-1,ns-1);

		out.subcube(y_mid_b_out,0,0,ny_out-1,x_mid_a_out,ns-1)				//lower left quadrant
				= in.subcube(y_mid_b_in,0,0,ny_in-1,x_mid_a_in,ns-1);

		out.subcube(y_mid_b_out,x_mid_b_out,0,ny_out-1,nx_out-1,ns-1)	 	//lower right quadrant
				= in.subcube(y_mid_b_in,x_mid_b_in,0,ny_in-1,nx_in-1,ns-1);

		//perform additional bandwidth limiting if necessary
		if (!no_additional_bw_necessary){
			arma::cx_mat bw_mask_out = calc_bw_mask(x_dim, y_dim, nx_out, ny_out);
			for (unsigned int is=0; is<ns; is++){out.slice(is)%=bw_mask_out;}
		}

		return out;
	}

	arma::cx_cube shrink_to_viewing_area(const arma::cx_cube& tsition_func, double rel_x_dim, double rel_y_dim){
		if ( (rel_x_dim >= 1) || (rel_y_dim >= 1) ){
			return tsition_func;
		}

		const unsigned int nx_before = static_cast<unsigned>(tsition_func.slice(0).n_cols);
		const unsigned int ny_before = static_cast<unsigned>(tsition_func.slice(0).n_rows);
		const unsigned int nx_after = static_cast<unsigned>(ceil(static_cast<double>(nx_before)*rel_x_dim));
		const unsigned int ny_after = static_cast<unsigned>(ceil(static_cast<double>(ny_before)*rel_y_dim));

		const unsigned int cut_columns = static_cast<unsigned>(floor(static_cast<double>(nx_before-nx_after)*0.5));
		const unsigned int cut_rows = static_cast<unsigned>(floor(static_cast<double>(ny_before-ny_after)*0.5));

		const size_t first_row = cut_rows;
		const size_t first_col = cut_columns;
		const size_t first_slices = 0;
		const size_t last_row = ny_before-cut_rows-1;
		const size_t last_col = nx_before-cut_columns-1;
		const size_t last_slice = tsition_func.n_slices-1;
		//std::cout<<"from: ("<<first_row<<" , "<<first_col<<") to ("<<last_row<<" , "<<last_col<<")\n";
		return tsition_func.subcube(first_row,first_col,first_slices,last_row,last_col,last_slice);
	}



}




Transition_function::Transition_function(const std::string& tsition_key, const Shared_storage_container& ssc)
										: algorithm(ssc.cd.algorithm), specimen_x_dimension(ssc.cd.x_dim),
										  specimen_y_dimension(ssc.cd.y_dim), wrap_around(ssc.cd.transition_wrap_around){
	const Tsition_data& t_d = ssc.cd.tsitions.at(tsition_key);
	const double k_w = ssc.pd.k_inelastic.at(tsition_key);

	//scale the transition potential in Fourier space to fit the specimen sampling
	//(SI-unit of sc_tpot is Angstrom^2*sqrt(eV)/A )
	//std::cout<<"nx_tpot_before: "<<t_d.ft_tsition_pot.n_cols<<" ny_tpot_before: "<<t_d.ft_tsition_pot.n_rows<<"\n";

	arma::cx_cube sc_tpot = scale_ft(t_d.ft_tsition_pot,t_d.x_dim,t_d.y_dim,ssc.cd);
	const unsigned int nx_tpot = static_cast<unsigned int>(sc_tpot.n_cols);
	const unsigned int ny_tpot = static_cast<unsigned int>(sc_tpot.n_rows);
	//std::cout<<"nx_tpot: "<<nx_tpot<<" ny_tpot: "<<ny_tpot<<"\n";

	switch(algorithm){
		case Algorithm::relativistic:{
			tsition_func = arma::cx_cube(ny_tpot, nx_tpot, 10);
			const double delta_k_w = ssc.pd.k_elastic-k_w;

			//get Fourier coordinates for the rescaled transition potential matrix
			arma::mat qx = arma::ones(ny_tpot,1)*calc_fourier_coordinates(nx_tpot,t_d.x_dim);
			arma::mat qy = arma::trans(calc_fourier_coordinates(ny_tpot,t_d.y_dim))*arma::ones(1,nx_tpot);

			//calculate projected magnetic transition field in sqrt(eV)/(Angstrom*A)
			arma::cx_cube pB_w0 = arma::cx_cube(ny_tpot, nx_tpot, 3);
			pB_w0.slice(0) = (qy%sc_tpot.slice(3)-delta_k_w*sc_tpot.slice(2))*(-cst::i);
			pB_w0.slice(1) = (delta_k_w*sc_tpot.slice(1)-qx%sc_tpot.slice(3))*(-cst::i);
			pB_w0.slice(2) = (qx%sc_tpot.slice(2)-qy%sc_tpot.slice(1))*(-cst::i);
			for (unsigned int i=0; i<3; i++){
				qd_FFT(pB_w0.slice(i), t_d.x_dim, t_d.y_dim, cst::fft_backward);
				pB_w0.slice(i) = quadrant_swap(pB_w0.slice(i));
			}



			//calculate projected electric transition field/c in sqrt(eV)/(A*Angstrom)
			arma::cx_cube pE_w0_by_c = arma::cx_cube(ny_tpot, nx_tpot, 3);
			const double delta_epsilon_w_by_hbarc  = t_d.engy_loss*pow(10,3)/cst::hbarc;
			pE_w0_by_c.slice(0) = -cst::i*delta_epsilon_w_by_hbarc*sc_tpot.slice(1)+cst::i*qx%sc_tpot.slice(0);
			pE_w0_by_c.slice(1) = -cst::i*delta_epsilon_w_by_hbarc*sc_tpot.slice(2)+cst::i*qy%sc_tpot.slice(0);
			pE_w0_by_c.slice(2) = -cst::i*delta_epsilon_w_by_hbarc*sc_tpot.slice(3)+cst::i*delta_k_w*sc_tpot.slice(0);
			for (unsigned int i=0; i<3; i++){
				qd_FFT(pE_w0_by_c.slice(i), t_d.x_dim, t_d.y_dim, cst::fft_backward);
				pE_w0_by_c.slice(i) = quadrant_swap(pE_w0_by_c.slice(i));
			}


			//calculate projected transition potential in sqrt(eV)/A
			for (unsigned int i=0; i<4; i++){
				qd_FFT(sc_tpot.slice(i), t_d.x_dim, t_d.y_dim, cst::fft_backward);
				sc_tpot.slice(i) = quadrant_swap(sc_tpot.slice(i));
			}

			/*const double hbarkw_by_mc = 0.1*cst::hbar*k_w/(cst::m_e*cst::c);
			std::cout<<"hbarkw_by_mc is: "<<hbarkw_by_mc<<"\n";
			arma::mat relative_Aw = arma::abs(sc_tpot.slice(3));
			relative_Aw.save("transB_Awz.dat", arma::raw_ascii);*/


			//calculate the transition function
			const double epsilon_0 = ssc.cd.acc_voltage*pow(10,3)+cst::E_0; //elastic electron energy in eV
			const std::complex<double> pre_V =  cst::i*cst::e_by_chbar2*epsilon_0/k_w;	// epsilon_0*e/(c*hbar^2*k_w)
			const std::complex<double> pre_Ap = cst::e_by_hbar/k_w;						// e/(hbar*k_w)
			const std::complex<double> pre_Az = cst::i*cst::e_by_hbar;					// i*e/hbar
			const std::complex<double> pre_BE = cst::i*0.5*cst::e_by_hbar/k_w;			// i*e/(2*c*hbar*k_w)
			const arma::cx_mat V_and_Az_term = pre_V*sc_tpot.slice(0)-pre_Az*sc_tpot.slice(3);
			const arma::cx_cube B_plus_iE = pre_BE*(pB_w0+cst::i*pE_w0_by_c);
			const arma::cx_cube B_minus_iE = pre_BE*(pB_w0-cst::i*pE_w0_by_c);
			tsition_func.slice(0) = V_and_Az_term - B_plus_iE.slice(2);
			tsition_func.slice(1) = -(B_plus_iE.slice(0)-cst::i*B_plus_iE.slice(1));
			tsition_func.slice(2) = -(B_plus_iE.slice(0)+cst::i*B_plus_iE.slice(1));
			tsition_func.slice(3) = V_and_Az_term + B_plus_iE.slice(2);
			tsition_func.slice(4) = V_and_Az_term - B_minus_iE.slice(2);
			tsition_func.slice(5) = -(B_minus_iE.slice(0)-cst::i*B_minus_iE.slice(1));
			tsition_func.slice(6) = -(B_minus_iE.slice(0)+cst::i*B_minus_iE.slice(1));
			tsition_func.slice(7) = V_and_Az_term + B_minus_iE.slice(2);
			tsition_func.slice(8) = -pre_Ap*sc_tpot.slice(1);
			tsition_func.slice(9) = -pre_Ap*sc_tpot.slice(2);
		} break;

		case Algorithm::simplified_relativistic:{
			tsition_func = arma::cx_cube(ny_tpot,nx_tpot,3);

			//calculate projected transition potential in  sqrt(eV)/A
			for (unsigned int i=0; i<4; i++){
				qd_FFT(sc_tpot.slice(i), t_d.x_dim, t_d.y_dim, cst::fft_backward);
				sc_tpot.slice(i) = quadrant_swap(sc_tpot.slice(i));
			}


			//calculate the transition function
			const double epsilon_0 = ssc.cd.acc_voltage*pow(10,3)+cst::E_0; //elastic electron energy in eV
			const std::complex<double> pre_V =  cst::i*cst::e_by_chbar2*epsilon_0/k_w;	// epsilon_0*e/(c*hbar^2*k_w)
			const std::complex<double> pre_Ap = cst::e_by_hbar/k_w;						// e/(hbar*k_w)
			const std::complex<double> pre_Az = cst::i*cst::e_by_hbar;					// i*e/hbar
			tsition_func.slice(0) = pre_V*sc_tpot.slice(0)-pre_Az*sc_tpot.slice(3);
			tsition_func.slice(1) = -pre_Ap*sc_tpot.slice(1);
			tsition_func.slice(2) = -pre_Ap*sc_tpot.slice(2);
		} break;

		case Algorithm::conventional:{
			tsition_func = arma::cx_cube(ny_tpot, nx_tpot, 1);
			qd_FFT(sc_tpot.slice(0), t_d.x_dim, t_d.y_dim, cst::fft_backward);
			const double epsilon_w = electron_energy_in_eV(k_w);
			const std::complex<double> prefactor = cst::i*epsilon_w*cst::e_by_chbar2/k_w;
			tsition_func.slice(0)= prefactor*quadrant_swap(sc_tpot.slice(0));
		} break;
	}

	if (ssc.cd.tpot_reduced){
		const double shrink_x = ssc.cd.tpot_reduction*specimen_x_dimension/t_d.x_dim;
		const double shrink_y = ssc.cd.tpot_reduction*specimen_y_dimension/t_d.y_dim;
		//std::cout<<"shrink_x: "<<shrink_x<<" shrink_y: "<<shrink_y<<"\n";
		tsition_func = shrink_to_viewing_area(tsition_func, shrink_x, shrink_y);
	}
}


void Transition_function::apply_to(std::pair<double,double> atom_pos, const arma::cx_cube& elastic_spor,
								   const Spinor_derivative& elastic_spor_derv, arma::cx_cube& inel_spor) const{
	const unsigned int nx_ts = static_cast<unsigned>(tsition_func.slice(0).n_cols);
	const unsigned int ny_ts = static_cast<unsigned>(tsition_func.slice(0).n_rows);
	const unsigned int nx = static_cast<unsigned int>(elastic_spor.slice(0).n_cols);
	const unsigned int ny = static_cast<unsigned int>(elastic_spor.slice(0).n_rows);
	const arma::cx_cube& dx = elastic_spor_derv.dx();
	const arma::cx_cube& dy = elastic_spor_derv.dy();


	//find pixel position of atom core
	const unsigned int x_core = static_cast<unsigned>(rint(atom_pos.first*nx/specimen_x_dimension));
	const unsigned int y_core = static_cast<unsigned>(rint(atom_pos.second*ny/specimen_y_dimension));
	//std::cout<<"placing atom at ("<<x_core<<" , "<<y_core<<")\n";

	//arma::cx_cube inel_spor = arma::cx_cube(ny,nx, elastic_spor.n_slices, arma::fill::zeros);
	//get border coorindates of the tsition-pot on the wave-matrix
	const unsigned int x_mid = find_mid(nx_ts);
	const unsigned int y_mid = find_mid(ny_ts);
	const int left = x_core-x_mid;
	const int right = left+nx_ts-1;
	const int top = y_core-y_mid;
	const int bottom = top+ny_ts-1;



	//if the transition function fits within the wave-matrix, perform calculation via subcube:
	if ((left>=0) && (top>=0) && (right < static_cast<int>(nx)) && (bottom < static_cast<int>(ny))){
		switch(algorithm){
			case Algorithm::relativistic:{
				for (int i=0; i<=6; i+=2){
						inel_spor.subcube(top,left,i/2, bottom, right, i/2) = tsition_func.subcube(0,0,i,ny_ts-1,nx_ts-1,i)%elastic_spor.subcube(top,left,2*(i/4),bottom,right,2*(i/4));
						inel_spor.subcube(top,left,i/2,bottom,right,i/2) += tsition_func.subcube(0,0,i+1,ny_ts-1,nx_ts-1,i+1)%elastic_spor.subcube(top,left,2*(i/4)+1,bottom,right,2*(i/4)+1);
						inel_spor.subcube(top,left,i/2,bottom,right,i/2) += tsition_func.subcube(0,0,8,ny_ts-1,nx_ts-1,8)%dx.subcube(top,left,i/2,bottom,right,i/2);
						inel_spor.subcube(top,left,i/2,bottom,right,i/2) += tsition_func.subcube(0,0,9,ny_ts-1,nx_ts-1,9)%dy.subcube(top,left,i/2,bottom,right,i/2);
				}
			} break;
			case Algorithm::simplified_relativistic:{
				inel_spor.subcube(top,left,0,bottom,right,0) = tsition_func.subcube(0,0,0,ny_ts-1,nx_ts-1,0)%elastic_spor.subcube(top,left,0,bottom,right,0);
				inel_spor.subcube(top,left,0,bottom,right,0) += tsition_func.subcube(0,0,1,ny_ts-1,nx_ts-1,1)%dx.subcube(top,left,0,bottom,right,0);
				inel_spor.subcube(top,left,0,bottom,right,0) +=tsition_func.subcube(0,0,2,ny_ts-1,nx_ts-1,2)%dy.subcube(top,left,0,bottom,right,0);
			} break;
			case Algorithm::conventional:{
				inel_spor.subcube(top,left,0,bottom,right,0) = elastic_spor.subcube(top,left,0,bottom,right,0);
				inel_spor.subcube(top,left,0,bottom,right,0) %= tsition_func.slice(0);
			} break;
		}
	} else {
		//if the transition function does not fit within wave-matrix, perform calculation via peroidic array access (p_acc)
		int x_ts_start = 0, x_ts_end = nx_ts;
		int y_ts_start=0, y_ts_end=ny_ts;
		//if wrap around is turned off, raster only the non-wrapped part of the transition potential
		if (!wrap_around){
			if (left<0){x_ts_start = -left;} else {x_ts_start = left;}
			if (top<0){y_ts_start = -top;} else {y_ts_start = top;}
			if (right>=static_cast<int>(nx)){x_ts_end = x_ts_start+nx;} else {x_ts_end = right;}
			if (bottom>=static_cast<int>(ny)){y_ts_end = y_ts_start+ny;} else {y_ts_end = bottom;}
		}


		switch (algorithm){
			case Algorithm::relativistic:{
				for (int ix_ts=x_ts_start; ix_ts<x_ts_end; ix_ts++){
					for (int iy_ts=y_ts_start; iy_ts<y_ts_end; iy_ts++){
						unsigned int ix = p_acc(left+ix_ts, nx);
						unsigned int iy = p_acc(top+iy_ts,	ny);
						//XXX room for optimization (at least in code-appearance)???XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx+more comments
						for (int i=0; i<=6; i+=2){
							inel_spor(iy,ix,i/2) += tsition_func(iy_ts,ix_ts,i)*elastic_spor(iy,ix,2*(i/4));
							inel_spor(iy,ix,i/2) += tsition_func(iy_ts,ix_ts,i+1)*elastic_spor(iy,ix,2*(i/4)+1);
							inel_spor(iy,ix,i/2) += tsition_func(iy_ts,ix_ts,8)*dx(iy,ix,i/2);
							inel_spor(iy,ix,i/2) += tsition_func(iy_ts,ix_ts,9)*dy(iy,ix,i/2);
						}
					}
				}
			} break;
			case Algorithm::simplified_relativistic:{
				for (int ix_ts=x_ts_start; ix_ts<x_ts_end; ix_ts++){
					for (int iy_ts=y_ts_start; iy_ts<y_ts_end; iy_ts++){
						const unsigned int ix = p_acc(left+ix_ts,nx);
						const unsigned int iy = p_acc(top+iy_ts,ny);
						inel_spor(iy,ix,0) += elastic_spor(iy,ix,0)*tsition_func(iy_ts,ix_ts,0);
						inel_spor(iy,ix,0) += dx(iy,ix,0)*tsition_func(iy_ts,ix_ts,1);
						inel_spor(iy,ix,0) += dy(iy,ix,0)*tsition_func(iy_ts,ix_ts,2);
					}
				}
			} break;
			case Algorithm::conventional:{
				for (int ix_ts=x_ts_start; ix_ts<x_ts_end; ix_ts++){
					for (int iy_ts=y_ts_start; iy_ts<y_ts_end; iy_ts++){
						const unsigned int ix = p_acc(left+ix_ts,nx);
						const unsigned int iy = p_acc(top+iy_ts,ny);
						inel_spor(iy,ix,0) += elastic_spor(iy,ix,0)*tsition_func(iy_ts,ix_ts,0);
					}
				}
			} break;
		}
	}

}





