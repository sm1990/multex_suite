#ifndef shared_storage_container_HPP
#define shared_storage_container_HPP

#include<unordered_map>
#include<vector>
#include<string>
#include<armadillo>

#include"transmission_function.hpp"
#include"propagation_function.hpp"
#include"transition_function.hpp"
#include"objective_transfer_function.hpp"
#include"image.hpp"

/**
 *@file shared_storage_container.hpp
 *@brief Struct to share input data and results throughout multex
 */


///Input paramters specific to an atom type
struct Atom_type_data{
	double occ;   											///<Occupancy of the Atom_type
	unsigned int atomic_number; 							///<Atomic number of the Atom_type
	double rel_x_pos_unitcell;								///<x-position relative to the x-dimension of the unitcell
	double rel_y_pos_unitcell;								///<y-position relative to the y-dimension of the unitcell
	std::vector<std::pair<double,double>> xy_pos_specimen; 	///<Absolute xy-positions of all atoms of this type on the specimen in \f$\T{\AA}\f$
	std::vector<std::string> transitions; 					///<Associated inelastic transitions
};

///Input parameters specific to a single slice
struct Slice_data{
	unsigned int slice_sequence_number;						///<Number used to refer to this slice
	double z_dim;											///<z-dimension of the slice in \f$\T{\AA}\f$
	double x_dim_unitcell;									///<x-dimension of the unitcell in \f$\T{\AA}\f$
	double y_dim_unitcell;									///<y-dimension of the unitcell in \f$\T{\AA}\f$
	std::vector<Atom_type_data> atoms;						///<List of Atom_types within the slice
};

///Parameters for a specific transition
/*!The parameters saved in this struct are read
 * from an input file describing the transition
 * potential of a single transition. This input file
 * can be created using the transpot program.*/
struct Tsition_data{
	unsigned int atomic_number;								///<Atomic number of the atom the transition potential was calculated for
	double acc_voltage;										///<Acceleration voltage with which the transition potential was calculated in kV
	double engy_loss;										///<Energy loss the electrons suffer during this transition in keV
	double x_dim;											///<x-dimension of the read cube in \f$\T{\AA}\f$
	double y_dim;											///<y-dimension of the read cube in \f$\T{\AA}\f$
	arma::cx_cube ft_tsition_pot;							///<Transition potential in Fourier space
															/*!<For a relativistic transition potential, \p ft_tsition_pot cube contains
															 *  four matrices, while for a conventional calculation it only contains one matrix.*/
};


///Optional parameters only necessary for frozen phonon calculations
struct Frozen_phonon_data{
	unsigned int configurations;									///<Amount of phonon configurations
	std::unordered_map<unsigned int, double> thermal_displacements; ///<Standard deviation of thermal displacement for each atomic number in \f$\T{\AA}\f$
	int sim_id;														///<Optional ID to be used as a seed for the random numbers generated
};

///Options for the multislice algorithm
enum class Algorithm {conventional, relativistic, simplified_relativistic};
///Options for the electron spin
enum class Spin {up, down, none};

///Parameters from the config file
struct Config_data{
	//settings:
	std::string config_filename;							///<Name of the read json-file
	std::string path_to_tsitions;							///<Path to the directory where the transition files are stored
	std::string output_dir_name;							///<Name of the directory for the simulation results (if empty, the working directory is used).
	bool no_output;											///<Switch to turn off output
	Algorithm algorithm;									///<Algorithm for the multislice calculation
	Spin incident_beam_spin;								///<Spin of the incident beam
	bool transition_wrap_around; 							///<Switch for turning off wrap around of the transition potential with respect to the image area (default: on)
	bool tpot_reduced;										///<Switch for restricting the size of the transition potentials to \p tpot_reduction times viewing area
	double tpot_reduction;									///<Restrict considered size of the transition potentials to \p tpot_reduction times viewing area
	bool parallelism;										///<Switch to enable parallel calculation of inelastic interactions with different atoms (default: off).
	double acc_voltage;										///<Acceleration voltage in kV
	bool diffraction_space;                                 ///<Switch for toggling diffraction space on off (only works with enabled objective lens)


	//objective lens:
	bool objective_lens_enabled;							///<Switch for turning objective lens on and off
	double aperture_angle;									///<Aperture angle in rad (mrad in config-file)
	double C_1;												///<First aberration coefficient in \f$\T{\AA}\f$
	double C_3;												///<Third aberration coefficient in \f$\T{\AA}\f$ (mm in config-file)
	double C_5;												///<Fifth aberration coefficient in \f$\T{\AA}\f$ (mm in config_file)

	//illumination angles:
	bool illumination_angle_spread;							///<Switch for turning on illumination angle spread
	double convergence_angle;								///<Maximal illumination angle in mrad
	double hollow_cone_inner_angle;                         ///<Minimal illumination angle in mrad

	//frozen phonon:
	bool frozen_phonon_enabled;								///<Switch for turning frozen phonon on and off
	Frozen_phonon_data fp_data;								///<Additional data for \p frozen_phonon calculation

	//specimen structure:
	std::vector<unsigned int> full_slice_sequence;			///<Order of the slices in the specimen (i.e. 0th slice, 1th slice, 0th slice,...)
	unsigned int repetitions;								///<Repetitions of config-file \p slice_sequence (already included in \p full_slice_sequence)
	std::vector<unsigned int> append_slice_sequence;		///<Appended to end of repeated \p slice_sequence (already included in \p full_slice_sequence)

	unsigned int x_pixel;									///<Pixel of the simulated specimen in x-direction
	unsigned int y_pixel;									///<Pixel of the simulated specimen in y-direction
	double x_dim;											///<x-dimension of the specimen in \f$\T{\AA}\f$
	double y_dim;											///<y-dimension of the specimen in \f$\T{\AA}\f$
	std::vector<Slice_data> slices;							///<List of slices forming the specimen
	std::unordered_map<std::string, Tsition_data> tsitions;	///<Parameters of the transitions mapped to their file names
};


///Results that can be obtained ahead of multislicing
struct Pre_multislice_results{
	double k_elastic;													///<Wave number of the elastically scattered electron channel in 1/\f$\T{\AA}\f$
	std::unordered_map<std::string, double> k_inelastic;				///<Wave numbers of inelastically scattered electron channels in 1/\f$\T{\AA}\f$

	double q2_max; 														///<Maximal symmetric resolving power squared in 1/\f$\T{\AA}^2\f$
	arma::cx_mat bandwidth_limit_mask;									///<Mask to impose a spatial frequency limit of \p q2_max in Fourier space
	arma::rowvec qx_fourier_coor; 										///<x-coordinates of the image-area in Fourier space in 1/\f$\T{\AA}\f$
	arma::colvec qy_fourier_coor; 										///<y-coordinates of the image-are in Fourier space in 1/\f$\T{\AA}\f$

	std::vector<Propagation_function> elastic_prop_funcs; 				///<Propagation functions of the different slices in the specimen for elastic scattering
	using inel_prop_list = std::unordered_map<std::string, std::vector<Propagation_function>>; ///<Shorten type name
	inel_prop_list inelastic_prop_funcs;								///<Propagation function for different energies and different slices

	std::vector<Transmission_function> tmission_funcs;					///<Transmission functions for each slice (elastic functions are also used for inelastic scattering)

	std::unordered_map<std::string, Transition_function> tsition_funcs;	///<Transitions occurring in the specimen

	Objective_transfer_function elastic_obj_transfer_func;				///<Transfer function of the objective lens (elastic scattering)
	using inel_obj_trans_list = std::unordered_map<std::string, Objective_transfer_function>; ///<Shorten type name
	inel_obj_trans_list inelastic_obj_transfer_funcs;					///<Objective transfer functions for different energies (inelastic scattering)
};

///Results of the multislice calculation
struct Multislice_results{
	Image elastic_image;												///<Image of exclusively elastically scattered electrons (i.e. zero-loss image)
	std::unordered_map<std::string, Image> inel_images;					///<Images of all possible inelastic transitions summed for all atoms (i.e. one image for each transition file)
};








///Storage container for the whole program
/*!Container intended as a storage place for the whole
 * program. A simulation run starts by getting the entries from
 * the config file (-> \p cd). Then, as much as possible is
 * calculated before the multislice algorithm starts (-> \p pd).
 * Finally, the results of the multislice algorithm are
 * saved (-> \p mr).*/
struct Shared_storage_container{
	///Allow construction of shared storage only with a config file
	Shared_storage_container(const Config_data& config) : cd(config){};

	///Delete copy-assignement operator (move equivalent implicitly deleted)
	Shared_storage_container & operator = (const Shared_storage_container&) = delete;

	///Delete copy operator (move equivalent implicitly deleted)
	Shared_storage_container(const Shared_storage_container&) = delete;

	Config_data cd;				///<From config file
	Pre_multislice_results pd;	///<Calculated before starting the multislice algorithm
	Multislice_results mr;		///<Results of multislice calculation
};


#endif
