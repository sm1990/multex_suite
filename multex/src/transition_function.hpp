#ifndef transition_function_HPP
#define transition_function_HPP

#include<string>
#include<armadillo>

#include"spinor_derivative.hpp"

/**
 *@file transition_function.hpp
 *@brief Class that represents a transition function (header)
 */


//forward-declare classes from "shared_storage_container.hpp"
struct Shared_storage_container;
enum class Algorithm;
///Represents a transition function
/*!Represents a transition function. Transition functions
 * are used to include inelastic scattering within the otherwise
 * completely elastic multislice algorithm.
 */
class Transition_function{
 public:
	///Construct transition function
	/*!Construct transition function from \p tsition_key, which is the name of the file the transition
	 * potential data of the transition function was stored in, and the transition data, image
	 * settings and prepared data from \p ssc.
	 */
	explicit Transition_function( const std::string& tsition_key, const Shared_storage_container& ssc);
	///Apply transition function
	/*!Apply transition function to an elastic spinor matrix \p elastic_spor with the derivative
	 * \p elastic_spor_derv for an atom at position \p atom_pos. The results are saved in the
	 * spinor matrix \p inel_spor.
	 */
	void apply_to(std::pair<double,double> atom_pos, const arma::cx_cube& elastic_spor, const Spinor_derivative& elastic_spor_derv, arma::cx_cube& inel_spor) const;
	/*void save(const std::string& filename) const{
		arma::mat bla = arma::real(tsition_func.slice(0));
		bla.save(filename, arma::raw_ascii);
	}*/
 
 private:
	arma::cx_cube tsition_func;
	const Algorithm  algorithm;
	const double specimen_x_dimension;
	const double specimen_y_dimension;
	const bool wrap_around;

};



#endif
