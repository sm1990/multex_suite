#ifndef multex_utility_functions_HPP
#define multex_utility_functions_HPP

#include<armadillo>

/**
 *@file multex_utility_functions.hpp
 *@brief Utility functions used throughout multex (header)
 */



///Find the integer middle of the number \p n
/*!If \p n is an even number, n/2 is returned. If \p n is an odd number, (n+1)/2 is returned.
 */
unsigned int find_mid(unsigned int n);


///Calculate correct access indices for a one-dimensional array with periodic boundary conditions
/*!If \p index is larger or smaller than \p size, the correctly wrapped
 * access index is returned. In all other cases, the function returns \p index.
 */
unsigned int p_acc(int index, unsigned int size);


///Calculate the electron energy from the wavenumber
/*!The energy (i.e. rest and kinetic energy) of an electron with a wave number
 * \p k_iAng (in \f$\text{\AA}^{-1}\f$) can be obtained using the formula
 * \f[
 * 	\varepsilon = E+E_0 = \sqrt{\hbar^2c^2k^2+E_0^2}~,
 * \f]
 * with the kinetic energy of the electron \f$E\f$, the rest energy of the
 * electron \f$E_0\f$ and the wave number of the electron \f$k\f$.
 */
double electron_energy_in_eV(double k_iAng);

///Calculate electron wavelength from acceleration voltage
/*!The electron wavelength in \f$\text{\AA}\f$ is calculated
 * from the acceleration voltage \p U_kV in kV. The formula
 * used is \cite Reimer2008
 * \f[
 *  \lambda = \frac{h}{\sqrt{2m_0E\left(1+\frac{E}{2E_0}\right)}}~,
 * \f]
 * with electron rest mass \f$m\f$, electron kinetic energy \f$E\f$ and
 * electron rest energy \f$E_0\f$.
 */
double electron_wavelength_in_angstrom(double U_kV);



///Quick and dirty in-place Fourier transform
/*!Calculates the in-place Fourier transform of an armadillo matrix. The Fourier transform
   convention is:
	\f{align*}{
    \FT^2[f(x,y)] &= \int \int  f(x,y) e^{iq_xx} e^{iq_yy} \T{d}x \T{d}y\\
    \FT^{-2}[f(q_x,q_y)] &= \frac{1}{(2\pi)^2} \int \int f(q_x,q_y) e^{-iq_xx} e^{-iq_yy} \T{d}q_xx \T{d}q_y .
	\f}
   The parameter \p mat is the matrix that is to be (in-place) Fourier transformed,
   the parameters \p x_dim and \p y_dim are the x- and y-real space dimensions of
   \p mat and \p direction is the direction of the Fourier transform (i.e. forward
   for \p direction = 1 and backward for direction = -1).
 */
void qd_FFT(arma::cx_mat& mat, double x_dim, double y_dim, int direction );




///Calculate the reciprocal coordinates for a given sampling.
/*!Calculates the Fourier coordinates each pixel corresponds to in a matrix with \p pixel pixels
   and a real space size of \p real_space_dim.
 */
arma::rowvec calc_fourier_coordinates(unsigned int pixel, double real_space_dim);



#endif
