#include"prepare_for_multislice_calculation.hpp"

#include<cmath>

#include<armadillo>

#include"multex_exceptions.hpp"
#include"logger.hpp"
#include"multex_constants.hpp"
#include"multex_utility_functions.hpp"
#include"transmission_function.hpp"
#include"propagation_function.hpp"

/**
 *@file prepare_for_multislice_calculation.cpp
 *@brief Function to prepare as much as possible before starting the multislice calculation (source)
 */

namespace{


	void calc_wave_numbers(Pre_multislice_results& p_m_r, const Config_data c_d){
		double lambda_elastic = electron_wavelength_in_angstrom(c_d.acc_voltage);
		mlog::out()<<"elastic channel:\n";
		mlog::out()<<"energy loss [keV] \t energy [keV] \t wavelength [Ang] \n";
		mlog::out()<<0<<" \t\t\t "<<c_d.acc_voltage<<" \t\t "<<lambda_elastic<<"\n";
		p_m_r.k_elastic = 2.*cst::pi/lambda_elastic;
		for (const std::pair<const std::string, Tsition_data>& tsition : c_d.tsitions){
			const Tsition_data& t_d = tsition.second;
			const double energy_inelastic = c_d.acc_voltage - t_d.engy_loss;
			const double lambda_inelastic = electron_wavelength_in_angstrom(energy_inelastic);
			p_m_r.k_inelastic[tsition.first] = 2.*cst::pi/lambda_inelastic;
			mlog::out()<<"transition "<<tsition.first<<":\n";
			mlog::out()<<"energy loss [keV] \t energy [keV] \t wavelength [Ang] \n";
			mlog::out()<<t_d.engy_loss<<" \t\t "<<energy_inelastic<<" \t "<<lambda_inelastic<<"\n";
		}
		mlog::out()<<"\n";
	}





	double calc_maximum_symmetrical_resolution(const Config_data& c_d){
		double q2x_max = pow(c_d.x_pixel*cst::pi/c_d.x_dim,2);
		double q2y_max = pow(c_d.y_pixel*cst::pi/c_d.y_dim,2);
		if (q2x_max>q2y_max) {
			return q2y_max;
		} else {
			return  q2x_max;
		}

	}



	///calculate a mask for bandwidth limiting a matrix
	/*!Calculates a mask for bandwidth limiting. Each entry of the output matrix is either 1+0i (for a
	   value within the bandwidth limit) or 0+0i (for a value outside of the bandwidth limit). Another
	   cx_mat in FOURIER space (with real space dimensions x_dim and y_dim) can be bandwidth limited by
	   multiplying each of its' entries with the matrix returned by this function. q2_max field of
	   ssc has to be calculated beforehand!*/
	arma::cx_mat calc_bw_mask(const Shared_storage_container& ssc){
		const unsigned int x_pixel = ssc.cd.x_pixel;
		const unsigned int y_pixel = ssc.cd.y_pixel;
		const double q2_max = ssc.pd.q2_max;
		const arma::rowvec& qx_fourier_coor = ssc.pd.qx_fourier_coor;
		const arma::colvec& qy_fourier_coor = ssc.pd.qy_fourier_coor;


		//calculate mask for bandwith limitation of the propagation functions
		arma::cx_mat bw_mask = arma::cx_mat(y_pixel, x_pixel, arma::fill::ones);
		const double bw_limit = pow(2./3.,2)*q2_max;
		for (unsigned int ix=0; ix<x_pixel; ix++){
			for (unsigned int iy=0; iy<y_pixel; iy++){
				double q2 = pow(qx_fourier_coor(ix),2)+pow(qy_fourier_coor(iy),2);
				if (q2>bw_limit){bw_mask(iy,ix) = 0.;} // else {bw_mask(iy,ix)=1.;} <- already initialized
			}
		}

		return bw_mask;
	}








	void calc_propagation_funcs(Shared_storage_container& ssc){
		//propagation functions for elastically scattered electrons:
		ssc.pd.elastic_prop_funcs.reserve(ssc.cd.slices.size());
		for (const Slice_data& slice : ssc.cd.slices){
			Propagation_function  prop_func(ssc.pd.k_elastic, slice.z_dim, ssc);
			ssc.pd.elastic_prop_funcs.emplace_back(prop_func);
		}


		//propagation functions for inelastically scattered electrons:
		for (const std::pair<const std::string, Tsition_data>& tsition : ssc.cd.tsitions){
			for (const Slice_data& slice : ssc.cd.slices){
				const std::string tsition_tag = tsition.first;
				auto& prop_funcs_with_tsition_k = ssc.pd.inelastic_prop_funcs[tsition_tag];
				prop_funcs_with_tsition_k.reserve(ssc.cd.slices.size());
				const double k_inelastic = ssc.pd.k_inelastic.at(tsition_tag);
				Propagation_function inel_prop_func(k_inelastic, slice.z_dim, ssc);
				prop_funcs_with_tsition_k.emplace_back(inel_prop_func);
			}
		}


	}


	void calc_transition_funcs(Shared_storage_container& ssc){
		for (const std::pair<const std::string, Tsition_data>& tsition : ssc.cd.tsitions){
			const std::string tsition_key = tsition.first;
			auto& tsition_funcs = ssc.pd.tsition_funcs;
			tsition_funcs.emplace(std::make_pair(tsition_key, Transition_function(tsition_key,ssc)));
		}

		//clear loaded transition matrices to save on RAM
		for ([[maybe_unused]] auto&  [key, tsition] : ssc.cd.tsitions){
			std::ignore = key; //suppress unused variable warning for gcc 7.2.0
			tsition.ft_tsition_pot.reset();
		}

	}


	void calc_objective_transfer_funcs(Shared_storage_container& ssc){

		//objective transfer functions for exclusively elastically scattered electrons:
		Objective_transfer_function exclusively_elastic(ssc.pd.k_elastic,ssc);
		ssc.pd.elastic_obj_transfer_func = std::move(exclusively_elastic);

		//objective transfer function for inelastically scattered electrons:
		for (const std::pair<const std::string, Tsition_data>& tsition : ssc.cd.tsitions){
			const double k_inelastic = ssc.pd.k_inelastic.at(tsition.first);
			Objective_transfer_function inelastic(k_inelastic,ssc);
			ssc.pd.inelastic_obj_transfer_funcs[tsition.first] = std::move(inelastic);
		}


	}






}


void prepare_for_multislice_calculation(Shared_storage_container& ssc){
	const Config_data& c_d = ssc.cd;
	Pre_multislice_results& p_m_r = ssc.pd;

	calc_wave_numbers(p_m_r,c_d);

	p_m_r.qx_fourier_coor = calc_fourier_coordinates(c_d.x_pixel, c_d.x_dim);
	p_m_r.qy_fourier_coor = arma::trans(calc_fourier_coordinates(c_d.y_pixel, c_d.y_dim));

	p_m_r.q2_max = calc_maximum_symmetrical_resolution(c_d);
	p_m_r.bandwidth_limit_mask = calc_bw_mask(ssc);

	calc_propagation_funcs(ssc);
	calc_transition_funcs(ssc);

	if (ssc.cd.objective_lens_enabled){
		calc_objective_transfer_funcs(ssc);
	}



}
