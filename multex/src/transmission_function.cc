#include"transmission_function.hpp"

#include<cmath>
#include<random>

#include<armadillo>

#include"multex_exceptions.hpp"
#include"multex_constants.hpp"
#include"multex_utility_functions.hpp"
#include"shared_storage_container.hpp"
#include"spinor_derivative.hpp"

/**
 *@file transmission_function.cc
 *@brief Class that represents a transmission function (source)
 */



namespace{




	//k_invAng: wavenumber in 1/Angstrom
	//formula: sigma = e*\varepsilon/(k*c^2*\hbar^2) [MyThesis]
	inline double sigma_in_iViAng(double k_iAng){
		return electron_energy_in_eV(k_iAng)/(cst::hbar2c2*k_iAng);
	}





	///analytical solution to the matrix exponential in the relativistic transmission function
	/*!Calculates the matrix exponential in the relativistic transmission function from the electrostatic potential
	 * V and the pojected electric field proj_ef. This can be done analytically, by first splitting of the
	 * exponential of the electrostatic potential (which is scalar) and then multiplying this with the analytic solution
	 * of the term containing the electric field. Since the terms in the resulting matrix repeat, it is sufficient
	 * to save the three terms calculated in this function. See also \cite MyThesis.
	 * @param[in] proj_V projected electrostatic potential
	 * @param[in] proj_ef projected electric field (slice(0): x-direction, slice(1): y_direction)
	 */
	arma::cx_cube matrix_exp_for_relativistic_tmission_func(const arma::cx_mat& proj_V, const arma::cx_cube& proj_ef){
		arma::cx_cube t_func(proj_V.n_rows, proj_V.n_cols, 3);
		arma::cx_mat exp_V = exp(proj_V);
		arma::cx_mat exy_abs= sqrt(pow(proj_ef.slice(0),2)+pow(proj_ef.slice(1),2));
		//arma::mat temp = arma::real(exy_abs);
		//temp.save("exy_abs.dat", arma::raw_ascii);
		t_func.slice(0) = exp_V%arma::cosh(exy_abs);
		t_func.slice(1) = exp_V%(proj_ef.slice(0)-cst::i*proj_ef.slice(1))/exy_abs%arma::sinh(exy_abs);
		t_func.slice(2) = exp_V%(proj_ef.slice(0)+cst::i*proj_ef.slice(1))/exy_abs%arma::sinh(exy_abs);
		return t_func;
	}





	//----------fe_atom--------------------------------------------------------------------------------
	///return the electron scattering factor
	/*!Returns the electron scattering factor in V*Angstrom^3 for atomic number Z
   	   at scattering angle q where 1 <= Z <= 103 and q2  = q*q with q =2pi/d = scattering angle.
   	   Function and cst::fparams taken from [2]
   	   @param[in] Z atomic number 1<= Z <= 103
   	   @param[in] q2 quadratic scattering angle q2 in 1/Angstrom^2*/
	double fe_atom(unsigned int Z, double q2){
		if ( (Z<1)	|| (Z>cst::fparams.size()) ){
			throw std::out_of_range("Atomic number out of range in fe_atom! -> Aborting");
		}

		//get index of cst::fparams corrresponding to atomic number Z
		unsigned int Z_index = Z-1;

		//switch to Fourier convention used by [1]
		q2 /= 4.*pow(cst::pi,2);
		double sum = 0.0;

		//Lorenztians:
		for (int i=0; i<6; i+=2){
			sum += cst::fparams[Z_index][i]/(q2+cst::fparams[Z_index][i+1]);
		}
		//Gaussians:
		for (int i=6; i<12; i+=2){
			sum += cst::fparams[Z_index][i]*exp(-q2*cst::fparams[Z_index][i+1]);
		}

		//add the prefactor so that the 2D-Fourier tranform of the return values will be in Volt*Angstrom;
		//this seems to be missing in atompot [2] (??)
		//	prefactor = 4*pi*a_0^2*(E_ryd/e)
		//	a_0 [Angstrom], E_ryd/e [V]
		//Further details:
		//	prefactor should correspond to the 2*pi*a_0*e-term from [2] where e is in V*Angstrom;
		//	since e [As] cannot be directly converted to [V*Angstrom] I assume that the parametrization
		//	carries the missing dimensions (i.e. epsilon_0 [A*s/V*m]) and the dimension of sum
		//	therefore must be [Angstrom*Volt*Angstrom/A*s]
		static const double prefactor = 4*cst::pi*pow(cst::a_0,2)*cst::ryd_en;

		return prefactor*sum;
	}


	arma::cx_cube calc_proj_electric_field(const arma::cx_mat& ft_pot, const Shared_storage_container& ssc){
		const unsigned int  x_pixel = ssc.cd.x_pixel;
		const unsigned int y_pixel = ssc.cd.y_pixel;
		const double x_dim = ssc.cd.x_dim;
		const double y_dim = ssc.cd.y_dim;

		const arma::rowvec& qx_fourier_coor = ssc.pd.qx_fourier_coor;
		const arma::colvec& qy_fourier_coor = ssc.pd.qy_fourier_coor;

		arma::cx_cube proj_ef(y_pixel, x_pixel, 2);
		proj_ef.slice(0) = (-cst::i*arma::ones(y_pixel,1)*qx_fourier_coor)%ft_pot;			//XXX war hier mal -i... + richtig? Fourier Konvention?? XXXX
		proj_ef.slice(1) = (-cst::i*qy_fourier_coor*arma::ones(1,x_pixel))%ft_pot;
		qd_FFT(proj_ef.slice(0), x_dim, y_dim, cst::fft_backward);
		qd_FFT(proj_ef.slice(1), x_dim, y_dim, cst::fft_backward);
		//make sure that the imaginary part of the projected electric field is zero:
		proj_ef = 0.5*(proj_ef+arma::conj(proj_ef));

		//proj_ef.slice(0).load("ex.dat");
		//proj_ef.slice(1).load("ey.dat");




		//proj_ef.fill(std::complex<double>(0,0));
		//proj_ef.fill(std::complex<double>(0,0));


		return proj_ef;
	}

	std::mt19937 initialize_random_number_generator(int seed){
		if (seed == cst::no_sim_id){
	    	std::array<int, 624> seed_data;	//fill all 624 states of the Mersenne Twister
	    	std::random_device rd;
	    	std::generate_n(seed_data.data(),seed_data.size(), std::ref(rd));
	    	std::seed_seq seed_seq(std::begin(seed_data), std::end(seed_data));
		    return std::mt19937(seed_seq);
	    } else {
	    	return std::mt19937(seed);
	    }
	}



	arma::cx_mat calc_slice_scattering_factor(const Slice_data& slice, const Shared_storage_container& ssc){

		const unsigned int x_pixel = ssc.cd.x_pixel;
		const unsigned int y_pixel = ssc.cd.y_pixel;
		const arma::rowvec& qx_fourier_coor = ssc.pd.qx_fourier_coor;
		const arma::colvec& qy_fourier_coor = ssc.pd.qy_fourier_coor;

		//calculate atomic scalar potential in Fourier space (at qz=0) for each slice in the specimen:
		arma::cx_mat ft_pot(y_pixel,x_pixel,arma::fill::zeros);
		for (const auto& atom : slice.atoms){
			//calculate scattering amplitude exp(i*(x_0*q_x+y_0_i*q_y)) for all atom positions x_0,y_0
			arma::cx_mat scamp(y_pixel, x_pixel, arma::fill::zeros);
			for (const auto& pos: atom.xy_pos_specimen){
				double x_coor = pos.first;
				double y_coor = pos.second;

				//randomly shift x- and y-coordinate  of the atoms for frozen lattice simulation
				if (ssc.cd.frozen_phonon_enabled){
					static std::mt19937 rnd_gen = initialize_random_number_generator(ssc.cd.fp_data.sim_id);
					const double std_deviation = ssc.cd.fp_data.thermal_displacements.at(atom.atomic_number);
					std::normal_distribution<double> n_dist(0,std_deviation);
					x_coor += n_dist(rnd_gen);
					y_coor += n_dist(rnd_gen);
				}

				scamp += exp(cst::i*(x_coor*arma::ones(y_pixel,1)*qx_fourier_coor
							 +y_coor*qy_fourier_coor*arma::ones(1,x_pixel)));
			}
			scamp *= atom.occ;

			//calculate Fourier transformed potential for this Atom_type and bandwidth-limit
			for (unsigned int iy=0; iy<y_pixel; iy++){
				for (unsigned int ix=0; ix<x_pixel; ix++){
					double q2 = pow(qx_fourier_coor(ix),2)+pow(qy_fourier_coor(iy),2);
					//limit resolving power to q2_max
					if (q2<=ssc.pd.q2_max){
						ft_pot(iy,ix) += scamp(iy,ix)*fe_atom(atom.atomic_number,q2);
					}  else {
						ft_pot(iy,ix) = std::complex<double>(0,0);
					}
				}
			}
		}

		return ft_pot;


	}

}





Transmission_function::Transmission_function(const Slice_data& slice,
											const Shared_storage_container& ssc) : algorithm(ssc.cd.algorithm){


	const Config_data& c_d = ssc.cd;
	const arma::cx_mat& ft_pot = calc_slice_scattering_factor(slice,ssc);

	double wavenumber = ssc.pd.k_elastic;
	const double sigma = sigma_in_iViAng(wavenumber);

	switch(algorithm){
		case Algorithm::relativistic:{
			const double kc2hbar = 2.*cst::hbarc*wavenumber;		//2*c*k*hbar/e in V
			const arma::cx_cube proj_ef = calc_proj_electric_field(ft_pot, ssc)/kc2hbar;



			tmission_func = arma::cx_cube(c_d.x_pixel, c_d.y_pixel, 3);
			arma::cx_mat proj_V = cst::i*sigma*ft_pot;
			qd_FFT(proj_V, c_d.x_dim, c_d.y_dim, cst::fft_backward);



			tmission_func = matrix_exp_for_relativistic_tmission_func(proj_V,proj_ef);
			//additional bandwidth limiting (uncomment if wanted)
			for (unsigned int is = 0; is<tmission_func.n_slices; is++){
				qd_FFT(tmission_func.slice(is), c_d.x_dim,c_d.y_dim,cst::fft_forward);
				tmission_func.slice(is)%=ssc.pd.bandwidth_limit_mask;
				qd_FFT(tmission_func.slice(is), c_d.x_dim,c_d.y_dim, cst::fft_backward);
			}

		} break;
		case Algorithm::simplified_relativistic: case Algorithm::conventional:{
			tmission_func = arma::cx_cube(c_d.x_pixel, c_d.y_pixel, 1);

			tmission_func.slice(0) = cst::i*sigma*ft_pot;
			qd_FFT(tmission_func.slice(0), c_d.x_dim, c_d.y_dim, cst::fft_backward);

			tmission_func.slice(0) = exp(tmission_func.slice(0));
			//additional bandwidth limiting (uncomment if wanted)
			qd_FFT(tmission_func.slice(0), c_d.x_dim,c_d.y_dim,cst::fft_forward);
			tmission_func.slice(0)%=ssc.pd.bandwidth_limit_mask;
			qd_FFT(tmission_func.slice(0), c_d.x_dim,c_d.y_dim, cst::fft_backward);
		} break;
	}



}




void Transmission_function::apply_to(arma::cx_cube& spinor_mat) const{
	switch(algorithm){
		case Algorithm::relativistic:{

			arma::cx_mat temp(spinor_mat.n_rows,spinor_mat.n_cols);
			//'chi_+'-part of the spinor matrix
			temp = spinor_mat.slice(0);
			spinor_mat.slice(0) = tmission_func.slice(0)%spinor_mat.slice(0);
			spinor_mat.slice(0) += tmission_func.slice(1)%spinor_mat.slice(1);
			spinor_mat.slice(1) = tmission_func.slice(0)%spinor_mat.slice(1);
			spinor_mat.slice(1) += tmission_func.slice(2)%temp;

			//'chi_-'-part of the spinor matrix
			temp = spinor_mat.slice(2);
			spinor_mat.slice(2) = tmission_func.slice(0)%spinor_mat.slice(2);
			spinor_mat.slice(2) -= tmission_func.slice(1)%spinor_mat.slice(3);
			spinor_mat.slice(3) = tmission_func.slice(0)%spinor_mat.slice(3);
			spinor_mat.slice(3) -= tmission_func.slice(2)%temp;

		} break;
		case Algorithm::simplified_relativistic: case Algorithm::conventional:{
			spinor_mat.slice(0) %= tmission_func.slice(0);
		} break;
	}

}



