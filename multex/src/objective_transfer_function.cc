#include"objective_transfer_function.hpp"

#include<iostream>

#include"multex_exceptions.hpp"
#include"logger.hpp"
#include"multex_constants.hpp"
#include"shared_storage_container.hpp"
#include"multex_utility_functions.hpp"


/**
 *@file objective_transfer_function.cc
 *@brief Class that represents the TEM objective (source)
 */


namespace{
	std::vector<std::pair<double,double>> calculate_tilt_angles(const Shared_storage_container& ssc){
		const double conv_mrad_to_rad = 0.001;
		const double illum_max = ssc.cd.convergence_angle*conv_mrad_to_rad;
		const double illum_min = ssc.cd.hollow_cone_inner_angle*conv_mrad_to_rad;
		const double wavelen = electron_wavelength_in_angstrom(ssc.cd.acc_voltage);
		const double x_sample = wavelen/ssc.cd.x_dim;
		const int nx_illum = static_cast<int>(std::round(illum_max/x_sample));
		const double y_sample = wavelen/ssc.cd.y_dim;
		const int ny_illum = static_cast<int>(std::round(illum_max/y_sample));
		std::vector<std::pair<double,double>> tilt_angles;

		for (int ixt = -nx_illum; ixt <= nx_illum; ixt++){
			for (int iyt = -ny_illum; iyt <= ny_illum; iyt++){
				const double x_tilt = ixt*x_sample;
				const double y_tilt = iyt*y_sample;
				const double tilt = sqrt(pow(x_tilt,2)+pow(y_tilt,2));
				if ( (tilt <= illum_max) && (tilt >= illum_min) ){
					tilt_angles.push_back(std::make_pair(x_tilt,y_tilt));
				}
			}
		}
		return tilt_angles;
	}	


	arma::mat calculate_aperture_function(double wavenumber, const arma::mat& q2,
			                              const Shared_storage_container& ssc){
		const unsigned int x_pixel = static_cast<unsigned int>(q2.n_cols);
		const unsigned int y_pixel = static_cast<unsigned int>(q2.n_rows);
		const double ap_angle_in_rad = ssc.cd.aperture_angle;

		const double frq_aperture_angle2 = pow(wavenumber*ap_angle_in_rad,2);
		arma::mat aperture_func(arma::size(q2));
		unsigned int beams_through_aperture = 0;
		for (unsigned int ix=0; ix<x_pixel; ix++){
			for (unsigned int iy=0; iy<y_pixel; iy++){
				if (q2(iy,ix)<= frq_aperture_angle2){
					aperture_func(iy,ix) = 1;
					beams_through_aperture++;
				} else {
					aperture_func(iy,ix) = 0;
				}
			}
		}

		if (beams_through_aperture<10){
			std::string msg = "Aperture might be to small (";
			msg += std::to_string(beams_through_aperture)+" beams)!\n";
			mlog::out()<<msg;
		}
		

		return aperture_func;

	}

	arma::mat calc_aber_func(double k, const arma::mat& q2, const Shared_storage_container& ssc){
		const double C_1 = ssc.cd.C_1;
		const double C_3 = ssc.cd.C_3;
		const double C_5 = ssc.cd.C_5;

		return 0.5*q2/k%(C_1+0.5*q2/pow(k,2)*C_3+1./3.*C_5*pow(q2,2)/pow(k,4));

	}

}





Objective_transfer_function::Objective_transfer_function(double wavenumber,
														 const Shared_storage_container& ssc){
	const unsigned int x_pixel = ssc.cd.x_pixel;
	const unsigned int y_pixel = ssc.cd.y_pixel;
	const arma::rowvec& qx_fourier_coor = ssc.pd.qx_fourier_coor;
	const arma::colvec& qy_fourier_coor = ssc.pd.qy_fourier_coor;

	arma::mat fourier_coor2 = pow(arma::ones(y_pixel,1)*qx_fourier_coor,2);
	fourier_coor2 += pow(qy_fourier_coor*arma::ones(1,x_pixel),2);

	arma::mat aperture_func;
	aperture_func = calculate_aperture_function(wavenumber,fourier_coor2,ssc);
	arma::mat aberration_func = calc_aber_func(wavenumber,fourier_coor2,ssc);	

	if (ssc.cd.hollow_cone_inner_angle != 0){
		std::cout<<"Spherical aberration for hollow cone probably not correct!\n";
		auto tilt_angles = calculate_tilt_angles(ssc);
		for (std::pair<double,double> tilt_angle : tilt_angles){
			const double x_tilt_freq = tilt_angle.first*wavenumber;
			const double y_tilt_freq = tilt_angle.second*wavenumber;
			fourier_coor2 = pow(arma::ones(y_pixel,1)*(qx_fourier_coor+x_tilt_freq),2);
			fourier_coor2 += pow((qy_fourier_coor+y_tilt_freq)*arma::ones(1,x_pixel),2);
			obj_trans_func[tilt_angle] = calculate_aperture_function(wavenumber,fourier_coor2,ssc)%exp(-cst::i*calc_aber_func(wavenumber,fourier_coor2,ssc)); //might not be correct for aberration function
		}

	}
	obj_trans_func[std::make_pair(0,0)] = aperture_func%exp(-cst::i*aberration_func);
}


void Objective_transfer_function::apply_to(arma::cx_cube& spinor_mat, double x_tilt, double y_tilt) const{
	const unsigned int components = static_cast<unsigned int>(spinor_mat.n_slices);
	if (obj_trans_func.size() == 1){
		for (unsigned int i=0; i<components; i++){
			spinor_mat.slice(i)%=obj_trans_func.at(std::make_pair(0,0));
		}
	} else {
		/*static int N = 0;
		N+= 1;
		if (N == 5){
			std::cout<<"outputting for x_tilt: "<<x_tilt<<" y_tilt: "<<y_tilt<<"\n\n\n!!\n";
			arma::mat out = arma::abs(obj_trans_func.at(std::make_pair(x_tilt,y_tilt)));
			out.save("transfunc.dat", arma::raw_ascii);
		}*/
		for (unsigned int i=0; i<components; i++){
			spinor_mat.slice(i)%=obj_trans_func.at(std::make_pair(x_tilt,y_tilt));
		}	
	}		

}


