#general setting
filename_image = "O_520eV.dat";
x_dim = 15.62040001;   #width of the image in Angstrom
y_dim = 15.62040001;   #height of the image in Angstrom
nx = 256;              #width of image in pixel
ny = 256;              #height of the image in pixel


map_clim = [0,1.3385e-06];
xlabel_map = "distance in \\AA";
ylabel_map = "distance in \\AA";
clabel_map = "intensity in arb. u.";
map_color = "gray";


#font settings
colorbar_axis_fontsize = 18;
colorbar_title_fontsize = 18;
axis_linewidth = 1.5;
axis_fontsize = 18;
xylabel_fontsize=22;
plot_window_position = [0,0,576,432]; #[startx,starty,width,height]

image = load(filename_image);


#calculate image coordinates
X = (0:nx-1)*x_dim/nx; #x-coordinate in Angstrom
Y = (0:ny-1)*y_dim/ny; #y-coordinate in Angstrom


#plot image
figure('Position', plot_window_position);
imagesc(X,Y,image);
colormap(map_color);
p = get(gca, "position");
p(3) = p(3)-0.03;
set(gca,"position",p);
cbh = colorbar();
set(cbh, 'fontsize', colorbar_axis_fontsize, 'title',clabel_map);
cb_title = get(cbh, 'title');
set(cb_title, 'fontsize', colorbar_title_fontsize);
set(gca, "linewidth", axis_linewidth);
set(gca, "fontsize", axis_fontsize);
set(gca, 'YDir', 'normal'); #flip writing on y-axis
xlabel(xlabel_map, 'fontsize', xylabel_fontsize);
ylabel(ylabel_map, 'fontsize', xylabel_fontsize);
caxis(map_clim);
print('-dpdflatex', "map_A");



clear all;
